import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Service } from "../common/service";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
  

  private baseUrl: string;

  constructor(private http: HttpClient, private service: Service) {
    this.baseUrl = this.service.UrlApi;
  }

  getAll() {
    return this.http.get<any>(`${this.baseUrl}/employees`);
  }

  getManagers() {
    return this.http.get<any>(`${this.baseUrl}/users/managers`);
  }

  getDirectors() {
    return this.http.get<any>(`${this.baseUrl}/users/directors`);
  }

  getOptions() {
    return this.http.get<any>(`${this.baseUrl}/employees/options`);
  }

  getById(id) {
    return this.http.get<any>(`${this.baseUrl}/employees/${id}`);
  }

  getProfile(id) {
    return this.http.get<any>(`${this.baseUrl}/employees/${id}/profile`);
  }

  search(model) {
    return this.http.post<any>(`${this.baseUrl}/employees/search`, model);
  }

  searchUnemployees(model) {
    return this.http.post<any>(`${this.baseUrl}/employees/search/unemployees`, model);
  }

  addCategories(json){
    return this.http.put<any>(`${this.baseUrl}/employees/categories`, json);
  }

  getTasks(id){
    return this.http.get<any>(`${this.baseUrl}/employees/${id}/categories`);
  }

  put(id, json){
    return this.http.put<any>(`${this.baseUrl}/employees/${id}`, json);
  }

  getCurrentCategories() {
    return this.http.get<any>(`${this.baseUrl}/employees/currentCategories`);
  }

  getUrlForImportFile(yearId: number, monthId: number, prepaidId: number){
    return `${this.baseUrl}/prepaid/${prepaidId}/import/${yearId}/${monthId}`;
  }

  add(model){
    return this.http.post<any>(`${this.baseUrl}/employees/`, model);
  }

  updateEmployee(id, model){
    return this.http.put<any>(`${this.baseUrl}/employees/${id}/employee`, model);
  }
}
