import { Injectable } from '@angular/core';
import { Service } from '../common/service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CustomerService {

  private apiUrl: string;

  constructor(private http: HttpClient, private service: Service) {
    this.apiUrl = this.service.UrlApi + '/customers';
  }

  getAll() {
    return this.http.get<any>(this.apiUrl);
  }

  getAllOptions() {
    return this.http.get<any>(`${this.apiUrl}/all/options`);
  }

  getById(customerId) {
    return this.http.get<any>(`${this.apiUrl}/${customerId}`);
  }

  update() {
    return this.http.put<any>(this.apiUrl, {});
  }

  add(model) {
       return this.http.post<any>(`${this.apiUrl}`, model);
  }

  active(id, active) {
    return this.http.put<any>(`${this.apiUrl}/${id}/active/${active}`, {});
  }

  updateConsultant(model, id){
    return this.http.put<any>(`${this.apiUrl}/${id}/`, model);
  }
}
