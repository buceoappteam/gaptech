import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';
import { Service } from "./service";
import {map} from 'rxjs/operators';

@Injectable()
export class CommonService {
    constructor(private http: Http,
                private service: Service) {
    }

    getSupportMail(){
        return this.http.get(`${this.service.UrlApi}/supportMail`, { headers: this.service.getLoginHeaders()}).pipe(map(this.responseHanlder));
    }

    responseHanlder(response)
    {
        return response._body;
    }
}