import { Injectable } from '@angular/core';
import { Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Service } from "../common/service";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserDelegateService {
    private baseUrl: string;

    constructor(private http: HttpClient, private service: Service) {
        this.baseUrl = this.service.UrlApi;
    }

    post(model) {
        return this.http.post<any>(`${this.baseUrl}/userDelegate`, model);
    }

    get() {
       return this.http.get<any>(`${this.baseUrl}/userDelegate`);
    }

    delete(id: number) {
        return this.http.delete<any>(`${this.baseUrl}/userDelegate/${id}`);
    }
}