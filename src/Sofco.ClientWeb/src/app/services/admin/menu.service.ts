import { Injectable } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Service } from '../common/service';
import { Menu } from '../../models/admin/menu';
import { HttpClient } from '@angular/common/http';
import { UserInfoService } from '../common/user-info.service';

@Injectable()
export class MenuService {
    private baseUrl: string;

    public menu: Menu[];
    public userIsDirector: boolean;
    public userIsManager: boolean;
    public currentUser: any;
    public user: any;

    constructor(private http: HttpClient, private service: Service) {
         this.baseUrl = this.service.UrlApi;

        if (!this.menu) {
            const menu = JSON.parse(localStorage.getItem('menu'));
            
            if (menu != null) {
                this.menu = menu.menus;
            }
        }

        if (!this.currentUser) {
            this.currentUser = Cookie.get('currentUser');
        }

        const userInfo = UserInfoService.getUserInfo();

        if (userInfo) {
            this.currentUser = userInfo.name;
            this.user = userInfo;
        }
    }

    get() {
       return this.http.get<any>(`${this.baseUrl}/menu/`);
    }

    hasModule(module: string) {
        return this.menu.findIndex(x => x.module === module) > -1;
    }

    hasFunctionality(module: string, functionality: string) {
       return this.menu.findIndex(x => x.module === module && x.functionality === functionality) > -1;
    }

    hasAdminMenu() {
       if (this.hasModule("USER") || this.hasModule("ROLE") || this.hasModule("MODULE") || this.hasModule("FUNCITONALITY") || this.hasModule("CATEGORY") || this.hasModule("TASK") || this.hasModule("CONFIG")){
            return true;
       }
       return false;
    }

    hasResourcesMenu() {
        if (this.hasModule("RESOURCES") || this.hasModule("ALLOCATION")) {
            return true;
        }
        return false;
    }

    hasContractsMenu() {
        if (this.hasModule("CONSULTANT") || this.hasModule("COSTCENTER") || this.hasModule("CONTRACTS")) {
            return true;
        }
        return false;
    }

    hasWorkTimeManagement() {
        if (this.hasModule("WORKTIME")) {
            return true;
       }
       return false;
    }
}
