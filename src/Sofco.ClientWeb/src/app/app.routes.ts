
import { AuthGuard } from './guards/auth.guard';
import { Routes, RouterModule } from "@angular/router";
import { BlankLayoutComponent } from "./components/common/layouts/blankLayout.component";
import { BasicLayoutComponent } from "./components/common/layouts/basicLayout.component";
import { ForbiddenComponent } from "./views/appviews/errors/403/forbidden.component";
import { StarterViewComponent } from "./views/appviews/home/starterview.component";
import { LoginComponent } from "./views/appviews/login/login.component";
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './views/appviews/errors/404/notfound.component';

export const ROUTES:Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full', canActivate: [AuthGuard] },

  {
    path: '', component: BasicLayoutComponent,
    children: [
      { path: 'inicio', component: StarterViewComponent, canActivate: [AuthGuard] }
    ]
  },

  {
    path: '', component: BlankLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: '403', component: ForbiddenComponent, canActivate: [AuthGuard] },
      { path: '404', component: NotFoundComponent, canActivate: [AuthGuard] }
    ]
  },

  {
    path: 'admin', 
    component: BasicLayoutComponent,
    loadChildren: 'app/views/admin/admin.module#AdminModule'
  },

  {
    path: 'management', 
    component: BasicLayoutComponent,
    loadChildren: 'app/views/contracts/contracts.module#ContractsModule'
  },

  {
    path: 'resources', 
    component: BasicLayoutComponent,
    loadChildren: 'app/views/resources/resources.module#ResourceModule'
  },
 
  {
    path: 'profile', 
    component: BasicLayoutComponent,
    loadChildren: 'app/views/profile/profile.module#ProfileModule'
  },

  {
    path: 'workTimeManagement', 
    component: BasicLayoutComponent,
    loadChildren: 'app/views/worktime-management/worktime-management.module#WorkTimeManagementModule'
  },

  // Handle all other routes
  { path: '**',  redirectTo: '404' }
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(ROUTES);