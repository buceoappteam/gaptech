import { Component, OnInit } from '@angular/core';
import { AppSetting } from '../../../services/common/app-setting'
import { Subscription } from 'rxjs';
import { CommonService } from 'app/services/common/common.service';
declare function require(name: string);

@Component({ 
  selector: 'app-footer',
  templateUrl: 'footer.template.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {
 
  public appVersion: string;
  public apiVersion: string;
  public supportMail: string;

  supportSubscrip: Subscription;

  constructor(private appSetting: AppSetting, private commonService: CommonService) {
    const appPackage = require(`../../../../../package.json`);

    this.appVersion = appPackage.version;

    this.apiVersion = appSetting.ApiVersion;
  }

  ngOnInit(): void {
    var supportMail = localStorage.getItem("supportMail");

    if(supportMail){
      this.supportMail = supportMail;
    }
    else{
      this.getSupportMail();
    }
  } 

  getSupportMail(){
    this.supportSubscrip = this.commonService.getSupportMail().subscribe(response => {
        this.supportMail = response;
        localStorage.setItem("supportMail", this.supportMail);
    });
  }
}
