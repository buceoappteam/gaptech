import { Routes, RouterModule } from "@angular/router";
import { WorkTimeComponent } from "../worktime-management/worktime/worktime.component";
import { ResourceDetailComponent } from "../resources/detail/resource-detail.component";
import { AuthGuard } from "../../guards/auth.guard";

const PROFILE_ROUTER: Routes = [
    { path: "workTime", component: WorkTimeComponent, canActivate: [AuthGuard], data: { module: "PROFILE", functionality: "WORKTIME" } },
    { path: ":id", component: ResourceDetailComponent, canActivate: [AuthGuard], data: { fromRrhh: false, module: "PROFILE", functionality: "VIEW" } },
]

export const ProfileRouter = RouterModule.forChild(PROFILE_ROUTER);