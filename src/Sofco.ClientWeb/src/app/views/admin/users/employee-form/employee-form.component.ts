import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from "rxjs";
import { UtilsService } from "app/services/common/utils.service";
import { Router, ActivatedRoute } from "@angular/router";
import { MessageService } from "app/services/common/message.service";
import { Employee } from "app/models/admin/employee";
import { BsLocaleService, BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { EmployeeService } from "app/services/resources/employee.service";
import { I18nService } from "app/services/common/i18n.service";

declare var moment: any;

@Component({
    selector: 'employee-form',
    templateUrl: './employee-form.component.html',
    styleUrls: ['./employee-form.component.scss']
})

export class EmployeeFormComponent implements OnInit, OnDestroy {

    getCountriesSubscrip: Subscription
    getProvincesSubscrip: Subscription
    getEmployeeSubscrip: Subscription;
    addEmployeeSubscrip: Subscription
    updateEmployeeSubscrip: Subscription

    employeeForm: FormGroup
    model: Employee = new Employee()
    private id: number;
    private isEdit = false;
    bsConfig: Partial<BsDatepickerConfig>;
    employeeId: number
    userName: string
    userEmail: string

    provinces: any[] = new Array()
    countries: any[] = new Array()
    documentTypes: any[] = new Array()

    @Output() notifyEmployeeName: EventEmitter<string> = new EventEmitter<string>()
    @ViewChild('dateBirthday') dateBirthday;

    formErrors = {
        'name': '',
        'PhoneCountryCode': '',
        'PhoneAreaCode': '',
        'PhoneNumber': '',
        'country': '',
        'businessHours': '',
        'employeeNumber': '',
        'startDate': '',
        'province': '',
        'percentage': '',
    };
    validationMessages = {}

    constructor(private fb: FormBuilder,
        private utilsService: UtilsService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageService,
        private localeService: BsLocaleService,
        private employeeService: EmployeeService,
        private i18nService: I18nService) {
    }

    ngOnInit(): void {

        this.model = new Employee()
        this.localeService.use('es');
        this.bsConfig = Object.assign({}, { containerClass: 'theme-dark-blue', showWeekNumbers: false });

        this.dateBirthday.maxDate = new Date()

        this.getCountries()
        this.getDocumentTypes()

        this.employeeForm = this.fb.group({
            name: ['', Validators.required],
            phoneCountryCode: ['', Validators.required],
            phoneAreaCode: ['', Validators.required],
            phoneNumber: ['', Validators.required],
            country: [null, Validators.required],
            businessHours: ['', [Validators.required, Validators.min(0), Validators.max(24)]],
            employeeNumber: ['', Validators.required],
            startDate: ['', Validators.required],
            province: [null, Validators.required],
            percentage: ['', [Validators.required, Validators.min(0), Validators.max(100)]],
            userName: [{ value: '', disabled: true }],
            userEmail: [{ value: '', disabled: true }],
            address: [''],
            location: ['']
        })

        this.validationMessages = {
            'name': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'phoneCountryCode': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'phoneAreaCode': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'phoneNumber': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'country': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'businessHours': {
                'required': this.i18nService.translateByKey('requiredField'),
                'min': this.i18nService.translateByKey('ADMIN.USERS.minMaxbusinessHours'),
                'max': this.i18nService.translateByKey('ADMIN.USERS.minMaxbusinessHours'),
            },
            'employeeNumber': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'startDate': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'province': {
                'required': this.i18nService.translateByKey('requiredField'),
            },
            'percentage': {
                'required': this.i18nService.translateByKey('requiredField'),
                'min': this.i18nService.translateByKey('ADMIN.USERS.minMaxpercentage'),
                'max': this.i18nService.translateByKey('ADMIN.USERS.minMaxpercentage'),
            },
        };

        this.employeeForm.valueChanges.subscribe(data => {
            this.logValidationErrors();

        })

        this.employeeForm.get('country').valueChanges.subscribe(data => {
            this.getProvinces(data.id)
        })

    }


    ngOnDestroy(): void {
        if (this.getProvincesSubscrip) this.getProvincesSubscrip.unsubscribe();
        if (this.getCountriesSubscrip) this.getCountriesSubscrip.unsubscribe();
        if (this.getEmployeeSubscrip) this.getEmployeeSubscrip.unsubscribe();
        if (this.addEmployeeSubscrip) this.addEmployeeSubscrip.unsubscribe();
        if (this.updateEmployeeSubscrip) this.updateEmployeeSubscrip.unsubscribe();
    }

    logValidationErrors(group: FormGroup = this.employeeForm): void {
        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);
            this.formErrors[key] = '';
            if (abstractControl && !abstractControl.valid
                && (abstractControl.touched || abstractControl.dirty)) {
                const messages = this.validationMessages[key];
                for (const errorKey in abstractControl.errors) {
                    if (errorKey) {
                        this.formErrors[key] += messages[errorKey] + ' ';
                    }
                }
            }
        });
    }

    getProvinces(countryId) {
        this.messageService.showLoading();
        this.getProvincesSubscrip = this.utilsService.getProvinces(countryId).subscribe(data => {
            this.messageService.closeLoading()

            this.provinces = data;
        },
            error => {
                this.messageService.closeLoading();
            })
    }

    getCountries() {
        this.messageService.showLoading();
        this.getCountriesSubscrip = this.utilsService.getCountries().subscribe(data => {
            this.messageService.closeLoading()

            this.countries = data;
            if (this.countries.length > 0) {
                this.employeeForm.get('country').setValue(this.countries[0])
            }
        },
            error => {
                this.messageService.closeLoading();
            });
    }

    getDocumentTypes() {
        this.messageService.showLoading();
        this.getCountriesSubscrip = this.utilsService.getDocumentTypes().subscribe(
            data => {
                this.messageService.closeLoading()
                this.documentTypes = data;
            },
            error => {
                this.messageService.closeLoading();
            });
    }

    setUserPropierties(name, username, userEmail) {

        this.employeeForm.patchValue({
            name: name,
            userName: username,
            userEmail: userEmail
        })
        this.userName = username;
        this.userEmail = userEmail;
    }

    save() {
        if (this.validateForm()) {
            this.mapFormValuesToModel()
            if (this.isEdit) {
                this.updateEmployee()
            }
            else {
                this.AddEmployeeAndUser()
            }
        }
    }

    markAsDirty(group: FormGroup = this.employeeForm): void {
        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);
            abstractControl.markAsDirty();
        });
    }

    updateEmployee() {
        this.messageService.showLoading();
       
        this.updateEmployeeSubscrip = this.employeeService.updateEmployee(this.employeeId, this.model).subscribe(
            response => {
                this.messageService.closeLoading();
                this.notifyEmployeeName.emit(this.model.name)
            },
            error => {
                this.messageService.closeLoading();
            }
        )
    }

    AddEmployeeAndUser() {
        this.messageService.showLoading();

        this.addEmployeeSubscrip = this.employeeService.add(this.model).subscribe(
            response => {
                this.messageService.closeLoading();

                this.back();
            },
            error => {
                this.messageService.closeLoading();
            }
        )
    }

    getEmployee(idEmployee, userId, userName, userEmail) {
        this.messageService.showLoading();
        this.employeeId = idEmployee

        this.getEmployeeSubscrip = this.employeeService.getProfile(idEmployee).subscribe(
            response => {

                this.getProvincesSubscrip = this.utilsService.getProvinces(response.data.countryId).subscribe(
                    data => {
                        this.messageService.closeLoading()
                        this.provinces = data;
                        this.model = response.data
                        this.model.phoneNumber = parseInt(response.data.phoneNumber)
                        this.userName = userName
                        this.userEmail = userEmail
                        this.model.userId = userId
                        if (response.data.endDate != null) {
                            this.model.endDate = moment(response.data.endDate).format("DD/MM/YYYY")
                        }
                        if (response.data.birthday != null) {
                            this.model.birthday = moment(response.data.birthday).toDate()
                        }

                        this.mapModelToFormValues()
                        $('#tabsEmployee li:nth-child(1) a').click()
                    })

                this.messageService.closeLoading();
            },
            error => {
                this.messageService.closeLoading();
            }
        )
    }

    mapFormValuesToModel() {
        this.model.name = this.employeeForm.value.name
        this.model.phoneCountryCode = parseInt(this.employeeForm.value.phoneCountryCode)
        this.model.phoneAreaCode = parseInt(this.employeeForm.value.phoneAreaCode)
        this.model.phoneNumber = parseInt(this.employeeForm.value.phoneNumber)
        this.model.businessHours = this.employeeForm.value.businessHours
        this.model.employeeNumber = this.employeeForm.value.employeeNumber
        this.model.startDate = this.employeeForm.value.startDate
        this.model.provinceId = this.employeeForm.value.province.id
        this.model.percentage = this.employeeForm.value.percentage
        this.model.userName = this.userName
        this.model.email = this.userEmail
        this.model.address = this.employeeForm.value.address
        this.model.location = this.employeeForm.value.location
        this.model.endDate = null
    }

    mapModelToFormValues() {
        this.employeeForm.patchValue({
            name: this.model.name,
            phoneCountryCode: this.model.phoneCountryCode,
            phoneAreaCode: this.model.phoneAreaCode,
            phoneNumber: this.model.phoneNumber,
            businessHours: this.model.businessHours,
            employeeNumber: this.model.employeeNumber,
            startDate: moment(this.model.startDate).toDate(),
            percentage: this.model.percentage,
            address: this.model.address,
            location: this.model.location,
            userName: this.userName,
            userEmail: this.userEmail,
            country: this.countries.find(x => x.id == this.model.countryId),
            province: this.provinces.find(x => x.id == this.model.provinceId)
        })
    }

    validateForm() {
        if (this.employeeForm.valid) {
            return true
        }
        else {
            $('#tabsEmployee li:nth-child(1) a').click()
            this.markAsDirty()
            this.logValidationErrors(this.employeeForm)
            return false
        }
    }

    back() {
        window.history.back();
    }

}