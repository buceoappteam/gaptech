import { Router } from '@angular/router';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MessageService } from '../../../../services/common/message.service';
import { UserService } from '../../../../services/admin/user.service';
import { Subscription } from 'rxjs';
import { EmployeeFormComponent } from '../employee-form/employee-form.component';
import { Employee } from 'app/models/admin/employee';

@Component({
    selector: 'app-user-add',
    templateUrl: './user-add.component.html',
    styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnDestroy {

    public model: any = { userPrincipalName: "", name: "", userName: "" };
    public modelEmployee
    public loading: boolean = false;
    public usersFound: boolean = false;
    public toSearch: string;
    public users: any[] = new Array<any>();

    public showEmployee: boolean = false
    public showSave: boolean = false

    private searchSubscrip: Subscription;
    private saveSubscrip: Subscription;

    @ViewChild("employeeDetail") employeeDetail: EmployeeFormComponent

    constructor(private service: UserService,
        private messageService: MessageService,
        private router: Router) { }

    ngOnDestroy(): void {
        if (this.searchSubscrip) this.searchSubscrip.unsubscribe();
        if (this.saveSubscrip) this.saveSubscrip.unsubscribe();
    }

    save() {
        if (this.employeeDetail.validateForm()) {
            this.messageService.showLoading();
            
            this.employeeDetail.save()
        }
    }

    back() {
        this.router.navigate(['/admin/users']);
    }

    select(user) {

        this.model = user;
        this.showSave = true;

        this.employeeDetail.setUserPropierties(user.displayName, user.userName, user.userPrincipalName)
        this.showEmployee = true
    }

    searchBySurname() {
        this.loading = true;
        this.usersFound = false;
        this.showEmployee = false

        this.searchSubscrip = this.service.searchBySurname(this.toSearch).subscribe(
            response => {
                this.loading = false;
                this.users = response.value;

                if (this.users.length > 0) {
             
                    if(this.users.length == 1){
                        this.select(this.users[0]);
                    }
                    else{
                        this.usersFound = true;
                    }
                }
                else {
                    this.messageService.showError("ADMIN.USERS.userNotFoundInAd");
                }
            },
            error => this.loading = false
        )
    }

    searchByEmail() {
        this.loading = true;
        this.usersFound = false;
        this.showEmployee = false

        this.searchSubscrip = this.service.searchByEmail(this.toSearch).subscribe(
            response => {
                this.loading = false;

                if(response && response.data && response.data.userName){
                    this.select(response.data);
                }
                else{
                    this.messageService.showError("ADMIN.USERS.userNotFoundInAd");
                }
            },
            error => this.loading = false)
    }
}
