import { Subscription } from 'rxjs';
import { Ng2ModalConfig } from '../../../../components/modal/ng2modal-config';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { UserService } from "../../../../services/admin/user.service";
import { UserDetail } from "../../../../models/admin/userDetail";
import { MenuService } from '../../../../services/admin/menu.service';
import { RoleService } from 'app/services/admin/role.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
    private id;
    public user = <UserDetail>{};
    private roleId: any;

    public modalConfig: Ng2ModalConfig = new Ng2ModalConfig(
        "ADMIN.USERS.addGroup", //title
        "modalGroups", //id
        true,          //Accept Button
        true,          //Cancel Button
        "ACTIONS.ACCEPT",     //Accept Button Text
        "ACTIONS.cancel");   //Cancel Button Text

    public confirmModalConfig: Ng2ModalConfig = new Ng2ModalConfig(
      "ACTIONS.confirmDelete",
      "confirmModal",
      true,
      true,
      "ACTIONS.ACCEPT",
      "ACTIONS.cancel"
    );

    public employeeModalConfig: Ng2ModalConfig = new Ng2ModalConfig(
        "Editar Empledo",
        "employeeModal",
        true,
        true,
        "ACTIONS.ACCEPT",
        "ACTIONS.cancel"
      );

    private routeSubscrip: Subscription;
    private detailsSubscrip: Subscription;

    //GroupService
    public checkAtLeft:boolean = true;

    public rolesToAdd: any[] = new Array();

    public rolesToAddSubscrip: Subscription;

    @ViewChild('modalRoles') modalRoles;
    @ViewChild('confirmModal') confirmModal;
    @ViewChild('employeeModal') employeeModal;
    @ViewChild('employeeDetail') employeeDetail;
    
    constructor(
        private service: UserService, 
        private activatedRoute: ActivatedRoute, 
        private menuService: MenuService,
        private roleService: RoleService) { }

    ngOnInit() {
        this.employeeModal.size = 'modal-lg'
        this.routeSubscrip = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.getDetails();
        });
    }

    canAddRole(){
        return this.menuService.hasFunctionality("USER", "ADD-ROLE");
    }
 
    getDetails(){
        this.detailsSubscrip = this.service.getDetail(this.id).subscribe(
            user => {
                this.user = user;                
                this.getAllRoles();
            },
            error =>{
            this.back()
            });
    }

    ngOnDestroy(){
        if(this.routeSubscrip) this.routeSubscrip.unsubscribe();
        if(this.detailsSubscrip) this.detailsSubscrip.unsubscribe();
        if(this.rolesToAddSubscrip) this.rolesToAddSubscrip.unsubscribe();
    }

    getAllRoles(){
        this.rolesToAddSubscrip = this.roleService.getOptions().subscribe(
            data => {
                this.rolesToAdd = new Array();
                var roles = new Array();
                var index = 0;
                for(var i: number = 0; i<data.length; i++){

                    if(!this.isOptionInArray(this.user.roles, data[i]) ){
                        data[i].included = false;
                        data[i].index = index;
                        roles.push(data[i]);
                        index++;
                    }
                    
                }

                this.rolesToAdd = roles;
            });
    }

    private isOptionInArray(arrGroup, option: any): boolean{
        var esta: boolean = false;

        for(var i: number = 0; i<arrGroup.length; i++){
            if(arrGroup[i].id == option.id ){
                esta = true;
                break;
            }
        }

        return esta;
    }

    assignRoles(){

        var arrRolesToAdd = this.rolesToAdd.filter((el)=> el.included).map((item) => {
            return item.id
        });

        var objToSend = {
            rolesToAdd: arrRolesToAdd,
            rolesToRemove: []
        };

        this.service.assignRoles(this.id, objToSend).subscribe(
            () => {
                this.modalRoles.hide();
                this.getDetails();
            });
    }

    unassignRole(){
        this.service.unassignRole(this.user.id, this.roleId).subscribe(
            () => {
                this.confirmModal.hide();
                this.getDetails();
            },
            () => this.confirmModal.hide());
    }

    openConfirmModal(roleId){
        this.roleId = roleId;
        this.confirmModal.show();
    }

    openEmployeeModal(){
        
        this.employeeDetail.isEdit = true
        this.employeeDetail.getEmployee(this.user.employeeId, this.user.id, this.user.userName, this.user.email)
        this.employeeModal.show();
    }

    updateEmployee(){
        this.employeeModal.isProcessing = false;
        //this.employeeModal.hide()
        this.employeeDetail.save()
    }

    closeEmployeeModal(){
        this.employeeModal.hide();
    }

    back() {
        window.history.back();
    }

    updateName(employeeName: string){
       
        this.user.name = employeeName
        this.employeeModal.hide()
    }
}
