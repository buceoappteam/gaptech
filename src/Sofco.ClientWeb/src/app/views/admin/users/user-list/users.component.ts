import { MessageService } from '../../../../services/common/message.service';
import { DatatablesLocationTexts } from '../../../../components/datatables/datatables.location-texts';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DatatablesEditionType } from "../../../../components/datatables/datatables.edition-type";
import { DatatablesColumn } from "../../../../components/datatables/datatables.columns";
import { Subscription } from "rxjs";
import { DatatablesOptions } from "../../../../components/datatables/datatables.options";
import { DatatablesDataType } from "../../../../components/datatables/datatables.datatype";
import { DatatablesAlignment } from "../../../../components/datatables/datatables.alignment";
import { UserService } from "../../../../services/admin/user.service";
import { I18nService } from '../../../../services/common/i18n.service';
import { MenuService } from 'app/services/admin/menu.service';
import { DataTableService } from 'app/services/common/datatable.service';
import { Ng2ModalConfig } from 'app/components/modal/ng2modal-config';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit, OnDestroy {

    data;
    getAllSubscrip: Subscription;
    editionType = DatatablesEditionType.ButtonsAtTheEndOfTheRow;
    locationTexts = new DatatablesLocationTexts("Assign");
    public modalMessage: string = "hola";
    public userSelected: any;

    @ViewChild('dt') dt; 
    @ViewChild("modalUser") confirmUserModal;
    public modalUserConfig: Ng2ModalConfig = new Ng2ModalConfig(
        "ACTIONS.confirmBody", //title
        "modalConfirm", //id
        true,          //Accept Button
        true,          //Cancel Button
        "ACTIONS.DEACTIVATE",     //Accept Button Text
        "ACTIONS.cancel");   //Cancel Button Text

    //subscriptions
    deleteSubscrip: Subscription;
    getSubscrip: Subscription;
    habilitarSubscrip: Subscription;

    // options = new DatatablesOptions( 
    //     true,  //edit
    //     false,  //delete
    //     false,  //view
    //     true,  //habInhab
    //     false,  //other
    //     false, //other2
    //     false, //other3
    //     "fa-compress",     //other1Icon
    //     "fa-check",      //other2Icon
    //     "fa-cogs",        //other3Icon
    //     { title: this.i18nService.translateByKey("ADMIN.USERS.TITLE"), columns: [0, 1, 2, 3, 4, 5]},
    //     0,     //orderByColumn
    //     "asc"
    // ); 

    // private dataTypeEnum = DatatablesDataType;
    // private alignmentEnum = DatatablesAlignment;

    // columns: DatatablesColumn[] = [
    //     new DatatablesColumn("id", "Id", "", 0, this.dataTypeEnum.number, this.alignmentEnum.left),
    //     new DatatablesColumn("name", "ADMIN.name", "70px", 1, this.dataTypeEnum.string, this.alignmentEnum.left),
    //     new DatatablesColumn("employeeNumber", "allocationManagement.resources.grid.employeeNumber", "30px", this.dataTypeEnum.number, 1, this.alignmentEnum.left),
    //     new DatatablesColumn("email", "ADMIN.mail", "70px", 1, this.dataTypeEnum.string, this.alignmentEnum.left),
    //     new DatatablesColumn("active", "ADMIN.active", "10px", 1, this.dataTypeEnum.boolean, this.alignmentEnum.center),
    //     new DatatablesColumn("startDate", "ADMIN.startDate", "15px", 1, this.dataTypeEnum.date, this.alignmentEnum.center),
    //     new DatatablesColumn("endDate", "ADMIN.endDate", "15px", 1, this.dataTypeEnum.date, this.alignmentEnum.center),
    // ]

    constructor(
      private router: Router,
      public menuService: MenuService,
      private service: UserService,
      private messageService: MessageService,
      private i18nService: I18nService,
      private dataTableService: DataTableService) {

   //   this.options.descripFieldName = "name";
    }

    ngOnInit() {
        this.getAll();
    }

    ngOnDestroy(){
        if(this.getAllSubscrip) this.getAllSubscrip.unsubscribe();
        if(this.deleteSubscrip) this.deleteSubscrip.unsubscribe();
        if(this.getSubscrip) this.getSubscrip.unsubscribe();
        if(this.habilitarSubscrip) this.habilitarSubscrip.unsubscribe();
    }

    getAll() {
        this.messageService.showLoading();

        this.getAllSubscrip = this.service.getAll().subscribe(
            d => { 
                this.messageService.closeLoading();
                this.data = d;
                this.initGrid()
            },
            err => this.messageService.closeLoading());
    }

    goToDetail(id: number) {
        this.router.navigate(['/admin/users/edit', id]);
    }

    getEntity(id: number, callback = null) {
        this.getSubscrip = this.service.get(id).subscribe(
            data => {
                if (callback != null) { callback(data); }
            });
    }

    openModalhabInhab(user){
        this.userSelected = user;
        let active = user.active;

        if (active){
          //this.confirm = this.disableEntity;
          this.modalUserConfig.acceptButtonText = "ACTIONS.DEACTIVATE";
          this.modalMessage = this.locationTexts.disableQuestion.replace("¶", user.name)
          this.confirmUserModal.show();
        } else {
         // this.confirm = this.enableEntity;
          this.modalUserConfig.acceptButtonText = "ACTIONS.ACTIVATE"
          this.modalMessage = this.locationTexts.enableQuestion.replace("¶", user.name)
          this.confirmUserModal.show();
        }
    }

    habInhab(){
        this.confirmUserModal.hide();
        if (this.userSelected.active){
            this.deactivate(this.userSelected.id);
        } else {
            this.activate(this.userSelected.id);
        }
    }

    deactivate(id: number){
        this.deleteSubscrip = this.service.deactivate(id).subscribe(
            response => {
                //this.getEntity(id, (e) => this.dt.updateById(id, e));
                this.userSelected.active = response.data.active
                this.userSelected.endDate = response.data.endDate
            });
    }

    activate(id: number){
        this.habilitarSubscrip = this.service.activate(id).subscribe(
            response => {
                //this.getEntity(id, (e) => this.dt.updateById(id, e));
                this.userSelected.active = response.data.active
                this.userSelected.endDate = response.data.endDate
            });
    }

    initGrid() {
        var columns = [0, 1, 2, 4];
        var title = `Listado Usuarios`;

        var params = {
            selector: '#stateTable',
            columns: columns,
            title: title,
            withExport: true,
        }

        this.dataTableService.destroy(params.selector);
        this.dataTableService.initialize(params);
    }

}