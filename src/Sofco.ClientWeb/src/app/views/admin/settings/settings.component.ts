import { MessageService } from '../../../services/common/message.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SettingsService } from '../../../services/admin/settings.service';
import { AppSettingService } from '../../../services/common/app-setting.service';
import { AppSetting } from '../../../services/common/app-setting';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit, OnDestroy {

  settings: Array<any>;
  serviceSubscrip: Subscription;
  public loading = false;

  licenseTypes: Array<any> = new Array();
  jobSettings: Array<any> = new Array();

  constructor(
      private service: SettingsService,
      private messageService: MessageService,
      private appSettting: AppSetting,
      private appSetttingService: AppSettingService) {
  }

  ngOnInit() {
    this.getAll();
  }

  ngOnDestroy() {
    if (this.serviceSubscrip) { this.serviceSubscrip.unsubscribe(); }
  }

  getAll() {
    this.loading = true;
    this.serviceSubscrip = this.service.getAll().subscribe(
      d => {
        this.settings = d.body.data;
        this.loading = false
      },
      err => this.loading = false);
  }


  save() {
    this.loading = true;

    this.serviceSubscrip = this.service.save(this.settings).subscribe(
      d => { this.settings = d.data; this.loading = false; this.saveResponseHandler(); },
      err => this.loading = false);
  }

  saveResponseHandler() {
    this.messageService.succes('ADMIN.settings.saveSuccess');
    for (const key in this.appSettting) {

      const item = this.settings.find(s => s.key === key);

      if (item != null) {
        this.appSettting[key] = this.appSetttingService.parseValueByType(item);
      }
    }
  }
}
