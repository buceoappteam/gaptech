import { Routes, RouterModule } from "@angular/router";
import { SettingsComponent } from "./settings/settings.component";
import { AuthGuard } from "../../guards/auth.guard";
import { TaskEditComponent } from "./tasks/edit/task-edit.component";
import { TaskAddComponent } from "./tasks/add/task-add.component";
import { TaskListComponent } from "./tasks/list/task-list.component";
import { CategoryEditComponent } from "./category/edit/category-edit.component";
import { CategoryAddComponent } from "./category/add/category-add.component";
import { CategoryListComponent } from "./category/list/category-list.component";
import { ModuleEditComponent } from "./modules/module-edit/module-edit.component";
import { ModulesComponent } from "./modules/module-list/modules.component";
import { FunctionalitiesComponent } from "./functionalities/functionalities.component";
import { UserDetailComponent } from "./users/user-detail/user-detail.component";
import { UserAddComponent } from "./users/user-add/user-add.component";
import { UsersComponent } from "./users/user-list/users.component";
import { RolEditComponent } from "./roles/rol-edit/rol-edit.component";
import { RolAddComponent } from "./roles/rol-add/rol-add.component";
import { RolesComponent } from "./roles/rol-list/roles.component";
import { WorkflowListComponent } from "./workflow/workflows-list/workflow-list.component";
import { WorkflowDetailComponent } from "./workflow/workflow-detail/workflow-detail.component";
import { WorkflowTransitionAddComponent } from "./workflow/transition-add/transition-add";
import { WorkflowTransitionEditComponent } from "./workflow/transition-edit/transition-edit";
import { WorkflowStateListComponent } from "./workflow/state-list/state-list.component";
import { WorkflowStateAddComponent } from "./workflow/state-add/state-add.component";
import { WorkflowStateEditComponent } from './workflow/state-edit/state-edit.component';
import { HolidaysComponent } from "./holidays/holidays.component";

const ADMIN_ROUTER: Routes = [
    { path: 'roles', children:[
        { path: '', component: RolesComponent, canActivate: [AuthGuard], data: { module: "ROLE", functionality: "LIST" } },
        { path: 'add', component: RolAddComponent, canActivate: [AuthGuard], data: { module: "ROLE", functionality: "ADD" } },
        { path: 'edit/:id', component: RolEditComponent, canActivate: [AuthGuard], data: { module: "ROLE", functionality: "UPDATE" } }
      ]},

      { path: "users", children: [
         { path: '', component: UsersComponent, canActivate: [AuthGuard], data: { module: "USER", functionality: "LIST" } },
         { path: 'add', component: UserAddComponent, canActivate: [AuthGuard], data: { module: "USER", functionality: "ADD" } },
         { path: 'edit/:id', component: UserDetailComponent, canActivate: [AuthGuard], data: { module: "USER", functionality: "UPDATE" } }
      ]},

      { path: "functionalities", component: FunctionalitiesComponent, canActivate: [AuthGuard], data: { module: "FUNCITONALITY", functionality: "LIST" } },

      { path: "entities", children: [
        { path: '', component: ModulesComponent, canActivate: [AuthGuard], data: { module: "MODULE", functionality: "LIST" } },
        { path: 'edit/:id', component: ModuleEditComponent, canActivate: [AuthGuard], data: { module: "MODULE", functionality: "UPDATE" } }
      ]},

      { path: "categories", children: [
        { path: '', component: CategoryListComponent, canActivate: [AuthGuard], data: { module: "CATEGORY", functionality: "LIST" } },
        { path: 'add', component: CategoryAddComponent, canActivate: [AuthGuard], data: { module: "CATEGORY", functionality: "ADD" } },
        { path: ':id/edit', component: CategoryEditComponent, canActivate: [AuthGuard], data: { module: "CATEGORY", functionality: "UPDATE" } }
      ]},

      { path: "tasks", children: [
        { path: '', component: TaskListComponent, canActivate: [AuthGuard], data: { module: "TASK", functionality: "LIST" } },
        { path: 'add', component: TaskAddComponent, canActivate: [AuthGuard], data: { module: "TASK", functionality: "ADD" } },
        { path: ':id/edit', component: TaskEditComponent, canActivate: [AuthGuard], data: { module: "TASK", functionality: "UPDATE" } }
      ]},

      { path: "states", children: [
        { path: '', component: WorkflowStateListComponent, canActivate: [AuthGuard], data: { module: "STATES", functionality: "LIST" } },
        { path: 'add', component: WorkflowStateAddComponent, canActivate: [AuthGuard], data: { module: "STATES", functionality: "ADD" } },
        { path: ':id/edit', component: WorkflowStateEditComponent, canActivate: [AuthGuard], data: { module: "STATES", functionality: "UPDATE" } }
      ]},

      { path: 'settings', children: [
        { path: '', component: SettingsComponent, canActivate: [AuthGuard], data: { module: "CONFIG", functionality: "UPDATE" } }
      ]},

      { path: "workflows", children: [
        { path: '', component: WorkflowListComponent, canActivate: [AuthGuard] },
        { path: ':id', component: WorkflowDetailComponent, canActivate: [AuthGuard] },
        { path: ':workflowId/transition/new', component: WorkflowTransitionAddComponent, canActivate: [AuthGuard] },
        { path: ':workflowId/transition/:id', component: WorkflowTransitionEditComponent, canActivate: [AuthGuard] }
      ]},

      {
        path: 'holidays',
        children: [
          { path: "", component: HolidaysComponent, canActivate: [AuthGuard], data: { module: "HOLIDAY", functionality: "LIST" } }
        ]
      },
];

export const AdminRouter = RouterModule.forChild(ADMIN_ROUTER);
