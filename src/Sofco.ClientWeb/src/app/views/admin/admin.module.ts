import { TranslateModule } from '@ngx-translate/core';
import { Ng2ModalModule } from '../../components/modal/ng2modal.module';
import { ICheckModule } from '../../components/icheck/icheck.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { Ng2DatatablesModule } from "../../components/datatables/ng2-datatables.module";
import { NumbersOnlyModule } from 'app/components/numbersOnly/numberOnly.directive';

import { RolesComponent } from './roles/rol-list/roles.component';
import { RolAddComponent } from './roles/rol-add/rol-add.component';
import { RolEditComponent } from './roles/rol-edit/rol-edit.component';

import { UsersComponent } from './users/user-list/users.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';

import { SettingsComponent } from './settings/settings.component';

import { FunctionalitiesComponent } from './functionalities/functionalities.component';
import { ModulesComponent } from './modules/module-list/modules.component';
import { ModuleEditComponent } from './modules/module-edit/module-edit.component';
import { FunctionalityService } from "../../services/admin/functionality.service";
import { ModuleService } from "../../services/admin/module.service";
import { RoleService } from "../../services/admin/role.service";
import { UserService } from "../../services/admin/user.service";
import { SettingsService } from "../../services/admin/settings.service";
import { UserAddComponent } from './users/user-add/user-add.component';
import { SpinnerModule } from '../../components/spinner/spinner.module';
import { DatePickerModule } from '../../components/date-picker/date-picker.module';
import { CategoryService } from '../../services/admin/category.service';
import { CategoryAddComponent } from './category/add/category-add.component';
import { CategoryEditComponent } from './category/edit/category-edit.component';
import { CategoryListComponent } from './category/list/category-list.component';
import { TaskService } from '../../services/admin/task.service';
import { TaskAddComponent } from './tasks/add/task-add.component';
import { TaskListComponent } from './tasks/list/task-list.component';
import { TaskEditComponent } from './tasks/edit/task-edit.component';
import { Select2Module } from '../../components/select2/select2';
import { AdminRouter } from './admin.router';
import { NgSelectModule } from '@ng-select/ng-select';
import { WorkflowListComponent } from './workflow/workflows-list/workflow-list.component';
import { WorkflowService } from 'app/services/workflow/workflow.service';
import { WorkflowDetailComponent } from './workflow/workflow-detail/workflow-detail.component';
import { WorkflowAddComponent } from './workflow/workflow-add/workflow-add.component';
import { WorkflowTransitionAddComponent } from './workflow/transition-add/transition-add';
import { UtilsService } from 'app/services/common/utils.service';
import { WorkflowTransitionFormComponent } from './workflow/transition-form/transition-form';
import { WorkflowTransitionEditComponent } from './workflow/transition-edit/transition-edit';
import { WorkflowStateListComponent } from './workflow/state-list/state-list.component';
import { WorkflowStateAddComponent } from './workflow/state-add/state-add.component';
import { WorkflowStateEditComponent } from './workflow/state-edit/state-edit.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { HolidayService } from 'app/services/worktime-management/holiday.service';
import { EmployeeFormComponent } from './users/employee-form/employee-form.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { EmployeeService } from 'app/services/resources/employee.service';
import { DigitModule } from 'app/components/digit-limit/digit-limit.directive';

@NgModule({
  declarations: [RolesComponent, RolAddComponent, RolEditComponent, UsersComponent, FunctionalitiesComponent, 
                 UserDetailComponent, ModulesComponent, ModuleEditComponent, UserAddComponent, HolidaysComponent,
                 SettingsComponent, CategoryAddComponent, CategoryEditComponent, CategoryListComponent, TaskAddComponent, TaskListComponent,
                 TaskEditComponent, WorkflowTransitionEditComponent, WorkflowListComponent, WorkflowDetailComponent, WorkflowAddComponent,
                 WorkflowTransitionAddComponent, WorkflowTransitionFormComponent, WorkflowStateListComponent, WorkflowStateAddComponent, 
                 WorkflowStateEditComponent, EmployeeFormComponent],

  imports     : [CommonModule, Ng2DatatablesModule, RouterModule, FormsModule, ICheckModule, Ng2ModalModule, TranslateModule, 
                 SpinnerModule, DatePickerModule, Select2Module, AdminRouter, NgSelectModule, ReactiveFormsModule
                 , BsDatepickerModule.forRoot(), NumbersOnlyModule, DigitModule],    


  providers   : [RoleService, UserService, FunctionalityService, ModuleService, SettingsService, HolidayService,
                 CategoryService, TaskService, WorkflowService, UtilsService, EmployeeService],

  exports     : [RolesComponent, RolAddComponent, RolEditComponent, UsersComponent, FunctionalitiesComponent, 
                 UserDetailComponent, SettingsComponent]
})

export class AdminModule {}
