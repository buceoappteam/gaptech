import { Routes, RouterModule } from "@angular/router";
import { WorkTimeApprovalComponent } from "./approval/worktime-approval.component";
import { WorkTimeSearchComponent } from "./search/worktime-search.component";
import { AuthGuard } from "../../guards/auth.guard";
import { ImportWorkTimesComponent } from "app/views/worktime-management/import/import-worktime.component";
import { WorkTimeControlComponent } from "./worktime-control/worktime-control.component";
import { AddCloseDateComponent } from "./closeDates/add/closeDate-add.component";

const WORKTIME_ROUTER: Routes = [
    {
        path: 'workTime', children:[
          { path: "approval", component: WorkTimeApprovalComponent, canActivate: [AuthGuard], data: { module: "WORKTIME", functionality: "APPROVAL" } },
          { path: "search", component: WorkTimeSearchComponent, canActivate: [AuthGuard], data: { module: "WORKTIME", functionality: "LIST" } },
          { path: "import", component: ImportWorkTimesComponent, canActivate: [AuthGuard], data: { module: "WORKTIME", functionality: "IMPORT" } },
          { path: "control", component: WorkTimeControlComponent, canActivate: [AuthGuard], data: { module: "WORKTIME", functionality: "CONTROL" } }
        ]
    },

    { path: "closeDate",  component: AddCloseDateComponent, canActivate: [AuthGuard], data: { module: "WORKTIME", functionality: "CLOSEDATE" } },
];

export const WorkTimeRouter = RouterModule.forChild(WORKTIME_ROUTER);