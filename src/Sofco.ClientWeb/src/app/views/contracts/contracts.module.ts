import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { PeityModule } from '../../components/charts/peity';
import { TranslateModule } from "@ngx-translate/core";
import { AnalyticService } from '../../services/contracts/analytic.service';
import { SpinnerModule } from '../../components/spinner/spinner.module';
import { Select2Module } from '../../components/select2/select2';
import { AllocationService } from '../../services/resources/allocation.service';
import { NgDatepickerModule } from 'ng2-datepicker';
import { EmployeeService } from '../../services/resources/employee.service';
import { IboxtoolsModule } from '../../components/common/iboxtools/iboxtools.module';
import { Ng2ModalModule } from '../../components/modal/ng2modal.module';
import { CostCenterService } from '../../services/contracts/cost-center.service';
import { ICheckModule } from '../../components/icheck/icheck.module';
import { PCheckModule } from '../../components/pcheck/pcheck.module';
import { DateRangePickerModule } from '../../components/date-range-picker/date-range.picker.module';
import { DatePickerModule } from '../../components/date-picker/date-picker.module';
import { AnalyticSearchComponent } from './analytics/search/analytic-search.component';
import { AddCostCenterComponent } from './cost-center/add/add-cost-center.component';
import { EditAnalyticComponent } from './analytics/edit/edit-analytic.component';
import { EditCostCenterComponent } from './cost-center/edit/edit-cost-center.component';
import { ViewAnalyticComponent } from './analytics/view/view-analytic.component';
import { ListCostCenterComponent } from './cost-center/list/list-cost-center.component';
import { NewAnalyticComponent } from './analytics/new/new-analytic.component';
import { AnalyticFormComponent } from './analytics/analytic-form/analytic-form.component';
import { ContractsRouter } from './contracts.router';
import { AddAllocationComponent } from '../resources/allocation/add-by-analytic/add-by-analytic.component';
import { ResourceByAnalyticComponent } from './by-analytic/resource-by-analytic.component';
import { ResourceTimelineComponent } from '../resources/allocation/resource-timeline/resource-timeline.component';
import { CommonModule } from '@angular/common';
import { CustomerService } from '../../services/contracts/customer.service';
import { AllocationAssingTableModule } from '../resources/allocation/allocation-assignment-table/alloc-assing-table.module';
import { NumbersOnlyModule } from 'app/components/numbersOnly/numberOnly.directive';
import { CloseDateService } from 'app/services/worktime-management/closeDate.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddConsultantComponent } from './consultant/add/add-consultant.component';
import { ListConsultantComponent } from './consultant/list/list-consultant.component';
import { CustomersComponent } from './customers/customers.component';
import { CategoryService } from 'app/services/admin/category.service';
import { UtilsService } from 'app/services/common/utils.service';
import { UserDelegateComponent } from './userDelegate/userdelegate';
import { UserDelegateService } from 'app/services/admin/userDelegate.service';
import { WorktimeService } from 'app/services/worktime-management/worktime.service';
import { DigitModule } from 'app/components/digit-limit/digit-limit.directive';

@NgModule({
  declarations: [
    AnalyticSearchComponent, AddCostCenterComponent, ListCostCenterComponent, NewAnalyticComponent, AnalyticFormComponent,
    EditAnalyticComponent, ViewAnalyticComponent, EditCostCenterComponent, AddAllocationComponent, ResourceByAnalyticComponent,
    ResourceTimelineComponent, AddConsultantComponent, ListConsultantComponent, CustomersComponent, UserDelegateComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PeityModule,
    AllocationAssingTableModule,
    FormsModule,
    SpinnerModule,
    TranslateModule,
    Select2Module,
    NgDatepickerModule,
    DatePickerModule,
    DateRangePickerModule,
    IboxtoolsModule,
    ICheckModule,
    NumbersOnlyModule,
    Ng2ModalModule,
    PCheckModule,
    DigitModule,
    ContractsRouter,
    NgSelectModule,
    ReactiveFormsModule
  ],
  providers: [ AnalyticService, AllocationService, EmployeeService, CostCenterService, CustomerService, CategoryService, UtilsService, UserDelegateService, WorktimeService ],
  exports: [],
})

export class ContractsModule {
}
