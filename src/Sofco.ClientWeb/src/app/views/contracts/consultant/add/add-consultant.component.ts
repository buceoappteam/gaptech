import { OnInit, OnDestroy, Component } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription, from } from "rxjs";
import { UtilsService } from "app/services/common/utils.service"
import { CustomerService } from "app/services/contracts/customer.service"
import { Router, ActivatedRoute } from "@angular/router";
import { MessageService } from "app/services/common/message.service";

@Component({
    selector: 'add-consultant',
    templateUrl: './add-consultant.component.html',
    styleUrls: ['./add-consultant.component.scss']
})
export class AddConsultantComponent implements OnInit, OnDestroy {

    getCountriesSubscrip: Subscription
    getProvincesSubscrip: Subscription
    AddConsultantSubscrip: Subscription
    UpdateConsultantSubscrip: Subscription
    ParamsSubscrip: Subscription;
    GetSubscrip: Subscription;

    consultantForm: FormGroup
    model: any

    provinces: any[] = new Array()
    countries: any[] = new Array()
    private id: number;
    private isEdit = false;

    formErrors = {
        'name': '',
        'cuit': '',
        'telefone': '',
        'city': '',
        'province': ''
    };

    validationMessages = {}

    constructor(private fb: FormBuilder,
        private translate: TranslateService,
        private utilsService: UtilsService,
        private customerService: CustomerService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageService) {

    }

    ngOnInit(): void {

        this.getCountries()

        this.ParamsSubscrip = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
            if (this.id) {
                this.isEdit = true
                this.getDetails();
            }
            else {
            }
        });

        this.consultantForm = this.fb.group({
            name: ['', Validators.required],
            cuit: ['', Validators.required],
            telephone: ['', Validators.required],
            address: [''],
            postalCode: [''],
            city: ['', Validators.required],
            country: ['', Validators.required],
            province: ['', Validators.required],
            contact: ['']
        })

        this.validationMessages = {
            'name': {
                'required': this.translate.get('requiredField'),
            },
            'cuit': {
                'required': this.translate.get('requiredField'),
            },
            'telephone': {
                'required': this.translate.get('requiredField'),
            },
            'country': {
                'required': this.translate.get('requiredField'),
            },
            'city': {
                'required': this.translate.get('requiredField'),
            },
            'province': {
                'required': this.translate.get('requiredField'),
            },
        };

        this.consultantForm.valueChanges.subscribe(data => {
            this.logValidationErrors();

        })

        this.consultantForm.get('country').valueChanges.subscribe(data => {
            this.getProvinces(data.id)
        })

    }

    ngOnDestroy(): void {
        if (this.getProvincesSubscrip) this.getProvincesSubscrip.unsubscribe();
        if (this.getCountriesSubscrip) this.getCountriesSubscrip.unsubscribe();
        if (this.AddConsultantSubscrip) this.AddConsultantSubscrip.unsubscribe();
        if (this.UpdateConsultantSubscrip) this.UpdateConsultantSubscrip.unsubscribe();
        if (this.ParamsSubscrip) this.ParamsSubscrip.unsubscribe();
        if (this.GetSubscrip) this.GetSubscrip.unsubscribe();
    }

    save(): void {

        if (this.consultantForm.valid) {
            if (this.isEdit) {
                this.updateConsultant()
            }
            else {
                this.AddConsultant()
            }
        }
        else {
            this.markDirty()
            this.logValidationErrors(this.consultantForm)
        }
    }

    getProvinces(countryId) {
        this.messageService.showLoading();
        this.getProvincesSubscrip = this.utilsService.getProvinces(countryId).subscribe(data => {
            this.messageService.closeLoading()

            this.provinces = data;
        },
            error => {
                this.messageService.closeLoading();
            })
    }

    getCountries() {
        this.messageService.showLoading();
        this.getCountriesSubscrip = this.utilsService.getCountries().subscribe(data => {
            this.messageService.closeLoading()

            this.countries = data;
        },
            error => {
                this.messageService.closeLoading();
            });
    }

    logValidationErrors(group: FormGroup = this.consultantForm): void {
        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);
            this.formErrors[key] = '';
            if (abstractControl && !abstractControl.valid
                && (abstractControl.touched || abstractControl.dirty)) {
                const messages = this.validationMessages[key];
                for (const errorKey in abstractControl.errors) {
                    if (errorKey) {
                        this.formErrors[key] += messages[errorKey].value + ' ';
                    }
                }
            }
        });
    }

    markDirty(group: FormGroup = this.consultantForm): void {
        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);
            abstractControl.markAsDirty();
        });
    }

    mapFormValuesToModel() {
        this.model = {
            name: this.consultantForm.value.name,
            cuit: this.consultantForm.value.cuit,
            telephone: this.consultantForm.value.telephone,
            address: this.consultantForm.value.address,
            postalCode: this.consultantForm.value.postalCode,
            city: this.consultantForm.value.city,
            provinceId: this.consultantForm.value.province.id,
            contact: this.consultantForm.value.contact,
            active: true
        }
    }

    getDetails() {
        this.messageService.showLoading();

        this.GetSubscrip = this.customerService.getById(this.id).subscribe(
            response => {

                this.getProvincesSubscrip = this.utilsService.getProvinces(response.data.province.countryId).subscribe(data => {
                    this.messageService.closeLoading()

                    this.provinces = data;

                    this.consultantForm.patchValue({
                        name: response.data.name,
                        cuit: response.data.cuit,
                        telephone: response.data.telephone,
                        address: response.data.address,
                        postalCode: response.data.postalCode,
                        city: response.data.city,
                        country: this.countries.find(x => x.id == response.data.province.countryId),
                        province: this.provinces.find(x => x.id == response.data.province.id),
                        contact: response.data.contact
                    })

                })
            },
            error => this.messageService.closeLoading());
    }

    AddConsultant() {
        this.messageService.showLoading();
        this.mapFormValuesToModel()
        this.AddConsultantSubscrip = this.customerService.add(this.model).subscribe(
            response => {
                this.messageService.closeLoading()
                this.router.navigate(["/management/consultant"]);
            },
            error => { this.messageService.closeLoading(); }
        )
    }

    updateConsultant() {
        this.messageService.showLoading();
        this.mapFormValuesToModel()
        this.UpdateConsultantSubscrip = this.customerService.updateConsultant(this.model, this.id).subscribe(
            response => {
                this.messageService.closeLoading()
                this.router.navigate(["/management/consultant"]);
            },
            error => { this.messageService.closeLoading(); }
        )
    }

}