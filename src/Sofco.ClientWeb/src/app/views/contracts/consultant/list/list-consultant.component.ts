import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MenuService } from 'app/services/admin/menu.service';
import { DataTableService } from 'app/services/common/datatable.service';
import { MessageService } from 'app/services/common/message.service';
import { CustomerService } from 'app/services/contracts/customer.service';

declare var moment: any;

@Component({
  selector: 'app-list-consultant',
  templateUrl: './list-consultant.component.html',
  styleUrls: ['./list-consultant.component.scss']
})
export class ListConsultantComponent implements OnInit, OnDestroy {

  public getConsultantSubscrip: Subscription;
  public activateSubscrip: Subscription;

  public consultants: any[] = new Array();


  constructor(private router: Router,
    public menuService: MenuService,
    private dataTableService: DataTableService,
    private messageService: MessageService,
    private customerService: CustomerService) { }

  ngOnInit(): void {
    this.getAll();
  }

  ngOnDestroy(): void {
    if (this.getConsultantSubscrip) this.getConsultantSubscrip.unsubscribe();
    if (this.activateSubscrip) this.activateSubscrip.unsubscribe();
  }

  getAll() {
    this.messageService.showLoading();

    this.getConsultantSubscrip = this.customerService.getAll().subscribe(
      response => {
        this.messageService.closeLoading();
        this.consultants = response.data;
        this.initGrid();
      },
      error => this.messageService.closeLoading());
  }

  initGrid() {
    var columns = [0, 1, 2, 3];
    var title = `Areas-${moment(new Date()).format("YYYYMMDD")}`;

    var params = {
      selector: '#consultantTable',
      columns: columns,
      title: title,
      withExport: true
    }

    this.dataTableService.destroy(params.selector);
    this.dataTableService.initialize(params);
  }

  goToDetail(id) {
    this.router.navigate([`/management/consultant/${id}/edit`]);
  }

  habInhabClick(consultant) {
    this.messageService.showLoading();
    var newState = !consultant.active;

    this.activateSubscrip = this.customerService.active(consultant.id, newState).subscribe(
      response => {
        this.messageService.closeLoading();
        consultant.active = newState;
      },
      error => {
        this.messageService.closeLoading();
      });

  }

}
