import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";
import { AnalyticService } from "../../../../services/contracts/analytic.service";
import { DataTableService } from "../../../../services/common/datatable.service";
import { MenuService } from "../../../../services/admin/menu.service";
import { MessageService } from "../../../../services/common/message.service";
import { I18nService } from "../../../../services/common/i18n.service";
import { CustomerService } from "../../../../services/contracts/customer.service";
import { EmployeeService } from "../../../../services/resources/employee.service";
import * as FileSaver from "file-saver";
declare var moment: any;

@Component({
    selector: 'analytic-search',
    templateUrl: './analytic-search.component.html'
})

export class AnalyticSearchComponent implements OnInit, OnDestroy {

    public analytics: any[] = new Array();
    public customers: any[] = new Array();
    public analyticStatus: any[] = new Array();
    public managers: any[] = new Array();
    public directors: any[] = new Array();
    public analyticId: any;
    public customerId: any;
    public analyticStatusId: any;
    public managerId: any;
    public directorId: any;

    public model: any[] = new Array<any>();
    public resources: any[] = new Array<any>();
    public loading:  boolean = true;
    public loadingResources: boolean = true;
    public resource: any;

    getAllSubscrip: Subscription;
    getAllEmployeesSubscrip: Subscription;
    suscription: Subscription;

    constructor(private analyticService: AnalyticService,
                private customerService: CustomerService,
                private employeeService: EmployeeService,
                private router: Router,
                private i18nService: I18nService,
                public menuService: MenuService,
                private messageService: MessageService,
                private dataTableService: DataTableService){
    }

    ngOnInit(): void {
        const data = JSON.parse(sessionStorage.getItem('analyticSearchCriteria'));
        if (data) {
            this.analyticId = data.analyticId;
            this.customerId = data.customerId;
            this.analyticStatusId = data.analyticStatusId;
            this.managerId = data.managerId;
            this.directorId = data.directorId;
        }
        
        this.getCustomers();
        this.getAnalytics();
        this.getAnalyticStatus();
        this.getManagers();
        this.getDirectors();
    }

    getAnalytics() {
        this.getAllSubscrip = this.analyticService.getAll().subscribe(data => {
            this.analytics = this.mapAnalyticToSelect(data);
            this.loading = false;
            this.searchCriteriaChange();
        });
    }

    ngOnDestroy(): void {
        if(this.getAllSubscrip) this.getAllSubscrip.unsubscribe();
    }

    gotToEdit(analytic){
        this.router.navigate([`/management/contracts/${analytic.id}/edit`]);
    }

    gotToResources(analytic){
        sessionStorage.setItem('analyticName', analytic.title + ' - ' + analytic.name);
        this.router.navigate([`/management/contracts/${analytic.id}/employees`]);
    }

    goToAssignResource(analytic){
        if(this.menuService.hasFunctionality('ALLOCATION', 'ASSIGN')){
            sessionStorage.setItem("analytic", JSON.stringify(analytic));
            this.router.navigate([`/management/contracts/${analytic.id}/allocations`]);
        }
        else{
            this.messageService.showError("allocationManagement.allocation.forbidden");
        }
    }

    goToAdd(){
        this.router.navigate(['/management/contracts/new']);
    }
 
    getStatus(analytic){
        switch(analytic.status){
            case 1: return this.i18nService.translateByKey("allocationManagement.analytics.status.open");
            case 2: return this.i18nService.translateByKey("allocationManagement.analytics.status.close");
            case 3: return this.i18nService.translateByKey("allocationManagement.analytics.status.closeForExpenses");
        }
    }

    collapse() {
        if ($("#collapseOne").hasClass('in')) {
            $("#collapseOne").removeClass('in');
        } else {
            $("#collapseOne").addClass('in');
        }
        this.changeIcon();
    }

    changeIcon() {
        if ($("#collapseOne").hasClass('in')) {
            $("#search-icon").toggleClass('fa-caret-down').toggleClass('fa-caret-up');
        } else {
            $("#search-icon").toggleClass('fa-caret-up').toggleClass('fa-caret-down');
        }
    }

    mapAnalyticToSelect(data: Array<any>): Array<any> {
        data.forEach(x => x.text = x.title + ' - ' + x.name);
        return data;
    }

    getCustomers() {
        this.customerService.getAllOptions().subscribe(d => {
            this.customers = d.data;
        });
    }

    customerChange() {
        this.searchCriteriaChange();
    }

    getAnalyticStatus() {
        const openStatus = 'allocationManagement.analytics.status.open';
        const closeStatus = 'allocationManagement.analytics.status.close';
        const closeForExpensesStatus = 'allocationManagement.analytics.status.closeForExpenses';

        this.analyticStatus = [
            { 'id': 1, 'text': openStatus},
            { 'id': 2, 'text': closeStatus},
            { 'id': 3, 'text': closeForExpensesStatus},
        ];
    }

    getManagers() {
        this.suscription = this.employeeService.getManagers().subscribe(data => {
            this.managers = data;
        });
    }

    getDirectors() {
        this.suscription = this.employeeService.getDirectors().subscribe(data => {
            this.directors = data;
        });
    }

    searchCriteriaChange() {
        const searchCriteria = {
            analyticId: this.analyticId,
            customerId: this.customerId,
            analyticStatusId: this.analyticStatusId,
            managerId: this.managerId,
            directorId: this.directorId,
        };

        this.getAnalyticsBySearchCriteria(searchCriteria);
    }

    getAnalyticsBySearchCriteria(searchCriteria) {
        this.getAllSubscrip = this.analyticService.get(searchCriteria).subscribe(res => {
            this.model = res.data;
            this.loading = false;
 
            var columns = [0, 1, 2, 3, 4, 5];
            var title = `Analiticas-${moment(new Date()).format("YYYYMMDD")}`;

            const options = {
                selector: "#analyticsTable",
                columns: columns,
                title: title,
                withExport: true
            };

            this.dataTableService.destroy(options.selector);
            this.dataTableService.initialize(options);
            this.storeSearchCriteria(searchCriteria);
        });
    }

    storeSearchCriteria(searchCriteria) {
        sessionStorage.setItem('analyticSearchCriteria', JSON.stringify(searchCriteria));
    }

    export(){
        const ids = this.model.map(item => item.id);

        this.messageService.showLoading();

        this.analyticService.createReport(ids).subscribe(file => {
            this.messageService.closeLoading();
            FileSaver.saveAs(file, `Reporte Analiticas.xlsx`);
        },
        err => {
            this.messageService.closeLoading();
        });
    }

    clean() {
        this.customerId = null;
        this.analyticId = null;
        this.analyticStatusId = null;
        this.managerId = null;
        this.directorId = null;

        sessionStorage.removeItem('analyticSearchCriteria');

        this.searchCriteriaChange();
    }
}
