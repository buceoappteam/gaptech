import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { MenuService } from "../../../services/admin/menu.service";
import { MessageService } from "../../../services/common/message.service";
import { AllocationService } from "../../../services/resources/allocation.service";
import { Ng2ModalConfig } from "../../../components/modal/ng2modal-config";
import { EmployeeService } from "../../../services/resources/employee.service";
import { UserService } from "../../../services/admin/user.service";
import { CategoryService } from "../../../services/admin/category.service";

declare var $: any;

@Component({
    selector: 'resource-by-analytic',
    templateUrl: './resource-by-analytic.component.html',
    styleUrls: ['./resource-by-analytic.component.scss']
})

export class ResourceByAnalyticComponent implements OnInit, OnDestroy {

    @ViewChild('categoriesModal') categoriesModal;
    public categoriesModalConfig: Ng2ModalConfig = new Ng2ModalConfig(
        "ADMIN.category.list",
        "categoriesModal",
        true,
        true,
        "ACTIONS.ACCEPT",
        "ACTIONS.cancel"
    ); 
    
    public resources: any[] = new Array<any>();
    public categories: any[] = new Array<any>();
    public users: any[] = new Array<any>();
    public analyticName: string;
    analyticId: number;

    public endDate: Date = new Date();
    public resourceSelected: any;

    getAllSubscrip: Subscription;
    paramsSubscrip: Subscription;
    getUsersSubscrip: Subscription;
    getCategorySubscrip: Subscription;
    addCategoriesSubscrip: Subscription;
    public pendingWorkingHours = false;

    constructor(private router: Router,
                public menuService: MenuService,
                private messageService: MessageService,
                private employeeService: EmployeeService,
                private usersService: UserService,
                private categoryService: CategoryService,
                private activatedRoute: ActivatedRoute,
                private allocationervice: AllocationService){
    }

    ngOnInit(): void {
        this.paramsSubscrip = this.activatedRoute.params.subscribe(params => {
            this.analyticId = params['id'];
            this.analyticName = sessionStorage.getItem('analyticName');
            this.getAll();
          });

        this.getUsersSubscrip = this.usersService.getOptions().subscribe(data => {  
            this.users = data;
        });

        this.getCategories();
    }

    ngOnDestroy(): void {
        if(this.getAllSubscrip) this.getAllSubscrip.unsubscribe();
        if(this.paramsSubscrip) this.paramsSubscrip.unsubscribe();
        if(this.getUsersSubscrip) this.getUsersSubscrip.unsubscribe();
        if(this.getCategorySubscrip) this.getCategorySubscrip.unsubscribe();
        if(this.addCategoriesSubscrip) this.addCategoriesSubscrip.unsubscribe();
    }
 
    getAll(){
        this.messageService.showLoading();

        this.getAllSubscrip = this.allocationervice.getAllocationsByAnalytic(this.analyticId).subscribe(data => {
            this.resources = data;
            this.messageService.closeLoading();
        },
        () => this.messageService.closeLoading());
    }
 
    goToProfile(resource){
        this.router.navigate([`/resources/${resource.id}`]);
    }

    canViewProfile(){
        return this.menuService.hasFunctionality('PROFILE', 'VIEW');
    }

    goToAssignAnalytics(resource){
        sessionStorage.setItem("resource", JSON.stringify(resource));
        this.router.navigate([`/resources/${resource.id}/allocations`]);
    }

    openCategoryModal(resource){
        this.resourceSelected = resource;

        this.categories.forEach(category => {
            category.selected = false;
        });

        if(this.resourceSelected.categories && this.resourceSelected.categories.length > 0){
            this.resourceSelected.categories.forEach(resourceCategoryId => {
            
                var category = this.categories.find(x => x.id == resourceCategoryId);

                if(category) category.selected = true;
            });
        }

        this.categoriesModal.show();
    }

    getCategories(){
        this.getCategorySubscrip = this.categoryService.getOptions().subscribe(
            data => {
                this.categories = data.map(x => {
                    x.selected = false;
                    return x;
                });
            });
    }

    addCategoryDisabled(){
        return this.resources.filter(x => x.selected == true).length == 0;
    }

    saveCategories(){
        var categoriesSelected = this.categories.filter(x => x.selected == true).map(item => item.id);
        var usersSelected = [this.resourceSelected.id];

        var json = {
            categoriesToAdd: categoriesSelected,
            employees: usersSelected,
            clean: true
        }

        this.addCategoriesSubscrip = this.employeeService.addCategories(json).subscribe(response => {
            this.messageService.closeLoading();
            this.categoriesModal.hide();

            this.resourceSelected.categories = categoriesSelected;
        },
        () => {
            this.messageService.closeLoading();
            this.categoriesModal.hide();
        });
    }

    canAddCategories(){
        return this.menuService.hasFunctionality('RESOURCES', 'ADD-CATEGORY');
    }
}