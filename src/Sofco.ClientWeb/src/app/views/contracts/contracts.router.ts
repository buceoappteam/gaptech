import { Routes, RouterModule } from "@angular/router";
import { AnalyticSearchComponent } from "./analytics/search/analytic-search.component";
import { NewAnalyticComponent } from "./analytics/new/new-analytic.component";
import { EditAnalyticComponent } from "./analytics/edit/edit-analytic.component";
import { AddAllocationComponent } from "../resources/allocation/add-by-analytic/add-by-analytic.component";
import { ResourceByAnalyticComponent } from "./by-analytic/resource-by-analytic.component";
import { ListCostCenterComponent } from "./cost-center/list/list-cost-center.component";
import { AddCostCenterComponent } from "./cost-center/add/add-cost-center.component";
import { EditCostCenterComponent } from "./cost-center/edit/edit-cost-center.component";
import { AuthGuard } from "../../guards/auth.guard";
import { AddConsultantComponent } from "./consultant/add/add-consultant.component";
import { ListConsultantComponent } from "./consultant/list/list-consultant.component";
import { UserDelegateComponent } from "./userDelegate/userdelegate";

const CONTRACTS_ROUTER: Routes = [
    { path: "contracts",
        children: [
        { path: "", component: AnalyticSearchComponent, canActivate: [AuthGuard], data: { module: "CONTRACTS", functionality: "LIST" } },
        { path: "new", component: NewAnalyticComponent, canActivate: [AuthGuard], data: { module: "CONTRACTS", functionality: "ADD" } },
        { path: ":id/edit", component: EditAnalyticComponent, canActivate: [AuthGuard], data: { module: "CONTRACTS", functionality: "UPDATE" } },
        { path: ":id/allocations", component: AddAllocationComponent, canActivate: [AuthGuard], data: { module: "ALLOCATION", functionality: "ASSIGN" } },
        { path: ":id/employees", component: ResourceByAnalyticComponent, canActivate: [AuthGuard], data: { module: "CONTRACTS", functionality: "RESOURCES" } },
    ]},
    
    {
      path: "costCenter",
      children: [
        { path: "", component: ListCostCenterComponent, canActivate: [AuthGuard], data: { module: "COSTCENTER", functionality: "LIST" } },
        { path: "add", component: AddCostCenterComponent, canActivate: [AuthGuard], data: { module: "COSTCENTER", functionality: "ADD" } },
        { path: ":id/edit", component: EditCostCenterComponent, canActivate: [AuthGuard], data: { module: "COSTCENTER", functionality: "UPDATE" } }
      ]
    },

    { path: "delegates", component: UserDelegateComponent, canActivate: [AuthGuard], data: { module: "CONTRACTS", functionality: "DELEGATES" } },
    
    {
      path: 'consultant',
      children: [
        { path: "", component: ListConsultantComponent, canActivate: [AuthGuard] },        
        { path: "add", component: AddConsultantComponent, canActivate: [AuthGuard]},
        { path: ":id/edit", component: AddConsultantComponent, canActivate: [AuthGuard]}
      ]
    }
];

export const ContractsRouter = RouterModule.forChild(CONTRACTS_ROUTER);