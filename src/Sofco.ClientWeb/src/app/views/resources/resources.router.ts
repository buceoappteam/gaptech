import { Routes, RouterModule } from "@angular/router";
import { AllocationReportComponent } from "./allocation/report/allocation-report.component";
import { AuthGuard } from "../../guards/auth.guard";
import { ResourceSearchComponent } from "./search/resource-search.component";
import { ResourceDetailComponent } from "./detail/resource-detail.component";
import { AddAllocationByResourceComponent } from "./allocation/add-by-resource/add-by-resource.component";
import { UnemployeesSearchComponent } from "./search-unemployees/unemployees-search.component";

const ALLOCATION_ROUTER: Routes = [
    { path: "allocationsReport", component: AllocationReportComponent, canActivate: [AuthGuard], data: { module: "ALLOCATION", functionality: "REPORT" } },

    { path: "unemployees", component: UnemployeesSearchComponent, canActivate: [AuthGuard], data: { module: "RESOURCES", functionality: "UNEMPLOYEES" } },

    { path: "", component: ResourceSearchComponent, canActivate: [AuthGuard], data: { module: "RESOURCES", functionality: "LIST" } },

    { path: ":id", component: ResourceDetailComponent, canActivate: [AuthGuard], data: { fromRrhh: true, module: "PROFILE", functionality: "VIEW" } },

    { path: ":id/allocations", component: AddAllocationByResourceComponent, canActivate: [AuthGuard], data: { module: "ALLOCATION", functionality: "ASSIGN" } },
];

export const AllocationRouter = RouterModule.forChild(ALLOCATION_ROUTER);