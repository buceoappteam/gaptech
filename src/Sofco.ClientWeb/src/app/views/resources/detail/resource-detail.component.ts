import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { MenuService } from "../../../services/admin/menu.service";
import { MessageService } from "../../../services/common/message.service";
import { EmployeeService } from "../../../services/resources/employee.service";
import { DataTableService } from "../../../services/common/datatable.service";
import { Ng2ModalConfig } from "../../../components/modal/ng2modal-config";
import { UserInfoService } from "../../../services/common/user-info.service";
import { UserService } from "../../../services/admin/user.service";
import { I18nService } from "../../../services/common/i18n.service";
import { WorkflowStateType } from "app/models/enums/workflowStateType";

@Component({
    selector: 'resource-detail',
    templateUrl: './resource-detail.component.html',
    styleUrls: ['./resource-detail.component.scss']
})
export class ResourceDetailComponent implements OnInit, OnDestroy {

    @ViewChild('businessHoursModal') businessHoursModal;
    public businessHoursModalConfig: Ng2ModalConfig = new Ng2ModalConfig(
        "allocationManagement.resources.hoursByContract",
        "businessHoursModal",
        false,
        true,
        "ACTIONS.ACCEPT",
        "ACTIONS.cancel"
    ); 

    resourceId: number;
    employeeLoggedId: number;
    userLoggedId: number;
    public model: any;

    public isManager: boolean = false;

    public editModel = {
        businessHours: 0,
        businessHoursDescription: "",
        office: "",
        managerId: null,
        billingPercentage: 0,
        holidaysPending: null,
        hasCreditCard: false
    }

    public tasks: any[] = new Array();

    getSubscrip: Subscription;
    paramsSubscrip: Subscription;
    putSubscrip: Subscription;
    getDataSubscrip: Subscription;
    getTasksSubscrip: Subscription;

    constructor(private router: Router,
                private i18nService: I18nService,
                private menuService: MenuService,
                private messageService: MessageService,
                private userService: UserService,
                private dataTableService: DataTableService,
                private activatedRoute: ActivatedRoute,
                private employeeService: EmployeeService){

        this.businessHoursModalConfig.acceptInlineButton = true;
    }

    ngOnInit(): void {
        this.paramsSubscrip = this.activatedRoute.params.subscribe(params => {
            this.resourceId = params['id'];

            var data = <any>JSON.stringify(this.activatedRoute.snapshot.data);
            var dataJson = JSON.parse(data);

            if (!dataJson.fromRrhh) {
                const userInfo = UserInfoService.getUserInfo();

                if (userInfo && userInfo.employeeId && userInfo.employeeId != this.resourceId) {
                    this.router.navigate([`/403`]);
                    return;
                }

                this.userLoggedId = userInfo.id;
                this.employeeLoggedId = userInfo.employeeId;
            }

            this.getProfile();
            this.getTasks();
        });
    }

    ngOnDestroy(): void {
        if (this.getSubscrip) { this.getSubscrip.unsubscribe(); }
        if (this.paramsSubscrip) { this.paramsSubscrip.unsubscribe(); }
        if (this.getDataSubscrip) { this.getDataSubscrip.unsubscribe(); }
        if (this.putSubscrip) { this.putSubscrip.unsubscribe(); }
        if (this.getTasksSubscrip) { this.getTasksSubscrip.unsubscribe(); }
    }

    getProfile(){
        this.messageService.showLoading();

        this.getSubscrip = this.employeeService.getProfile(this.resourceId).subscribe(response => {
            this.model = response.data;

            this.editModel.businessHours = response.data.businessHours;
            this.editModel.businessHoursDescription = response.data.businessHoursDescription;
            this.editModel.office = response.data.officeAddress;
            this.editModel.billingPercentage = response.data.percentage;

            this.messageService.closeLoading();
            this.initGrid();
        },
        () => this.messageService.closeLoading());
    }

    initGrid(){
        const options = { selector: '#analyticsTable', columnDefs: [ {'aTargets': [4,5], "sType": "date-uk"} ], order: [[ 4, "desc" ]] };
        this.dataTableService.initialize(options);
    }

    goToTimeSheet(){
        this.router.navigate([`/profile/workTime`]);
    }

    getTasks(){
        this.getDataSubscrip = this.employeeService.getTasks(this.resourceId).subscribe(response => {
            this.tasks = response.data;

            var params = { selector: "#tasksTable" };
    
            this.dataTableService.initialize(params);
        });
    }

    update(){
        var json = {
            businessHours: this.editModel.businessHours,
            businessHoursDescription: this.editModel.businessHoursDescription,
            office: this.editModel.office,
            billingPercentage: this.editModel.billingPercentage,
        };

        this.putSubscrip = this.employeeService.put(this.resourceId, json).subscribe(data => {
            this.businessHoursModal.hide();

            setTimeout(() => {
                window.location.reload();
            }, 750);
        },
        () => this.businessHoursModal.resetButtons());
    }

    getFormattedPhone() {
        if(this.model == null) return "";

        const phoneCountryCode = this.model.phoneCountryCode;
        const phoneAreaCode = this.model.phoneAreaCode;
        const phoneNumber = this.model.phoneNumber;

        return "+"+phoneCountryCode+" "+phoneAreaCode+" "+phoneNumber;
    }

    lowerizeFirstLetter(txt) {
        return txt.charAt(0).toLowerCase() + txt.slice(1);
    }

    resolveReference(data:any[]) {
        const self = this;

        data.forEach(x => {
            const fields = JSON.parse(x.fields);
            fields.forEach(item => {
                const key = self.lowerizeFirstLetter(item);
                if(key === 'managerId')
                {
                    self.setManagerReference(x);
                }
            });
        });
    }

    setManagerReference(item)
    {
        item.employee.managerId = item.employee.manager.name;
        item.employeePrevious.managerId = item.employeePrevious.manager.name;
    }

    getStatusClass(type){
        switch(type){
            case WorkflowStateType.Info: return "label-success";
            case WorkflowStateType.Warning: return "label-warning";
            case WorkflowStateType.Success: return "label-primary";
            case WorkflowStateType.Danger: return "label-danger";
        }
    }
    
    userLoggedIsManager(){
        return this.editModel.managerId == this.userLoggedId;
    }
}
