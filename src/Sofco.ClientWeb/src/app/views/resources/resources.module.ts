import { FormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { PeityModule } from '../../components/charts/peity';
import { TranslateModule } from "@ngx-translate/core";
import { AnalyticService } from '../../services/contracts/analytic.service';
import { SpinnerModule } from '../../components/spinner/spinner.module';
import { Select2Module } from '../../components/select2/select2';
import { AllocationService } from '../../services/resources/allocation.service';
import { NgDatepickerModule } from 'ng2-datepicker';
import { EmployeeService } from '../../services/resources/employee.service';
import { IboxtoolsModule } from '../../components/common/iboxtools/iboxtools.module';
import { AddAllocationByResourceComponent } from './allocation/add-by-resource/add-by-resource.component';
import { ResourceSearchComponent } from './search/resource-search.component';
import { Ng2ModalModule } from '../../components/modal/ng2modal.module';
import { ICheckModule } from '../../components/icheck/icheck.module';
import { PCheckModule } from '../../components/pcheck/pcheck.module';
import { AllocationReportComponent } from './allocation/report/allocation-report.component';
import { DateRangePickerModule } from '../../components/date-range-picker/date-range.picker.module';
import { DatePickerModule } from '../../components/date-picker/date-picker.module';
import { AllocationRouter } from './resources.router';
import { CategoryService } from '../../services/admin/category.service';
import { CommonModule } from '@angular/common';
import { ResourceDetailModule } from './detail/resource-detail.module';
import { AllocationAssingTableModule } from './allocation/allocation-assignment-table/alloc-assing-table.module';
import { NumbersOnlyModule } from 'app/components/numbersOnly/numberOnly.directive';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { UnemployeesSearchComponent } from './search-unemployees/unemployees-search.component';

@NgModule({
  declarations: [
    AddAllocationByResourceComponent,
    ResourceSearchComponent, 
    AllocationReportComponent,
    UnemployeesSearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AllocationAssingTableModule,
    ResourceDetailModule,
    PeityModule,
    FormsModule,
    SpinnerModule,
    TranslateModule,
    BsDatepickerModule,
    Select2Module,
    NgDatepickerModule,
    DatePickerModule,
    DateRangePickerModule,
    IboxtoolsModule,
    ICheckModule,
    NumbersOnlyModule,
    Ng2ModalModule,
    PCheckModule,
    AllocationRouter,
    NgSelectModule
  ],
  providers: [ AnalyticService, AllocationService, EmployeeService, CategoryService ],

  exports: [],
})

export class ResourceModule {
}
