export class Employee {

    public employeeNumber: string    
    public name: string 
    public birthday: Date
    public startDate: Date 
    public endDate: Date 
    public profile: string 
    public technology: string 
    public seniority: string  
    public percentage: number
    public address: string 
    public location: string 
    public provinceId: number 
    public countryId: number
    public officeAddress: string 
    public email: string 
    public businessHours: number 
    public businessHoursDescription: string 
    public documentTypeId: number 
    public documentNumber: string 
    public cuil: string 
    public phoneCountryCode: number 
    public phoneAreaCode: number 
    public phoneNumber: number 
    public userName: string
    public userId: number;
}