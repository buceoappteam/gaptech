export const environment = {
  production: true,
  urlApi:  'http://aztech01wd-web.azurewebsites.net/api',
  draftWorkflowStateId: 8,
  gafWorkflowStateId: 10,
  dafWorkflowStateId: 4,
  rrhhWorkflowStateId: 6,
  rejectedWorkflowStateId: 2,
};