export const environment = {
  production: false,
  urlApi: 'http://localhost:9696/api',
  draftWorkflowStateId: 8,
  gafWorkflowStateId: 10,
  dafWorkflowStateId: 4,
  rrhhWorkflowStateId: 6,
  rejectedWorkflowStateId: 2,
}; 