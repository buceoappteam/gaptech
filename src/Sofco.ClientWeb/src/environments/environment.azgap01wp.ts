export const environment = {
  production: true,
  urlApi:  'https://azgap01wp-web.azurewebsites.net/api',
  draftWorkflowStateId: 8,
  gafWorkflowStateId: 10,
  rejectedWorkflowStateId: 2,
  dafWorkflowStateId: 4,
  rrhhWorkflowStateId: 6,
};