﻿using System;
using System.Collections.Generic;
using Sofco.Common.Domains;
using Sofco.Domain.Interfaces;
using Sofco.Domain.Models.Common;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Models.WorkTimeManagement;
using Sofco.Domain.Relationships;

namespace Sofco.Domain.Models.Admin
{
    public class User : BaseEntity, ILogicalDelete, IAuditDates
    {
        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public bool Active { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Employee Employee { get; set; }

        public ICollection<Analytic> Analytics1 { get; set; }
        public ICollection<Analytic> Analytics2 { get; set; }

        public ICollection<WorkTime> WorkTimes { get; set; }

        public IList<Workflow.Workflow> Workflows { get; set; }
        public IList<Workflow.Workflow> Workflows2 { get; set; }

        public IList<Workflow.WorkflowReadAccess> WorkflowReadAccesses { get; set; }
        public IList<Workflow.WorkflowReadAccess> WorkflowReadAccesses2 { get; set; }

        public IList<Workflow.WorkflowState> WorkflowStates { get; set; }
        public IList<Workflow.WorkflowState> WorkflowStates2 { get; set; }

        public IList<Workflow.WorkflowStateAccess> WorkflowStateAccesses { get; set; }
        public IList<Workflow.WorkflowStateAccess> WorkflowStateAccesses2 { get; set; }

        public IList<Workflow.WorkflowStateNotifier> WorkflowStateNotifiers { get; set; }
        public IList<Workflow.WorkflowStateNotifier> WorkflowStateNotifiers2 { get; set; }

        public IList<Workflow.WorkflowStateTransition> WorkflowStateTransitions { get; set; }
        public IList<Workflow.WorkflowStateTransition> WorkflowStateTransitions2 { get; set; }

        public IList<Workflow.WorkflowType> WorkflowTypes2 { get; set; }
        public IList<Workflow.WorkflowType> WorkflowTypes { get; set; }

        public IList<UserRole> UserRoles { get; set; }

        public IList<UserDelegate> UserDelegates1 { get; set; }
        public IList<UserDelegate> UserDelegates2 { get; set; }
    }
}
