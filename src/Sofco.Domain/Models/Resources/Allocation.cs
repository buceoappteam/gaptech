﻿using System;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Domain.Models.Resources
{
    public class Allocation : BaseEntity
    {
        public DateTime StartDate { get; set; }

        public decimal Percentage { get; set; }

        public int AnalyticId { get; set; }

        public Analytic Analytic { get; set; }

        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
