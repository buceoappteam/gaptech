﻿using System;
using System.Collections.Generic;
using Sofco.Common.Domains;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Models.WorkTimeManagement;
using Sofco.Domain.Relationships;
using Sofco.Domain.Utils;

namespace Sofco.Domain.Models.Resources
{
    public class Employee : BaseEntity, IEntityDate
    {
        public string EmployeeNumber { get; set; }

        public string Name { get; set; }

        public DateTime? Birthday { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Profile { get; set; }

        public string Technology { get; set; }

        public string Seniority { get; set; }

        public decimal BillingPercentage { get; set; }

        public ICollection<Allocation> Allocations { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

        public string CreatedByUser { get; set; }

        public string Address { get; set; }

        public string Location { get; set; }

        public int ProvinceId { get; set; }
        public Province Province { get; set; }

        public string OfficeAddress { get; set; }

        public string Email { get; set; }

        public int BusinessHours { get; set; }

        public string BusinessHoursDescription { get; set; }

        public IList<EmployeeCategory> EmployeeCategories { get; set; }

        public IList<WorkTime> WorkTimes { get; set; }

        public int? DocumentTypeId { get; set; }
        public DocumentType DocumentType { get; set; }

        public string DocumentNumber { get; set; }

        public string Cuil { get; set; }

        public int PhoneCountryCode { get; set; }

        public int PhoneAreaCode { get; set; }

        public string PhoneNumber { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
