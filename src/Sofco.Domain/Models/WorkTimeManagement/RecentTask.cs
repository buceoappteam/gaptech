﻿namespace Sofco.Domain.Models.WorkTimeManagement
{
    public class RecentTask : BaseEntity
    {
        public string Analytic { get; set; }

        public int AnalyticId { get; set; }

        public int CategoryId { get; set; }

        public decimal Hours { get; set; }

        public string Reference { get; set; }

        public string Task { get; set; }

        public int TaskId { get; set; }

        public int UserId { get; set; }
    }
}
