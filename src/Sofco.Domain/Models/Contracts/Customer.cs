﻿using System.Collections;
using System.Collections.Generic;
using Sofco.Domain.Utils;

namespace Sofco.Domain.Models.Contracts
{
    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Cuit { get; set; }

        public string Telephone { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public int ProvinceId { get; set; }
        public Province Province { get; set; }

        public string Contact { get; set; }

        public bool Active { get; set; }

        public IList<Analytic> Analytics { get; set; }
    }
}
