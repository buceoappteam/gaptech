﻿using System;
using System.Collections.Generic;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Models.WorkTimeManagement;
using Sofco.Domain.Utils;

namespace Sofco.Domain.Models.Contracts
{
    public class Analytic : BaseEntity
    {
        public string Title { get; set; }

        public int TitleId { get; set; }

        public int CostCenterId { get; set; }
        public CostCenter CostCenter { get; set; }

        public string Name { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public DateTime StartDateContract { get; set; }

        public DateTime EndDateContract { get; set; }

        public int ManagerId { get; set; }
        public User Manager { get; set; }

        public int DirectorId { get; set; }
        public User Director { get; set; }

        public string Proposal { get; set; }

        public AnalyticStatus Status { get; set; }

        //public int? SolutionId { get; set; }
        //public Solution Solution { get; set; }

        //public int? TechnologyId { get; set; }
        //public Technology Technology { get; set; }

        //public int? ServiceTypeId { get; set; }
        //public ServiceType ServiceType { get; set; }

        //public int? SoftwareLawId { get; set; }        
        //public SoftwareLaw SoftwareLaw { get; set; }

        public ICollection<Allocation> Allocations { get; set; }

        public ICollection<WorkTime> WorkTimes { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
