﻿using System;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Admin;

namespace Sofco.Domain.Models.Common
{
    public class UserDelegate : BaseEntity
    {
        public int UserId { get; set; }

        public User User { get; set; }

        public UserDelegateType Type { get; set; }

        public int GrantedUserId { get; set; }

        public User GrantedUser { get; set; }

        public int? AnalyticSourceId { get; set; }

        public int? UserSourceId { get; set; }

        public UserDelegateSourceType SourceType { get; set; }

        public DateTime Created { get; set; }
    }
}
