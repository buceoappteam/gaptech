﻿using System.Collections.Generic;

namespace Sofco.Domain.DTO
{
    public class EmployeeAddCategoriesParams
    {
        public IList<int> CategoriesToAdd { get; set; }
        public IList<int> Employees { get; set; }
        public bool Clean { get; set; }
    }
}
