﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sofco.Domain.Utils
{
    public class Country : Option
    {
        public IList<Province> Provinces { get; set; }
    }
}
