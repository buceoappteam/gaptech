﻿using System.Collections.Generic;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;

namespace Sofco.Domain.Utils
{
    public class Province : Option
    {
        public int CountryId { get; set; }

        public Country Country { get; set; }

        public IList<Customer> Customers { get; set; }

        public IList<Employee> Employees { get; set; }
    }
}
