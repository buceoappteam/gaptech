﻿using System.Collections.Generic;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Domain.Utils
{
    public class SoftwareLaw : Option
    {
        public ICollection<Analytic> Analytics { get; set; }
    }
}
