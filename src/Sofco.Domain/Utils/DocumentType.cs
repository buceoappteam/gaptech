﻿using Sofco.Domain.Models.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sofco.Domain.Utils
{
    public class DocumentType : Option
    {
        public IList<Employee> Employees { get; set; }
    }
}
