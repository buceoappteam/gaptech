﻿using System.Collections.Generic;

namespace Sofco.Domain.Utils
{
    public class AnalyticOptions
    {
        public IList<Option> Managers { get; set; }
        public IList<Option> Directors { get; set; }
        //public IList<Option> Solutions { get; set; }
        //public IList<Option> Technologies { get; set; }
        public IList<Option> Products { get; set; }
        //public IList<Option> SoftwareLaws { get; set; }
        //public IList<Option> ServiceTypes { get; set; }
    }
}
