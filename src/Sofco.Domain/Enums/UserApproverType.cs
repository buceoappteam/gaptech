﻿namespace Sofco.Domain.Enums
{
    public enum UserApproverType
    {
        WorkTime,

        LicenseAuthorizer,

        Refund
    }
}
