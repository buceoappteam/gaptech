﻿namespace Sofco.Domain.Enums
{
    public enum WorkTimeStatus
    {
        Draft = 1,
        Sent = 2,
        Approved = 3,
        Rejected = 4,
        SentToDirector = 5
    }
}
