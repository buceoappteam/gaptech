﻿namespace Sofco.Domain.Enums
{
    public enum ResponseStatus
    {
        NotFound = 404,
        Forbidden = 403,
        BadRequest = 300,
        Success = 200
    }
}
