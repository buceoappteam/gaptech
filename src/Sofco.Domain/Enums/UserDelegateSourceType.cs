﻿namespace Sofco.Domain.Enums
{
    public enum UserDelegateSourceType
    {
        All = 1,
        Analytic = 2,
        User = 3
    }
}
