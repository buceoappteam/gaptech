﻿namespace Sofco.Common.Settings
{
    public class AppSetting
    {
        public string Domain { get; set; }

        public string ApplicantUserSource { get; set; }

        public string RoleUserSource { get; set; }

        public string UserUserSource { get; set; }

        public string AdminRole { get; set; }

        public string ManagerRole { get; set; }

        public string DirectorRole { get; set; }

        public string HourDelegateRole { get; set; }

        public int WorkflowStatusRejectedId { get; set; }

        public string UserRole { get; set; }

        public string SupportMailKey { get; set; }
    }
}
