﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Models.Common;

namespace Sofco.DAL.Mappings.Common
{
    public static class UserDelegateMapping
    {
        public static void MapUserDelegate(this ModelBuilder builder)
        {
            // Primary Key
            builder.Entity<UserDelegate>().HasKey(_ => _.Id);
            builder.Entity<UserDelegate>().HasOne(x => x.User).WithMany(x => x.UserDelegates1).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<UserDelegate>().HasOne(x => x.GrantedUser).WithMany(x => x.UserDelegates2).HasForeignKey(x => x.GrantedUserId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
