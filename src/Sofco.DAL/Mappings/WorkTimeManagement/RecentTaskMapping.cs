﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.DAL.Mappings.WorkTimeManagement
{
    public static class RecentTaskMapping
    {
        public static void MapRecentTask(this ModelBuilder builder)
        {
            builder.Entity<RecentTask>().HasKey(_ => _.Id);
            builder.Entity<RecentTask>().Property(x => x.Analytic).HasMaxLength(100);
            builder.Entity<RecentTask>().Property(x => x.Task).HasMaxLength(100);
            builder.Entity<RecentTask>().Property(x => x.Reference).HasMaxLength(500);
        }
    }
}
