﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Utils;

namespace Sofco.DAL.Mappings.Utils
{
    public static class UtilsMapping
    {
        public static void MapUtils(this ModelBuilder builder)
        {
            // Primary Key
            builder.Entity<Solution>().HasKey(_ => _.Id);
            builder.Entity<Solution>().Property(_ => _.Text).HasMaxLength(60);

            // Primary Key
            builder.Entity<Technology>().HasKey(_ => _.Id);
            builder.Entity<Technology>().Property(_ => _.Text).HasMaxLength(60);

            // Primary Key
            builder.Entity<Product>().HasKey(_ => _.Id);
            builder.Entity<Product>().Property(_ => _.Text).HasMaxLength(60);

            // Primary Key
            builder.Entity<SoftwareLaw>().HasKey(_ => _.Id);
            builder.Entity<SoftwareLaw>().Property(_ => _.Text).HasMaxLength(60);

            // Primary Key
            builder.Entity<ServiceType>().HasKey(_ => _.Id);
            builder.Entity<ServiceType>().Property(_ => _.Text).HasMaxLength(60);

            // Primary Key
            builder.Entity<Country>().HasKey(_ => _.Id);
            builder.Entity<Country>().Property(_ => _.Text).HasMaxLength(60);
            builder.Entity<Country>().HasMany(x => x.Provinces).WithOne(x => x.Country).HasForeignKey(x => x.CountryId).OnDelete(DeleteBehavior.Restrict);

            // Primary Key
            builder.Entity<Province>().HasKey(_ => _.Id);
            builder.Entity<Province>().Property(_ => _.Text).HasMaxLength(60);
            builder.Entity<Province>().HasOne(x => x.Country).WithMany(x => x.Provinces).HasForeignKey(x => x.CountryId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Province>().HasMany(x => x.Customers).WithOne(x => x.Province).HasForeignKey(x => x.ProvinceId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
