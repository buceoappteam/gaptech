﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Relationships;

namespace Sofco.DAL.Mappings.Admin
{
    public static class UserRoleMapping
    {
        public static void MapUserRole(this ModelBuilder builder)
        {
            builder.Entity<UserRole>().HasKey(t => new { t.RoleId, t.UserId });

            builder.Entity<UserRole>()
                .HasOne(pt => pt.Role)
                .WithMany(p => p.UserRoles)
                .HasForeignKey(pt => pt.RoleId);

            builder.Entity<UserRole>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.UserRoles)
                .HasForeignKey(pt => pt.UserId);
        }
    }
}
