using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Models.Admin;

namespace Sofco.DAL.Mappings.Admin
{
    public static class UserMapping
    {
        public static void MapUsers(this ModelBuilder builder)
        {
            // Primary Key
            builder.Entity<User>().HasKey(_ => _.Id);
            builder.Entity<User>().Property(_ => _.Name).HasMaxLength(150).IsRequired();
            builder.Entity<User>().Property(_ => _.Email).HasMaxLength(150).IsRequired();
            builder.Entity<User>().Property(_ => _.UserName).HasMaxLength(150).IsRequired();

            builder.Entity<User>().HasMany(x => x.Analytics1).WithOne(x => x.Manager).HasForeignKey(x => x.ManagerId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<User>().HasMany(x => x.Analytics2).WithOne(x => x.Director).HasForeignKey(x => x.DirectorId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<User>().HasOne(x => x.Employee).WithOne(x => x.User).OnDelete(DeleteBehavior.Restrict);

            builder.Entity<User>().HasIndex(x => new { x.UserName }).IsUnique();
        }
    }
}
