﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Models.Contracts;

namespace Sofco.DAL.Mappings.Contracts
{
    public static class CustomerMapping
    {
        public static void MapCustomer(this ModelBuilder builder)
        {
            // Primary Key
            builder.Entity<Customer>().HasKey(_ => _.Id);
            builder.Entity<Customer>().Property(_ => _.Address).HasMaxLength(200);
            builder.Entity<Customer>().Property(_ => _.Cuit).HasMaxLength(200);
            builder.Entity<Customer>().Property(_ => _.City).HasMaxLength(200);
            builder.Entity<Customer>().Property(_ => _.Contact).HasMaxLength(200);
            builder.Entity<Customer>().Property(_ => _.Name).HasMaxLength(200);
            builder.Entity<Customer>().Property(_ => _.PostalCode).HasMaxLength(200);
            builder.Entity<Customer>().Property(_ => _.Telephone).HasMaxLength(200);

            builder.Entity<Customer>().HasMany(x => x.Analytics).WithOne(x => x.Customer).HasForeignKey(x => x.CustomerId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Customer>().HasOne(x => x.Province).WithMany(x => x.Customers).HasForeignKey(x => x.ProvinceId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
