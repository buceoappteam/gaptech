﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Models.Contracts;

namespace Sofco.DAL.Mappings.Contracts
{
    public static class AnalyticMapping
    {
        public static void MapAnalytic(this ModelBuilder builder)
        {
            builder.Entity<Analytic>().HasKey(_ => _.Id);
            builder.Entity<Analytic>().Property(_ => _.Name).HasMaxLength(200);
            builder.Entity<Analytic>().Property(_ => _.Proposal).HasMaxLength(500);
            builder.Entity<Analytic>().Property(_ => _.Title).HasMaxLength(150);

            builder.Entity<Analytic>().HasOne(x => x.Manager).WithMany(x => x.Analytics1).HasForeignKey(x => x.ManagerId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Analytic>().HasOne(x => x.Director).WithMany(x => x.Analytics2).HasForeignKey(x => x.DirectorId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Analytic>().HasOne(x => x.Customer).WithMany(x => x.Analytics).HasForeignKey(x => x.CustomerId).OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<Analytic>().HasOne(x => x.Technology).WithMany(x => x.Analytics).HasForeignKey(x => x.TechnologyId).OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<Analytic>().HasOne(x => x.Solution).WithMany(x => x.Analytics).HasForeignKey(x => x.SolutionId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Analytic>().HasOne(x => x.CostCenter).WithMany(x => x.Analytics).HasForeignKey(x => x.CostCenterId).OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<Analytic>().HasOne(x => x.SoftwareLaw).WithMany(x => x.Analytics).HasForeignKey(x => x.SoftwareLawId).OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<Analytic>().HasOne(x => x.ServiceType).WithMany(x => x.Analytics).HasForeignKey(x => x.ServiceTypeId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Analytic>().HasMany(x => x.WorkTimes).WithOne(x => x.Analytic).HasForeignKey(x => x.AnalyticId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
