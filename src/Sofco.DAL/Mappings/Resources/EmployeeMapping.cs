﻿using Microsoft.EntityFrameworkCore;
using Sofco.Domain.Models.Resources;

namespace Sofco.DAL.Mappings.Resources
{
    public static class EmployeeMapping
    {
        public static void MapEmployee(this ModelBuilder builder)
        {
            builder.Entity<Employee>().HasKey(_ => _.Id);
            builder.Entity<Employee>().HasIndex(u => u.EmployeeNumber).IsUnique();
            builder.Entity<Employee>().Property(x => x.Name).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.Profile).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.Seniority).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.Technology).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.CreatedByUser).HasMaxLength(50);
            builder.Entity<Employee>().Property(x => x.Address).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.Location).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.OfficeAddress).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.Email).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.BusinessHoursDescription).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.PhoneNumber).HasMaxLength(100);
            builder.Entity<Employee>().Property(x => x.Cuil).HasMaxLength(20);
            builder.Entity<Employee>().Property(x => x.DocumentNumber).HasMaxLength(20);

            builder.Entity<Employee>().HasOne(x => x.User).WithOne(x => x.Employee).HasForeignKey<Employee>(x => x.UserId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Employee>().HasMany(x => x.WorkTimes).WithOne(x => x.Employee).HasForeignKey(x => x.EmployeeId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Employee>().HasOne(x => x.Province).WithMany(x => x.Employees).HasForeignKey(x => x.ProvinceId).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Employee>().HasOne(x => x.DocumentType).WithMany(x => x.Employees).HasForeignKey(x => x.DocumentTypeId).OnDelete(DeleteBehavior.Restrict);

        }
    }
}
