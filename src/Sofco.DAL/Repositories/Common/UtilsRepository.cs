﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Common;
using Sofco.Domain.Utils;

namespace Sofco.DAL.Repositories.Common
{
    public class UtilsRepository : IUtilsRepository
    {
        protected readonly SofcoContext Context;

        public UtilsRepository(SofcoContext context)
        {
            Context = context;
        }

        public IList<Solution> GetSolutions()
        {
            return Context.Solutions.ToList().AsReadOnly();
        }

        public IList<Technology> GetTechnologies()
        {
            return Context.Technologies.ToList().AsReadOnly();
        }

        public IList<Product> GetProducts()
        {
            return Context.Products.ToList().AsReadOnly();
        }

        public IList<ServiceType> GetServiceTypes()
        {
            return Context.ServiceTypes.ToList().AsReadOnly();
        }

        public IList<SoftwareLaw> GetSoftwareLaws()
        {
            return Context.SoftwareLaws.ToList().AsReadOnly();
        }

        public IList<Province> GetProvinces(int countryId)
        {
            return Context.Provinces.Where(x => x.CountryId == countryId).ToList().AsReadOnly();
        }

        public IList<Country> GetCountries()
        {
            return Context.Countries.ToList().AsReadOnly();
        }

        public IList<DocumentType> GetDocumentTypes()
        {
            return Context.DocumentType.ToList().AsReadOnly();
        }
    }
}
