﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Common;
using Sofco.Core.Models.Common;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Common;

namespace Sofco.DAL.Repositories.Common
{
    public class UserDelegateRepository : BaseRepository<UserDelegate>, IUserDelegateRepository
    {
        public UserDelegateRepository(SofcoContext context) : base(context)
        {
        }

        public bool Exist(UserDelegateAddModel model, int userId)
        {
            if (model.AnalyticSourceId.HasValue && model.AnalyticSourceId.Value > 0)
            {
                if (model.UserSourceId.HasValue && model.UserSourceId.Value > 0)
                {
                    return context.UserDelegates.Any(x => x.UserId == userId && 
                                                          x.GrantedUserId == model.GrantedUserId && 
                                                          x.Type == model.Type && 
                                                          x.AnalyticSourceId.Value == model.AnalyticSourceId.Value && 
                                                          x.UserSourceId.Value == model.UserSourceId.Value && 
                                                          x.SourceType == model.SourceType);
                }
                else
                {
                    return context.UserDelegates.Any(x => x.UserId == userId && 
                                                          x.GrantedUserId == model.GrantedUserId &&
                                                          x.Type == model.Type &&
                                                          x.AnalyticSourceId.Value == model.AnalyticSourceId.Value &&
                                                          x.SourceType == model.SourceType);
                }
            }
            else
            {
                return context.UserDelegates.Any(x => x.UserId == userId && x.GrantedUserId == model.GrantedUserId && x.Type == model.Type);
            }
        }

        public IList<UserDelegate> GetByUserId(int userId)
        {
            return context.UserDelegates.Include(x => x.User).Include(x => x.GrantedUser).Where(x => x.UserId == userId).ToList();
        }

        public IList<UserDelegate> GetByGrantedUserIdAndType(int currentUserId, UserDelegateType type)
        {
            return context.UserDelegates.Where(x => x.GrantedUserId == currentUserId && x.Type == type).ToList();
        }
    }
}
