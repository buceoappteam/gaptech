﻿using System.Collections.Generic;
using System.Linq;
using Sofco.Core.DAL.WorkTimeManagement;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.DAL.Repositories.WorkTimeManagement
{
    public class RecentTaskRepository : BaseRepository<RecentTask>, IRecentTaskRepository
    {
        public RecentTaskRepository(SofcoContext context) : base(context)
        {
        }

        public bool Exist(int taskId, int analyticId)
        {
            return context.RecentTasks.Any(x => x.AnalyticId == analyticId && x.TaskId == taskId);
        }

        public IList<RecentTask> GetByUser(int currentUserId)
        {
            return context.RecentTasks.Where(x => x.UserId == currentUserId).ToList();
        }
    }
}
