﻿using System;
using System.Collections.Generic;
using System.Linq;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.WorkTimeManagement;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.DAL.Repositories.WorkTimeManagement
{
    public class WorkTimeRepository : BaseRepository<WorkTime>, IWorkTimeRepository
    {
        public WorkTimeRepository(SofcoContext context) : base(context)
        {
        }

        public IList<WorkTime> Get(DateTime startDate, DateTime endDate, int currentUserId)
        {
            return context.WorkTimes
                .Where(x => x.Employee.UserId == currentUserId && x.Date >= startDate && x.Date <= endDate)
                .Include(x => x.Employee)
                .Include(x => x.Analytic)
                .Include(x => x.Task)
                .ThenInclude(x => x.Category)
                .ToList();
        }

        public IList<WorkTime> SearchApproved(WorktimeHoursApprovedParams parameters)
        {
            var query = context.WorkTimes.Include(x => x.Employee).Include(x => x.Analytic).Include(x => x.Task).Where(x => x.Status == WorkTimeStatus.Approved);

            if (parameters.EmployeeId > 0)
                query = query.Where(x => x.EmployeeId == parameters.EmployeeId);

            query = query.Where(x => parameters.AnalyticIds.Contains(x.AnalyticId));

            if (parameters.FilterByDates)
            {
                query = query.Where(x => x.Date.Date >= parameters.StartDate.GetValueOrDefault().Date && x.Date.Date <= parameters.EndDate.GetValueOrDefault().Date);
            }

            return query.ToList();
        }

        public IList<WorkTime> SearchPending(int analyticId, int userId)
        {
            return context.WorkTimes
                .Include(x => x.Employee)
                .Include(x => x.Analytic)
                .Include(x => x.Task)
                .Where(x => ((x.Status == WorkTimeStatus.Sent && x.Analytic.ManagerId == userId) || 
                            (x.Status == WorkTimeStatus.SentToDirector && x.Analytic.DirectorId == userId)) && 
                            x.AnalyticId == analyticId)
                .ToList();
        }

        public void UpdateStatus(WorkTime worktime)
        {
            context.Entry(worktime).Property("Status").IsModified = true;
            context.Entry(worktime).Property("ApprovalUserId").IsModified = true;
        }

        public void UpdateApprovalComment(WorkTime worktime)
        {
            context.Entry(worktime).Property("ApprovalComment").IsModified = true;
        }

        public void SendHours(int employeeId)
        {
            context.Database.ExecuteSqlCommand($"UPDATE app.worktimes SET status = {WorkTimeStatus.Sent} where status = {WorkTimeStatus.Draft} and employeeid = {employeeId}");
        }

        public void Save(WorkTime workTime)
        {
            if (workTime.Id == 0)
            {
                Insert(workTime);
                return;
            }

            Update(workTime);
        }

        public IList<WorkTime> Search(SearchParams parameters)
        {
            IQueryable<WorkTime> query = context.WorkTimes
                .Include(x => x.Employee)
                .Include(x => x.Task)
                    .ThenInclude(x => x.Category)
                .Include(x => x.Analytic)
                    .ThenInclude(x => x.Manager)
                .Include(x => x.Analytic)
                    .ThenInclude(x => x.Customer)
                .Where(x => x.Date.Date >= parameters.StartDate.GetValueOrDefault().Date && x.Date.Date <= parameters.EndDate.GetValueOrDefault().Date);

            if (parameters.Status > 0)
                query = query.Where(x => x.Status == (WorkTimeStatus) parameters.Status);

            if (parameters.AnalyticId.HasValue && parameters.AnalyticId > 0)
                query = query.Where(x => x.AnalyticId == parameters.AnalyticId.Value);

            if (parameters.EmployeeId.HasValue && parameters.EmployeeId > 0)
                query = query.Where(x => x.EmployeeId == parameters.EmployeeId.Value);

            if (parameters.ManagerId.HasValue && parameters.ManagerId > 0)
                query = query.Where(x => x.Analytic.ManagerId == parameters.ManagerId.Value);

            return query.ToList();
        }

        public void InsertBulk(IList<WorkTime> workTimesToAdd)
        {
            context.BulkInsert(workTimesToAdd);
        }

        public void SendManagerHours(int employeeid)
        {
            context.Database.ExecuteSqlCommand($"UPDATE app.worktimes SET status = {WorkTimeStatus.SentToDirector} where status = {WorkTimeStatus.Draft} and employeeid = {employeeid}");
        }

        public List<WorkTime> GetWorkTimeDraftByEmployeeId(int employeeId)
        {
            return context.WorkTimes
                .Include(x => x.Task)
                .Where(s => s.EmployeeId == employeeId
                            && s.Status == WorkTimeStatus.Draft)
                .ToList();
        }

        public IList<WorkTime> GetByAnalyticIds(DateTime startDate, DateTime endDate, List<int> analyticIds)
        {
            return context.WorkTimes
                .Where(x => analyticIds.Contains(x.AnalyticId) && x.Date.Date >= startDate.Date && x.Date.Date <= endDate.Date)
                .Include(x => x.Employee)
                .Include(x => x.Analytic)
                .Include(x => x.Task)
                .ToList();
        }

        public IList<WorkTime> SearchPendingByEmployee(int employeeId, int userId, int analyticId)
        {
            return context.WorkTimes
                .Include(x => x.Employee)
                .Include(x => x.Analytic)
                .Include(x => x.Task)
                .Where(x => ((x.Status == WorkTimeStatus.Sent && x.Analytic.ManagerId == userId) || 
                            (x.Status == WorkTimeStatus.SentToDirector && x.Analytic.DirectorId == userId)) &&
                            x.AnalyticId == analyticId &&
                            x.EmployeeId == employeeId)
                .ToList();
        }

        public decimal GetPendingHoursByEmployeeId(int employeeId)
        {
            return context.WorkTimes
                .Where(s => s.EmployeeId == employeeId
                            && s.Status == WorkTimeStatus.Sent)
                .Select(s => s.Hours)
                .Sum();
        }

        public decimal GetTotalHoursByDateExceptCurrentId(DateTime date, int currentUserId, int id)
        {
            return context.WorkTimes
                .Where(x => x.EmployeeId == currentUserId 
                        && x.Date.Year == date.Year 
                        && x.Date.Month == date.Month 
                        && x.Date.Day == date.Day
                        && x.Id != id)
                .Select(s => s.Hours)
                .Sum();
        }

        private new void Update(WorkTime workTime)
        {
            var stored = context.WorkTimes.SingleOrDefault(x => x.Id == workTime.Id);

            if (stored == null) throw new Exception("Item Not Found");

            stored.AnalyticId = workTime.AnalyticId;
            stored.TaskId  = workTime.TaskId;
            stored.Date  = workTime.Date;
            stored.Hours  = workTime.Hours;
            stored.UserComment  = workTime.UserComment;
            stored.Status  = workTime.Status;
            stored.Reference = workTime.Reference;
        }
    }
}
