﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Admin;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Models.Admin;

namespace Sofco.DAL.Repositories.Admin
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(SofcoContext context) : base(context)
        {
        }

        public bool ExistById(int id)
        {
            return context.Roles.Any(x => x.Id == id);
        }

        public IList<Role> GetAllActivesReadOnly()
        {
            return context.Set<Role>().Where(x => x.Active).ToList().AsReadOnly();
        }

        public bool ExistByDescription(string roleDescription, int roleId)
        {
            return context.Roles.Any(x => x.Description.Equals(roleDescription) && x.Id != roleId);
        }

        public Role GetByCode(string code)
        {
            return context.Roles.FirstOrDefault(s => s.Code == code);
        }

        public IList<Role> GetRolesByUser(string userName)
        {
            return context.UserRoles.Include(x => x.Role).Include(x => x.User).Where(x => x.User.UserName == userName).Select(x => x.Role).ToList();
        }

        public bool HasRole(int currentUserId, string managerRole)
        {
            return context.UserRoles
                .Include(x => x.Role)
                .Any(x => x.UserId == currentUserId && x.Role.Code == managerRole);
        }

        public Role GetDetail(int id)
        {
            return context.Set<Role>()
                    .Include(x => x.RoleFunctionality)
                        .ThenInclude(x => x.Functionality)
                            .ThenInclude(x => x.Module)
                   .SingleOrDefault(x => x.Id == id);
        }
    }
}
