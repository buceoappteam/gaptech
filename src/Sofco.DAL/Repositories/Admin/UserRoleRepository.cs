﻿using Sofco.Core.DAL.Admin;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Relationships;
using System.Linq;

namespace Sofco.DAL.Repositories.Admin
{
    public class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(SofcoContext context) : base(context)
        {
        }

        public bool ExistById(int userId, int roleId)
        {
            return context.UserRoles.Any(x => x.UserId == userId && x.RoleId == roleId);
        }
    }
}
