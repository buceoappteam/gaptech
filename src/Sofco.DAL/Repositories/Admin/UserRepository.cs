﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sofco.Common.Extensions;
using Sofco.Common.Settings;
using Sofco.Core.Config;
using Sofco.Core.DAL.Admin;
using Sofco.Core.Models.Admin;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Models.Admin;

namespace Sofco.DAL.Repositories.Admin
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly EmailConfig emailConfig;

        public UserRepository(SofcoContext context, IOptions<EmailConfig> emailConfig) : base(context)
        {
            this.emailConfig = emailConfig.Value;
        }

        public bool ExistById(int id)
        {
            return context.Set<User>().Any(x => x.Id == id);
        }

        public IList<User> GetAllActivesReadOnly()
        {
            return context.Set<User>().Where(x => x.Active).ToList().AsReadOnly();
        }

        public IList<User> GetAllWithEmployee()
        {
            return context.Set<User>()
                .Include(x => x.Employee)
                .ToList().AsReadOnly();
        }

        public User GetByIdWithEmployee(int id)
        {
            return context.Users
                .Include(x => x.Employee)
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public IList<User> GetAllFullReadOnly()
        {
            return context.Set<User>()
                .Include(x => x.UserRoles)
                    .ThenInclude(s => s.Role)
                .ToList()
                .AsReadOnly();
        }

        public User GetSingleWithUserGroup(Expression<Func<User, bool>> predicate)
        {
            return context.Set<User>()
                .Include(x => x.Employee)
                .Include(x => x.UserRoles)
                    .ThenInclude(s => s.Role)
                .SingleOrDefault(predicate);
        }

        public bool ExistByMail(string mail)
        {
            return context.Users.Any(x => x.Email == mail);
        }

        public UserLiteModel GetUserLiteByUserName(string userName)
        {
            return context.Users.Include(x => x.Employee).Where(s => s.UserName == userName).Select(s => new UserLiteModel
            {
                Id = s.Id,
                Name = s.Name,
                Email = s.Email,
                UserName = userName,
                Employee = s.Employee
            }).FirstOrDefault();
        }

        public User GetByEmail(string email)
        {
            return context.Users.Include(x => x.Employee).SingleOrDefault(x => x.Email == email);
        }

        public IList<User> GetByRole(string roleCode)
        {
            return context.Users
                .Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .Where(x => x.UserRoles.Any(s => s.Role.Code == roleCode))
                .ToList();
        }

        public bool IsActive(string userMail)
        {
            return context.Users.Any(x => x.Email == userMail && x.Active);
        }

        public UserLiteModel GetUserLiteById(int userId)
        {
            return context.Users.Include(x => x.Employee).Where(s => s.Id == userId).Select(s => new UserLiteModel
            {
                Id = s.Id,
                Name = s.Name,
                Email = s.Email,
                Employee = s.Employee
            }).FirstOrDefault();
        }

        public IList<User> GetByIdsWithRoles(IEnumerable<int> ids)
        {
            var users = context.Users
                .Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .Where(x => ids.Contains(x.Id))
                .ToList();

            return users;
        }
    }
}
