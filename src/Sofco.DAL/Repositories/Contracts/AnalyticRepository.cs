﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Contracts;
using Sofco.Core.Models.AllocationManagement;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Utils;

namespace Sofco.DAL.Repositories.AllocationManagement
{
    public class AnalyticRepository : BaseRepository<Analytic>, IAnalyticRepository
    {
        public AnalyticRepository(SofcoContext context) : base(context)
        {
        }

        public bool Exist(int id)
        {
            return context.Analytics.Any(x => x.Id == id);
        }

        public IList<Allocation> GetTimelineResources(int id, DateTime startDate, DateTime endDate)
        {
            return context.Allocations.Where(x => x.AnalyticId == id && x.StartDate >= startDate && x.StartDate <= endDate && x.Percentage > 0).Include(x => x.Employee).ToList().AsReadOnly();
        }

        public IList<Employee> GetResources(int id)
        {
            return context.Allocations.Where(x => x.AnalyticId == id && x.Percentage > 0 && !x.Employee.EndDate.HasValue)
                .Include(x => x.Employee)
                .Select(x => x.Employee)
                .Distinct()
                .ToList()
                .AsReadOnly();
        }

        public IList<Employee> GetResources(int id, DateTime startDate, DateTime endDate)
        {
            return context.Allocations.Where(x => x.AnalyticId == id && x.Percentage > 0 && !x.Employee.EndDate.HasValue && (x.StartDate.Date == startDate || x.StartDate.Date == endDate))
                .Include(x => x.Employee)
                .Select(x => x.Employee)
                .Distinct()
                .ToList()
                .AsReadOnly();
        }

        public Analytic GetLastAnalytic(int costCenterId)
        {
            return context.Analytics.Where(x => x.CostCenterId == costCenterId).OrderByDescending(x => x.TitleId).Include(x => x.CostCenter).FirstOrDefault();
        }

        public bool ExistTitle(string analyticTitle)
        {
            return context.Analytics.Any(x => x.Title.Equals(analyticTitle));
        }

        public void Close(Analytic analytic)
        {
            context.Entry(analytic).Property("Status").IsModified = true;
        }

        public ICollection<Analytic> GetAllOpenReadOnly()
        {
            return context.Analytics.Where(x => x.Status == AnalyticStatus.Open).ToList();
        }

        public bool IsManager(int analyticId, int currentUserId)
        {
            return context.WorkTimes.Include(x => x.Analytic).Any(x => x.AnalyticId == analyticId && x.Analytic.ManagerId == currentUserId);
        }

        public bool IsDirector(int analyticId, int currentUserId)
        {
            return context.WorkTimes.Include(x => x.Analytic).Any(x => x.AnalyticId == analyticId && x.Analytic.DirectorId == currentUserId);
        }

        public ICollection<Analytic> GetByClient(int clientId, bool onlyActives)
        {
            if (onlyActives)
            {
                return context.Analytics.Where(x => x.CustomerId == clientId && x.Status == AnalyticStatus.Open).ToList();
            }
            else
            {
                return context.Analytics.Where(x => x.CustomerId == clientId).ToList();
            }
        }

        public Analytic GetById(int allocationAnalyticId)
        {
            return context.Analytics.Include(x => x.Manager).SingleOrDefault(x => x.Id == allocationAnalyticId);
        }

        public ICollection<Analytic> GetByManagerIdAndDirectorId(int managerId)
        {
            return context.Analytics.Where(x => (x.ManagerId == managerId || x.DirectorId == managerId) && x.Status == AnalyticStatus.Open).ToList();
        }

        public IList<int> GetAnalyticsIdByEmployee(int employeeId)
        {
            var date = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);

            return context.Allocations.Where(x => x.EmployeeId == employeeId && x.Percentage > 0 && x.StartDate == date).Select(x => x.AnalyticId).Distinct().ToList();
        }

        public List<AnalyticLiteModel> GetLiteByManagerId(int managerId)
        {
            return context.Analytics
                .Where(x => x.ManagerId == managerId
                && x.Status == AnalyticStatus.Open)
                .Select(s => new AnalyticLiteModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Title = s.Title
                }).ToList();
        }

        public AnalyticLiteModel GetAnalyticLiteById(int id)
        {
            return context.Analytics.Where(s => s.Id == id).Select(s => new AnalyticLiteModel
            {
                Id = s.Id,
                Name = s.Name,
                Title = s.Title
            }).FirstOrDefault();

        }

        public IList<Analytic> GetAnalyticsLiteByEmployee(int employeeId, int userId, DateTime dateFrom, DateTime dateTo)
        {
            var analyticsByAllocations = context.Allocations
                .Where(x =>  x.EmployeeId == employeeId 
                             && x.Percentage > 0 
                             && x.Analytic.Status == AnalyticStatus.Open 
                             && (x.StartDate.Date == dateFrom || x.StartDate.Date == dateTo))
                .Include(x => x.Analytic)
                .Select(x => new Analytic
                {
                    Id = x.AnalyticId,
                    Title = x.Analytic.Title,
                    Name = x.Analytic.Name
                })
                .ToList();

            var analyticsByManagers = context.Analytics
                .Where(x => x.ManagerId == userId && x.Status == AnalyticStatus.Open)
                .Select(x => new Analytic
                {
                    Id = x.Id,
                    Title = x.Title,
                    Name = x.Name
                })
                .ToList();

            return analyticsByAllocations.Union(analyticsByManagers).ToList();
        }

        public Analytic GetByTitle(string title)
        {
            return context.Analytics.SingleOrDefault(x => x.Title == title);
        }

        public List<Analytic> GetBySearchCriteria(AnalyticSearchParameters searchCriteria)
        {
            IQueryable<Analytic> query = context.Analytics.Include(x => x.Manager).Include(x => x.Director).Include(x => x.Customer);

            if (searchCriteria.AnalyticId.HasValue && searchCriteria.AnalyticId.Value > 0)
            {
                query = query.Where(x => x.Id == searchCriteria.AnalyticId);
            }

            if (searchCriteria.CustomerId.HasValue && searchCriteria.CustomerId.Value > 0)
            {
                query = query.Where(x => x.CustomerId == searchCriteria.CustomerId);
            }

            if (searchCriteria.AnalyticStatusId.HasValue && searchCriteria.AnalyticStatusId.Value > 0)
            {
                query = query.Where(x => (int)x.Status == searchCriteria.AnalyticStatusId);
            }

            if (searchCriteria.ManagerId.HasValue && searchCriteria.ManagerId.Value > 0)
            {
                query = query.Where(x => x.ManagerId == searchCriteria.ManagerId);
            }

            if (searchCriteria.DirectorId.HasValue && searchCriteria.DirectorId.Value > 0)
            {
                query = query.Where(x => x.DirectorId == searchCriteria.DirectorId);
            }

            return query.ToList();
        }

        public List<Analytic> GetForReport(List<int> analytics)
        {
            return context.Analytics
                .Include(x => x.CostCenter)
                .Include(x => x.Manager)
                .Include(x => x.Director)
                //.Include(x => x.ServiceType)
                //.Include(x => x.SoftwareLaw)
                //.Include(x => x.Solution)
                //.Include(x => x.Technology)
                .Where(x => analytics.Contains(x.Id))
                .ToList();
        }

        public List<Analytic> GetByAllocations(int employeeId, DateTime dateFrom, DateTime dateTo)
        {
            var analyticIds = context.Allocations
                .Where(x => x.EmployeeId == employeeId && (x.StartDate.Date == dateFrom.Date || x.StartDate.Date == dateTo.Date))
                .Select(x => x.AnalyticId)
                .Distinct()
                .ToList();

            return context.Analytics
                .Include(x => x.Manager)
                .Where(x => analyticIds.Contains(x.Id))
                .Distinct()
                .ToList();
        }
    }
}
