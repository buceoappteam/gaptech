﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Contracts;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.Models.Contracts;

namespace Sofco.DAL.Repositories.Contracts
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(SofcoContext context) : base(context)
        {
        }

        public IList<Customer> GetAllActives()
        {
            return context.Customers.Where(x => x.Active).ToList().AsReadOnly();
        }

        public void UpdateStatus(Customer customer)
        {
            context.Entry(customer).Property("Active").IsModified = true;
        }

        public Customer GetById(int id)
        {
            return context.Customers.Where(x => x.Id == id)
                     .Include(x => x.Province)
                     .FirstOrDefault();
        }
    }
}
