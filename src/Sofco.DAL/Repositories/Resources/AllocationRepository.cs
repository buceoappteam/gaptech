﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Resources;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.DAL.Repositories.Common;
using Sofco.Domain.DTO;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Resources;

namespace Sofco.DAL.Repositories.Resources
{
    public class AllocationRepository : BaseRepository<Allocation>, IAllocationRepository
    {
        public AllocationRepository(SofcoContext context) : base(context)
        {
        }

        public ICollection<Allocation> GetAllocationsBetweenDays(int employeeId, DateTime startDate, DateTime endDate)
        {
            return context.Allocations
                .Where(x => x.EmployeeId == employeeId && x.StartDate >= startDate && x.StartDate <= endDate)
                .Include(x => x.Analytic)
                .Include(x => x.Employee)
                .OrderBy(x => x.AnalyticId).ThenBy(x => x.StartDate)
                .ToList();
        }

        public void UpdateReleaseDate(Allocation allocation)
        {
            context.Entry(allocation).Property("ReleaseDate").IsModified = true;
        }

        public ICollection<Employee> GetByAnalyticId(int analyticId)
        {
            return context.Allocations
                .Include(x => x.Employee)
                .Where(x => x.AnalyticId == analyticId)
                .Select(x => x.Employee)
                .Include(x => x.EmployeeCategories)
                .Distinct()
                .ToList();
        }

        public void DeleteAllocationWithReleaseDateNull()
        {
            context.Database.ExecuteSqlCommand("delete from app.allocations where releasedate = '0001-01-01 00:00:00.0000000'");
        }

        public DateTime GetStartDate(int analitycId, int employeeId)
        {
            return context.Allocations.Where(x => x.AnalyticId == analitycId && x.EmployeeId == employeeId && x.Percentage > 0).Min(x => x.StartDate);
        }

        public ICollection<Allocation> GetByEmployee(int id)
        {
            return context.Allocations
                .Include(x => x.Analytic)
                .ThenInclude(x => x.Customer)
                .Where(x => x.EmployeeId == id)
                .ToList();
        }

        public bool ExistCurrentAllocationByEmployeeAndManagerId(int employeeId, int managerId, DateTime startDate)
        {
            return context.Allocations
                .Include(x => x.Analytic)
                .Any(x => x.EmployeeId == employeeId
                          && x.Percentage > 0
                          && x.Analytic.ManagerId == managerId
                          && x.StartDate.Date == startDate.Date);
        }

        public ICollection<Employee> GetByEmployeesForReport(AllocationReportParams parameters)
        {
            IQueryable<Allocation> query = context.Allocations
                .Include(x => x.Analytic)
                .Include(x => x.Employee)
                .Where(x => x.StartDate.Date >= parameters.StartDate.GetValueOrDefault().Date && x.StartDate.Date <= parameters.EndDate.GetValueOrDefault().Date && !x.Employee.EndDate.HasValue);

            if (!parameters.IncludeStaff)
                query = query.Where(x => x.Employee.BillingPercentage != 0);

            if (!parameters.Unassigned)
            {
                if (parameters.AnalyticIds.Any())
                    query = query.Where(x => parameters.AnalyticIds.Contains(x.AnalyticId));

                if (parameters.EmployeeId.HasValue)
                    query = query.Where(x => x.EmployeeId == parameters.EmployeeId.Value);

                if (parameters.IncludeAnalyticId == 2)
                    query = query.Where(x => x.Analytic.Status == AnalyticStatus.Open);

                if (parameters.IncludeAnalyticId == 3)
                    query = query.Where(x => x.Analytic.Status == AnalyticStatus.Close || x.Analytic.Status == AnalyticStatus.CloseToExpenses);
            }

            return query.Select(x => x.Employee)
                .Include(x => x.Allocations)
                .ThenInclude(x => x.Analytic)
                .OrderBy(x => x.StartDate)
                .Distinct()
                .ToList();
        }

        public void RemoveAllocationByAnalytic(int analyticId, DateTime today)
        {
            var allocations = context.Allocations.Where(x => x.AnalyticId == analyticId && x.StartDate.Date > today.Date);
            context.Allocations.RemoveRange(allocations);
        }

        public void UpdatePercentage(Allocation allocation)
        {
            context.Entry(allocation).Property("Percentage").IsModified = true;
        }

        public ICollection<Allocation> GetAllocationsLiteBetweenDaysForWorkTimeControl(int employeeId, DateTime startDate, DateTime endDate)
        {
            return context.Allocations
                .Where(x => x.EmployeeId == employeeId
                            && x.Percentage > 0 
                            && x.StartDate.Date >= startDate.Date 
                            && x.StartDate.Date <= endDate.Date)
                .ToList();
        }
    }
}
