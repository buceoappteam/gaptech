﻿using System.Collections.Generic;
using System.Linq;
using Sofco.DAL.Repositories.Common;
using System;
using Microsoft.EntityFrameworkCore;
using Sofco.Core.DAL.Resources;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.DTO;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Relationships;

namespace Sofco.DAL.Repositories.AllocationManagement
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(SofcoContext context) : base(context)
        {
        }

        public bool Exist(int employeeId)
        {
            return context.Employees.Any(x => x.Id == employeeId);
        }

        public new ICollection<Employee> GetAll()
        {
            return context.Employees.Include(x => x.Province).Include(x => x.User).Where(x => x.EndDate == null).ToList();
        }

        public IList<Employee> GetUnassignedBetweenDays(DateTime startDate, DateTime endDate)
        {
            var from = new DateTime(startDate.Year, startDate.Month, 1).Date;
            var to = new DateTime(endDate.Year, endDate.Month, 1).Date;

            var employeeIdsWithAllocations = context.Allocations.Where(x => x.StartDate.Date == from || x.StartDate.Date == to).Select(x => x.EmployeeId).Distinct().ToList();

            return context.Employees.Where(x => !employeeIdsWithAllocations.Contains(x.Id) && x.EndDate == null).ToList();
        }

        public IList<Employee> GetByAnalyticIds(List<int> analyticIds)
        {
            var ids = context.Allocations.Where(x => analyticIds.Contains(x.AnalyticId) && x.Percentage > 0)
                .Select(x => x.Employee.Id)
                .Distinct()
                .ToList();

            return context.Employees
                .Include(x => x.WorkTimes)
                .Where(x => ids.Contains(x.Id)).ToList();
        }

        public IList<Employee> GetByAnalyticIdInCurrentDate(int analyticId)
        {
            var date = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);

            return context.Allocations.Include(x => x.Employee).Where(x =>
                x.AnalyticId == analyticId && x.Percentage > 0 && x.StartDate.Date == date.Date).Select(x => x.Employee).ToList();
        }

        public ICollection<Employee> Search(EmployeeSearchParams parameters)
        {
            IQueryable<Employee> query = context.Employees.Include(x => x.Province);

            if (parameters.Unassigned)
            {
                var employeeIdsWithAllocations = context.Allocations.Select(x => x.EmployeeId).Distinct().ToList();

                return query.Where(x => !employeeIdsWithAllocations.Contains(x.Id) && x.EndDate == null).ToList();
            }
            else
            {
                query = query.Where(x => x.EndDate == null);
            }

            if (!string.IsNullOrWhiteSpace(parameters.Name))
                query = query.Where(x => x.Name != null && x.Name.ToLowerInvariant().Contains(parameters.Name.ToLowerInvariant()));

            if (!string.IsNullOrWhiteSpace(parameters.Profile))
                query = query.Where(x => x.Profile != null && x.Profile.ToLowerInvariant().Contains(parameters.Profile.ToLowerInvariant()));

            if (!string.IsNullOrWhiteSpace(parameters.Seniority))
                query = query.Where(x => x.Seniority != null && x.Seniority.ToLowerInvariant().Contains(parameters.Seniority.ToLowerInvariant()));

            if (!string.IsNullOrWhiteSpace(parameters.Technology))
                query = query.Where(x => x.Technology != null && x.Technology.ToLowerInvariant().Contains(parameters.Technology.ToLowerInvariant()));

            if (!string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
                query = query.Where(x => x.EmployeeNumber != null && x.EmployeeNumber.ToLowerInvariant().Contains(parameters.EmployeeNumber.ToLowerInvariant()));

            if (parameters.Percentage.HasValue)
                query = query.Where(x => x.BillingPercentage == parameters.Percentage);

            if (parameters.AnalyticId.HasValue && parameters.AnalyticId > 0)
            {
                query = from employee in query
                        from allocation in context.Allocations
                        where employee.Id == allocation.EmployeeId && allocation.AnalyticId == parameters.AnalyticId
                        select employee;
            }

            return query.Distinct().ToList();
        }

        public IList<EmployeeCategory> GetEmployeeCategories(int employeeId)
        {
            return context.EmployeeCategories
                .Include(x => x.Category)
                    .ThenInclude(x => x.Tasks)
                .Where(x => x.EmployeeId == employeeId && x.Category.Active).ToList();
        }

        public void UpdateBusinessHours(Employee employee)
        {
            context.Entry(employee).Property("BusinessHours").IsModified = true;
            context.Entry(employee).Property("BusinessHoursDescription").IsModified = true;
            context.Entry(employee).Property("OfficeAddress").IsModified = true;
            context.Entry(employee).Property("HolidaysPendingByLaw").IsModified = true;
            context.Entry(employee).Property("ManagerId").IsModified = true;
            context.Entry(employee).Property("HolidaysPending").IsModified = true;
            context.Entry(employee).Property("BillingPercentage").IsModified = true;
            context.Entry(employee).Property("HasCreditCard").IsModified = true;
        }

        public IList<Employee> SearchUnemployees(UnemployeeSearchParameters parameters)
        {
            IQueryable<Employee> query = context.Employees.Where(x => x.EndDate.HasValue);

            if (!string.IsNullOrWhiteSpace(parameters.Name))
                query = query.Where(x => x.Name != null && x.Name.ToLowerInvariant().Contains(parameters.Name.ToLowerInvariant()));

            if (parameters.StartDate.HasValue)
                query = query.Where(x => x.EndDate.HasValue && x.EndDate.Value.Date >= parameters.StartDate.Value.Date);

            if (parameters.EndDate.HasValue)
                query = query.Where(x => x.EndDate.HasValue && x.EndDate.Value.Date <= parameters.EndDate.Value.Date);

            return query.ToList();
        }

        public IList<Employee> GetByAnalyticWithWorkTimes(int analyticId)
        {
            var ids = context.Allocations.Where(x => x.AnalyticId == analyticId && x.Percentage > 0)
                .Select(x => x.Employee.Id)
                .Distinct()
                .ToList();

            return context.Employees
                .Include(x => x.WorkTimes)
                .Include(x => x.Allocations)
                .Where(x => ids.Contains(x.Id)).ToList();
        }

        public Employee GetById(int id)
        {
            return context.Employees
                .Include(x => x.Province)
                    .ThenInclude(x => x.Country)
                .Include(x => x.DocumentType)
                .SingleOrDefault(x => x.Id == id);
        }

        public Employee GetByEmail(string email)
        {
            return context.Employees.Include(x => x.Province).SingleOrDefault(x => x.Email == email);
        }
    }
}
