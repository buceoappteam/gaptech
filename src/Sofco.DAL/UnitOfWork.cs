﻿using Microsoft.Extensions.Options;
using Sofco.Common.Settings;
using Sofco.Core.Config;
using Sofco.Core.DAL;
using Sofco.Core.DAL.Admin;
using Sofco.Core.DAL.Common;
using Sofco.Core.DAL.Contracts;
using Sofco.Core.DAL.Resources;
using Sofco.Core.DAL.Workflow;
using Sofco.Core.DAL.WorkTimeManagement;
using Sofco.DAL.Repositories.Admin;
using Sofco.DAL.Repositories.AllocationManagement;
using Sofco.DAL.Repositories.Common;
using Sofco.DAL.Repositories.Contracts;
using Sofco.DAL.Repositories.Resources;
using Sofco.DAL.Repositories.Workflow;
using Sofco.DAL.Repositories.WorkTimeManagement;

namespace Sofco.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SofcoContext context;

        private readonly IOptions<EmailConfig> emailConfig;

        private readonly IOptions<AppSetting> appSettingOptions;

        #region Admin

        private IUserRepository userRepository;
        private IRoleRepository roleRepository;
        private IModuleRepository moduleRepository;
        private IFunctionalityRepository functionalityRepository;
        private IMenuRepository menuRepository;
        private ISettingRepository settingRepository;
        private IRoleFunctionalityRepository roleFunctionalityRepository;
        private ICategoryRepository categoryRepository;
        private ITaskRepository taskRepository;
        private IWorkflowStateRepository workflowStateRepository;
        private IUserRoleRepository userRoleRepository;

        #endregion

        #region Billing

        private ICustomerRepository customerRepository;

        #endregion

        #region AllocationManagement

        private IAllocationRepository allocationRepository;
        private IAnalyticRepository analyticRepository;
        private ICostCenterRepository costCenterRepository;
        private IEmployeeRepository employeeRepository;

        #endregion

        #region HumanResource

        private ICloseDateRepository closeDateRepository;

        #endregion

        #region Common

        private IUtilsRepository utilsRepository;
        private IFileRepository fileRepository;
        private IUserDelegateRepository userDelegateRepository;

        #endregion

        #region WorkTimeManagement

        private IWorkTimeRepository workTimeRepository;
        private IHolidayRepository holidayRepository;
        private IRecentTaskRepository recentTaskRepository;

        #endregion

        private IWorkflowRepository workflowRepository;

        private IUserSourceRepository userSourceRepository;

        public UnitOfWork(SofcoContext context, IOptions<EmailConfig> emailConfig, IOptions<AppSetting> appSettingOptions)
        {
            this.context = context;
            this.emailConfig = emailConfig;
            this.appSettingOptions = appSettingOptions;
        }

        #region Admin

        public IUserRepository UserRepository => userRepository ?? (userRepository = new UserRepository(context, emailConfig));
        public IRoleRepository RoleRepository => roleRepository ?? (roleRepository = new RoleRepository(context));
        public IModuleRepository ModuleRepository => moduleRepository ?? (moduleRepository = new ModuleRepository(context));
        public IFunctionalityRepository FunctionalityRepository => functionalityRepository ?? (functionalityRepository = new FunctionalityRepository(context));
        public IMenuRepository MenuRepository => menuRepository ?? (menuRepository = new MenuRepository(context));
        public ISettingRepository SettingRepository => settingRepository ?? (settingRepository = new SettingRepository(context));
        public IRoleFunctionalityRepository RoleFunctionalityRepository => roleFunctionalityRepository ?? (roleFunctionalityRepository = new RoleFunctionalityRepository(context));
        public ICategoryRepository CategoryRepository => categoryRepository ?? (categoryRepository = new CategoryRepository(context));
        public ITaskRepository TaskRepository => taskRepository ?? (taskRepository = new TaskRepository(context));
        public IWorkflowStateRepository WorkflowStateRepository => workflowStateRepository ?? (workflowStateRepository = new WorkflowStateRepository(context));
        public IUserRoleRepository UserRoleRepository => userRoleRepository ?? (userRoleRepository = new UserRoleRepository(context));

        #endregion

        #region Billing

        public ICustomerRepository CustomerRepository => customerRepository ?? (customerRepository = new CustomerRepository(context));

        #endregion

        #region AllocationManagement

        public IAllocationRepository AllocationRepository => allocationRepository ?? (allocationRepository = new AllocationRepository(context));
        public IAnalyticRepository AnalyticRepository => analyticRepository ?? (analyticRepository = new AnalyticRepository(context));
        public ICostCenterRepository CostCenterRepository => costCenterRepository ?? (costCenterRepository = new CostCenterRepository(context));
        public IEmployeeRepository EmployeeRepository => employeeRepository ?? (employeeRepository = new EmployeeRepository(context));

        #endregion

        #region HumanResource

        public ICloseDateRepository CloseDateRepository => closeDateRepository ?? (closeDateRepository = new CloseDateRepository(context));

        #endregion

        #region Common

        public IUtilsRepository UtilsRepository => utilsRepository ?? (utilsRepository = new UtilsRepository(context));
        public IFileRepository FileRepository => fileRepository ?? (fileRepository = new FileRepository(context));
        public IUserDelegateRepository UserDelegateRepository => userDelegateRepository ?? (userDelegateRepository = new UserDelegateRepository(context));
        #endregion

        #region WorkTimeManagement

        public IWorkTimeRepository WorkTimeRepository => workTimeRepository ?? (workTimeRepository = new WorkTimeRepository(context));

        public IHolidayRepository HolidayRepository => holidayRepository ?? (holidayRepository = new HolidayRepository(context));
        public IRecentTaskRepository RecentTaskRepository => recentTaskRepository ?? (recentTaskRepository = new RecentTaskRepository(context));

        #endregion

        public IWorkflowRepository WorkflowRepository => workflowRepository ?? (workflowRepository = new WorkflowRepository(context));

        public IUserSourceRepository UserSourceRepository => userSourceRepository ?? (userSourceRepository = new UserSourceRepository(context));

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
