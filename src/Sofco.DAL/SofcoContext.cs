﻿using Microsoft.EntityFrameworkCore;
using Sofco.DAL.Mappings.Admin;
using Sofco.DAL.Mappings.Common;
using Sofco.DAL.Mappings.Contracts;
using Sofco.DAL.Mappings.Resources;
using Sofco.DAL.Mappings.Utils;
using Sofco.DAL.Mappings.Workflow;
using Sofco.DAL.Mappings.WorkTimeManagement;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Relationships;
using Sofco.Domain.Utils;
using Sofco.Domain.Models.Common;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Models.Rrhh;
using Sofco.Domain.Models.Workflow;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.DAL
{
    public class SofcoContext : DbContext
    {
        public const string AppSchemaName = "app";

        public SofcoContext(DbContextOptions<SofcoContext> options)
            : base(options)
        {
        }

        // Admin Mappings
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Functionality> Functionalities { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Task> Tasks { get; set; }

        // RelationShips
        public DbSet<RoleFunctionality> RoleFunctionality { get; set; }
        public DbSet<EmployeeCategory> EmployeeCategories { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        // Billing Mappings
        public DbSet<Customer> Customers { get; set; }

        // Allocation Management Mappings
        public DbSet<Analytic> Analytics { get; set; }
        public DbSet<Allocation> Allocations { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<CostCenter> CostCenters { get; set; }

        // Work Time Management
        public DbSet<WorkTime> WorkTimes { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<CloseDate> CloseDates { get; set; }
        public DbSet<RecentTask> RecentTasks { get; set; }

        // Common
        //public DbSet<File> Files { get; set; }
        public DbSet<UserDelegate> UserDelegates { get; set; }

        // Utils Mapping
        public DbSet<Technology> Technologies { get; set; }
        public DbSet<Solution> Solutions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<SoftwareLaw> SoftwareLaws { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<DocumentType> DocumentType { get; set; }


        //Workflow
        public DbSet<UserSource> UserSources { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowReadAccess> WorkflowReadAccesses { get; set; }
        public DbSet<WorkflowState> WorkflowStates { get; set; }
        public DbSet<WorkflowStateAccess> WorkflowStateAccesses { get; set; }
        public DbSet<WorkflowStateNotifier> WorkflowStateNotifiers { get; set; }
        public DbSet<WorkflowStateTransition> WorkflowStateTransitions { get; set; }
        public DbSet<WorkflowType> WorkflowTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.HasDefaultSchema(AppSchemaName);

            builder.MapRoles();
            builder.MapModules();
            builder.MapUsers();
            builder.MapFunctionalities();
            builder.MapUtils();
            builder.MapRoleFunctionality();
            builder.MapAnalytic();
            builder.MapAllocation();
            builder.MapEmployee();
            builder.MapSetting();
            builder.MapCostCenter();
            builder.MapCategory();
            builder.MapTasks();
            builder.MapEmployeeCategory();
            builder.MapWorkTime();
            builder.MapCustomer();
            builder.MapCloseDate();
            builder.MapUserSource();
            builder.MapWorkflow();
            builder.MapWorkflowReadAccess();
            builder.MapWorkflowState();
            builder.MapWorkflowStateAccess();
            builder.MapWorkflowStateNotifier();
            builder.MapWorkflowStateTransition();
            builder.MapWorkflowType();
            builder.MapUserRole();
            builder.MapHoliday();
            builder.MapUserDelegate();
            builder.MapRecentTask();
        }
    }
}
