﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sofco.WebApi.Migrations
{
    public partial class DocumentType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentNumberType",
                schema: "app",
                table: "Employees");

            migrationBuilder.AddColumn<int>(
                name: "DocumentTypeId",
                schema: "app",
                table: "Employees",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "DocumentType",
                schema: "app",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employees_DocumentTypeId",
                schema: "app",
                table: "Employees",
                column: "DocumentTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_DocumentType_DocumentTypeId",
                schema: "app",
                table: "Employees",
                column: "DocumentTypeId",
                principalSchema: "app",
                principalTable: "DocumentType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_DocumentType_DocumentTypeId",
                schema: "app",
                table: "Employees");

            migrationBuilder.DropTable(
                name: "DocumentType",
                schema: "app");

            migrationBuilder.DropIndex(
                name: "IX_Employees_DocumentTypeId",
                schema: "app",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "DocumentTypeId",
                schema: "app",
                table: "Employees");

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumberType",
                schema: "app",
                table: "Employees",
                maxLength: 100,
                nullable: true);
        }
    }
}
