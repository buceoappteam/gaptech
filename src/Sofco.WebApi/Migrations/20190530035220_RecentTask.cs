﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sofco.WebApi.Migrations
{
    public partial class RecentTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RecentTasks",
                schema: "app",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Analytic = table.Column<string>(maxLength: 100, nullable: true),
                    AnalyticId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    Hours = table.Column<decimal>(nullable: false),
                    Reference = table.Column<string>(maxLength: 500, nullable: true),
                    Task = table.Column<string>(maxLength: 100, nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecentTasks", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RecentTasks",
                schema: "app");
        }
    }
}
