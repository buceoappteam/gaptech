﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sofco.WebApi.Migrations
{
    public partial class RefactorDelegates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SourceId",
                schema: "app",
                table: "UserDelegates",
                newName: "UserSourceId");

            migrationBuilder.AddColumn<int>(
                name: "AnalyticSourceId",
                schema: "app",
                table: "UserDelegates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnalyticSourceId",
                schema: "app",
                table: "UserDelegates");

            migrationBuilder.RenameColumn(
                name: "UserSourceId",
                schema: "app",
                table: "UserDelegates",
                newName: "SourceId");
        }
    }
}
