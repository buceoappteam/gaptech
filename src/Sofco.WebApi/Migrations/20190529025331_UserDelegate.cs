﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sofco.WebApi.Migrations
{
    public partial class UserDelegate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserApprovers",
                schema: "app");

            migrationBuilder.CreateTable(
                name: "UserDelegates",
                schema: "app",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    GrantedUserId = table.Column<int>(nullable: false),
                    SourceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDelegates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDelegates_Users_GrantedUserId",
                        column: x => x.GrantedUserId,
                        principalSchema: "app",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserDelegates_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "app",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDelegates_GrantedUserId",
                schema: "app",
                table: "UserDelegates",
                column: "GrantedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDelegates_UserId",
                schema: "app",
                table: "UserDelegates",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDelegates",
                schema: "app");

            migrationBuilder.CreateTable(
                name: "UserApprovers",
                schema: "app",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnalyticId = table.Column<int>(nullable: false),
                    ApproverUserId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: true),
                    CreatedUser = table.Column<string>(maxLength: 50, nullable: true),
                    EmployeeId = table.Column<int>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    ModifiedUser = table.Column<string>(maxLength: 50, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserApprovers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserApprovers_Analytics_AnalyticId",
                        column: x => x.AnalyticId,
                        principalSchema: "app",
                        principalTable: "Analytics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserApprovers_Users_ApproverUserId",
                        column: x => x.ApproverUserId,
                        principalSchema: "app",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserApprovers_ApproverUserId",
                schema: "app",
                table: "UserApprovers",
                column: "ApproverUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserApprovers_AnalyticId_EmployeeId_ApproverUserId_Type",
                schema: "app",
                table: "UserApprovers",
                columns: new[] { "AnalyticId", "EmployeeId", "ApproverUserId", "Type" },
                unique: true);
        }
    }
}
