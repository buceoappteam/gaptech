﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Services.Common;
using Sofco.Domain.Utils;

namespace Sofco.WebApi.Controllers
{
    [Route("api/utils")]
    [Authorize]
    public class UtilsController : Controller
    {
        private readonly IUtilsService utilsService;

        public UtilsController(IUtilsService utilsService)
        {
            this.utilsService = utilsService;
        }

        [HttpGet("months")]
        public IActionResult GetMonths()
        {
            var months = utilsService.GetMonths();

            return Ok(months);
        }

        [HttpGet("closeMonths")]
        public IActionResult GetCloseMonths()
        {
            var months = utilsService.GetCloseMonths();

            return Ok(months);
        }

        [HttpGet("years")]
        public IActionResult GetYears()
        {
            var years = utilsService.GetYears();

            return Ok(years);
        }

        [HttpGet("provinces/{countryId}")]
        public IActionResult GetProvinces(int countryId)
        {
            var provinces = utilsService.GetProvinces(countryId);

            return Ok(provinces);
        }

        [HttpGet("countries")]
        public IActionResult GetCountries()
        {
            var countries = utilsService.GetCountries();

            return Ok(countries);
        }

        [HttpGet("documentTypes")]
        public IActionResult GetDocumentTypes()
        {
            var types = utilsService.GetDocumentTypes();

            return Ok(types);
        }
    }
}
