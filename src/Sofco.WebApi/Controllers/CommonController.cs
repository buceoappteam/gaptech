﻿using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Services.Admin;

namespace Sofco.WebApi.Controllers
{
    [Route("api")]
    public class CommonController : Controller
    {
        private readonly ISettingService settingService;

        public CommonController(ISettingService settingService)
        {
            this.settingService = settingService;
        }

        [HttpGet("supportMail")]
        public IActionResult Get()
        {
            var mail = settingService.GetSupportMail();

            return Ok(mail);
        }
    }
}
