﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Models;
using Sofco.Core.Models.Admin;
using Sofco.Core.Services;
using Sofco.Core.Services.Admin;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Relationships;
using Sofco.Domain.Utils;
using Sofco.WebApi.Extensions;
using Sofco.WebApi.Filters;

namespace Sofco.WebApi.Controllers.Admin
{
    [Route("api/users")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IFunctionalityService functionalityService;
        private readonly ILoginService loginService;
        private readonly IMapper mapper;

        public UserController(IUserService userService, IFunctionalityService functionalityService, ILoginService loginServ, IMapper mapper)
        {
            this.userService = userService;
            this.functionalityService = functionalityService;
            loginService = loginServ;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var users = userService.GetAllReadOnly(false);

            var models = Translate(users.ToList());

            return Ok(models.OrderBy(x => x.Name));
        }

        [HttpGet]
        [Route("options")]
        public IActionResult GetOptions()
        {
            var users = userService.GetAllReadOnly(true);
            var model = new List<UserSelectListItem>();

            foreach (var user in users)
                model.Add(new UserSelectListItem { Id = user.Id.ToString(), Text = user.Name, UserName = user.UserName, Email = user.Email });

            return Ok(model.OrderBy(x => x.Text));
        }

        [HttpGet]
        [Route("managers")]
        public IActionResult GetManagers()
        {
            var users = userService.GetManagers();

            return Ok(users);
        }

        [HttpGet]
        [Route("directors")]
        public IActionResult GetDirectors()
        {
            var users = userService.GetDirectors();

            return Ok(users);
        }


        [HttpGet("{id}/detail")]
        public IActionResult Detail(int id)
        {
            var response = userService.GetById(id);

            if (response.HasErrors())
                return BadRequest(response);

            var model = TranslateToUserDetailModel(response.Data);
            if (response.Data.Employee == null)
            {
                response.Messages.Add(new Message(Sofco.Resources.Admin.User.NotFound, MessageType.Error));
                return BadRequest(response);
            }

            model.EmployeeId = response.Data.Employee.Id;
            model.EmployeeName = response.Data.Employee.Name;

            if (response.Data.UserRoles.Any())
            {
                foreach (var userRole in response.Data.UserRoles)
                {
                    if (userRole != null)
                    {
                        var roleModel = new Option() { Id = userRole.RoleId, Text = userRole.Role.Description };

                        if (!model.Roles.Any(x => x.Id == userRole.RoleId && userRole.Role.Active))
                        {
                            model.Roles.Add(roleModel);
                        }
                    }
                }

                var roleFunctionalities = functionalityService.GetFunctionalitiesByRole(model.Roles.Select(x => x.Id));
                var modules = roleFunctionalities.Select(x => x.Functionality.Module).Distinct();

                foreach (var module in modules)
                {
                    var moduleModel = new ModuleModelDetail(module);

                    moduleModel.Functionalities = module.Functionalities.Select(x => new Option() { Id = x.Id, Text = x.Description }).ToList();

                    model.Modules.Add(moduleModel);
                }
            }

            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var response = userService.GetById(id);

            if (response.HasErrors())
                return BadRequest(response);

            var model = Translate(response.Data);
            model.Roles = new List<RoleModel>();

            foreach (var userGroup in response.Data.UserRoles)
            {
                model.Roles.Add(new RoleModel(userGroup.Role));
            }

            return Ok(model);
        }

        [HttpGet("email")]
        public IActionResult GetByMail()
        {
            var response = userService.GetUserInfo();

            return this.CreateResponse(response);
        }

        [HttpGet("ad/email/{mail}")]
        public IActionResult AdGetByEmail(string mail)
        {
            var response = userService.CheckIfExist(mail);

            if (response.HasErrors())
                return BadRequest(response);

            var azureResponse = loginService.GetUserFromAzureAdByEmail(mail);

            if (azureResponse.HasErrors())
                return BadRequest(azureResponse);

            return Ok(azureResponse);
        }

        [HttpGet("ad/surname/{surname}")]
        public IActionResult AdGetBySurname(string surname)
        {
            var azureResponse = loginService.GetUsersFromAzureAdBySurname(surname);

            if (azureResponse.HasErrors())
                return BadRequest(azureResponse);

            return Ok(azureResponse.Data);
        }


        [HttpPut]
        [Route("{id}/active/{active}")]
        public IActionResult Active(int id, bool active)
        {
            var response = userService.Active(id, active);

            if (response.HasErrors())
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost("{userId}/role/{roleId}")]
        public IActionResult AddUserRole(int userId, int roleId)
        {
            var response = userService.AddUserRole(userId, roleId);

            if (response.HasErrors())
                return BadRequest(response);

            return Ok(response);
        }

        [HttpDelete("{userId}/role/{roleId}")]
        public IActionResult RemoveUserRole(int userId, int roleId)
        {
            var response = userService.RemoveUserRole(userId, roleId);

            if (response.HasErrors())
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost]
        [Route("{userId}/roles")]
        public IActionResult ChangeUserRoles(int userId, [FromBody]UserRoleModel model)
        {
            var response = userService.ChangeUserRoles(userId, model.RolesToAdd, model.RolesToRemove);

            if (response.HasErrors())
                return BadRequest(response);

            return Ok(response);
        }

        private UserModel Translate(User user)
        {
            var model = mapper.Map<User, UserDetailModel>(user);
            //model.employeeNumber = int.Parse(user.Employee.EmployeeNumber);

            int legajo;
            if (!int.TryParse(user.Employee.EmployeeNumber, out legajo))
            {
                legajo = 0;
            }

            model.employeeNumber = legajo;

            return model;
        }

        private List<UserModel> Translate(List<User> users)
        {
            return mapper.Map<List<User>, List<UserModel>>(users);
        }

        private UserDetailModel TranslateToUserDetailModel(User user)
        {
            return mapper.Map<User, UserDetailModel>(user);
        }
    }
}
