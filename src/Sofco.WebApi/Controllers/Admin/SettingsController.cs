﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Services.Admin;
using Sofco.Domain.Models.Admin;
using Sofco.WebApi.Extensions;

namespace Sofco.WebApi.Controllers.Admin
{
    [Route("api/settings")]
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly ISettingService settingService;

        public SettingsController(ISettingService settingService)
        {
            this.settingService = settingService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var respone = settingService.GetAll();

            return this.CreateResponse(respone);
        }

        [HttpPost]
        public IActionResult Post([FromBody] List<Setting> settings)
        {
            var response = settingService.Save(settings);

            return this.CreateResponse(response);
        }

        [HttpPost("{id}")]
        public IActionResult Post([FromBody] Setting settings)
        {
            var response = settingService.Save(settings);

            return this.CreateResponse(response);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Setting settings)
        {
            var response = settingService.Update(settings);

            return this.CreateResponse(response);
        }
    }
}
