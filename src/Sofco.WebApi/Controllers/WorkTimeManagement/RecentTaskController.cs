﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Services.WorkTimeManagement;
using Sofco.WebApi.Extensions;

namespace Sofco.WebApi.Controllers.WorkTimeManagement
{
    [Route("api/recentTask")]
    [Authorize]
    public class RecentTaskController : Controller
    {
        private readonly IRecentTaskService recentTaskService;

        public RecentTaskController(IRecentTaskService recentTaskService)
        {
            this.recentTaskService = recentTaskService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = recentTaskService.Get();

            return this.CreateResponse(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var response = recentTaskService.Delete(id);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] decimal value)
        {
            var response = recentTaskService.Update(id, value);

            return this.CreateResponse(response);
        }
    }
}
