﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Services.Common;

namespace Sofco.WebApi.Controllers
{
    [Route("api/file")]
    [Authorize]
    public class FileController : Controller
    {
        private readonly IFileService fileService;

        public FileController(IFileService fileService)
        {
            this.fileService = fileService;
        }

        [HttpGet("{id}/{type}")]
        public IActionResult GetFile(int id, int type)
        {
            var response = fileService.ExportFile(id, GetPathUrl(type));

            if (response.HasErrors())
                return BadRequest(response);

            return Ok(response);
        }

        private string GetPathUrl(int type)
        {
            switch (type)
            {
                default: return string.Empty;
            }
        }
    }
}
