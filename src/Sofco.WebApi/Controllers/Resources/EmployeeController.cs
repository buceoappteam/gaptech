﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Models.Admin;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Models.Rrhh;
using Sofco.Core.Services;
using Sofco.Core.Services.Admin;
using Sofco.Core.Services.AllocationManagement;
using Sofco.Domain.DTO;
using Sofco.Domain.Utils;
using Sofco.WebApi.Extensions;

namespace Sofco.WebApi.Controllers.Resources
{
    [Authorize]
    [Route("api/employees")]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService employeeService;
        private readonly IUserService userService;
        private readonly ILoginService loginService;

        public EmployeeController(IEmployeeService employeeService, IUserService userService, ILoginService loginService)
        {
            this.employeeService = employeeService;
            this.userService = userService;
            this.loginService = loginService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var model = employeeService.GetAll().Select(x => new EmployeeModel(x));

            return Ok(model.OrderBy(x => x.Name));
        }

        [HttpGet("options")]
        public IActionResult GetOptions()
        {
            var options = new List<Option>();

            options.AddRange(employeeService.GetAll().OrderBy(x => x.Name).Select(x => new Option { Id = x.Id, Text = $"{x.EmployeeNumber} - {x.Name}" }));

            return Ok(options);
        }

        [HttpGet("{id}/profile")]
        public IActionResult GetProfile(int id)
        {
            var response = employeeService.GetProfile(id);

            return this.CreateResponse(response);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var response = employeeService.GetById(id);

            return this.CreateResponse(response);
        }

        [HttpPost("search")]
        public IActionResult Search([FromBody] EmployeeSearchParams parameters)
        {
            var searchResponse = employeeService.Search(parameters);

            var response = 
                new Response<IEnumerable<EmployeeModel>>
                {
                    Data = searchResponse.Data.Select(x => new EmployeeModel(x)),
                    Messages = searchResponse.Messages
                };

            return this.CreateResponse(response);
        }


        [HttpPut("categories")]
        public IActionResult AddCategories([FromBody] EmployeeAddCategoriesParams parameters)
        {
            var response = employeeService.AddCategories(parameters);

            return this.CreateResponse(response);
        }


        [HttpGet("{id}/categories")]
        public IActionResult GetCategories(int id)
        {
            var response = employeeService.GetCategories(id);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] EmployeeBusinessHoursParams model)
        {
            var response = employeeService.Update(id, model);

            return this.CreateResponse(response);
        }

        [HttpGet("currentCategories")]
        public IActionResult GetCurrentCategories()
        {
            var response = employeeService.GetCurrentCategories();

            return this.CreateResponse(response);
        }

        [HttpPost("search/unemployees")]
        public IActionResult GetUnemployees([FromBody] UnemployeeSearchParameters parameters)
        {
            var model = employeeService.GetUnemployees(parameters);

            return Ok(model);
        }

        [HttpPost]
        public IActionResult AddEmployee([FromBody] EmployeeAddModel model)
        {
            var azureResponse = loginService.GetUserFromAzureAdByEmail(model.Email);

            if (azureResponse.HasErrors())
                return BadRequest(azureResponse);

            var userAlreadyExistResponse = userService.CheckIfExist(model.Email);

            if (userAlreadyExistResponse.HasErrors())
                return BadRequest(userAlreadyExistResponse);

            var response = employeeService.AddEmployee(model);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}/employee")]
        public IActionResult UpdateEmployee([FromBody] EmployeeAddModel model, int id)
        {
            var response = employeeService.UpdateEmployee(model, id);

            return this.CreateResponse(response);
        }
    }
}
