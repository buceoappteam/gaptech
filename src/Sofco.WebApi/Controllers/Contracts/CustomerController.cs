﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Services.Billing;
using Sofco.WebApi.Extensions;

namespace Sofco.WebApi.Controllers.Contracts
{
    [Route("api/customers")]
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;

        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        [HttpGet("all/options")]
        public IActionResult GetAllOptions()
        {
            var response = customerService.GetCustomersOptions();

            return this.CreateResponse(response);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = customerService.GetAllCustomers();

            return this.CreateResponse(response);
        }

        [HttpGet("{customerId}")]
        public IActionResult GetById(int customerId)
        {
            var response = customerService.GetCustomerById(customerId);

            return this.CreateResponse(response);
        }

        [HttpPost]
        public IActionResult Post([FromBody] AddCustomerModel model)
        {
            var domain = model.CreateDomain();
            var response = customerService.AddCustomer(domain);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}/active/{active}")]
        public IActionResult Active(int id, bool active)
        {
            var response = this.customerService.Active(id, active);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] AddCustomerModel model, int id)
        {
            var domain = model.CreateDomain();
            var response = this.customerService.Update(domain, id);

            return this.CreateResponse(response);
        }
    }
}
