﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Models.Billing;
using Sofco.Core.Services.AllocationManagement;
using Sofco.Domain.Enums;
using Sofco.Domain.Utils;
using Sofco.WebApi.Extensions;

namespace Sofco.WebApi.Controllers.Contracts
{
    [Authorize]
    [Route("api/analytics")]
    public class AnalyticController : Controller
    {
        private readonly IAnalyticService analyticService;

        public AnalyticController(IAnalyticService analyticServ)
        {
            analyticService = analyticServ;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var model = analyticService.GetAll().Select(x => new AnalyticSearchViewModel(x));

            return Ok(model);
        }

        [HttpGet("options")]
        public IActionResult GetOptions()
        {
            var options = new List<AnalyticOption>();

            options.AddRange(analyticService.GetAll().Select(x => new AnalyticOption { Id = x.Id, Text = $"{x.Title} - {x.Name}", Title = x.Title }));

            return Ok(options);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var response = analyticService.GetById(id);

            if (response.HasErrors())
                return BadRequest(response);

            return Ok(new AnalyticModel(response.Data));
        }

        [HttpGet("formOptions")]
        public IActionResult GetFormOptions()
        {
            return Ok(analyticService.GetOptions());
        }

        [HttpGet("{id}/resources/timeline/{dateSince}/{months}")]
        public IActionResult GetTimelineResources(int id, DateTime dateSince, int months)
        {
            var responseResources = analyticService.GetTimelineResources(id, dateSince, months);

            var model = responseResources.Data.Select(x => new ResourceForAnalyticsModel(x));

            var response = new Response<IEnumerable<ResourceForAnalyticsModel>> { Data = model };

            response.AddMessages(responseResources.Messages);

            return Ok(response);
        }

        [HttpPost]
        public IActionResult Post([FromBody] AnalyticModel model)
        {
            var response = analyticService.Add(model.CreateDomain());

            return this.CreateResponse(response);
        }

        [HttpPut]
        public IActionResult Put([FromBody] AnalyticModel model)
        {
            var response = analyticService.Update(model);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}/close")]
        public IActionResult Close(int id)
        {
            var response = analyticService.Close(id, AnalyticStatus.Close);

            return this.CreateResponse(response);
        }

        [HttpPut("{id}/closeForExpenses")]
        public IActionResult CloseForExpenses(int id)
        {
            var response = analyticService.Close(id, AnalyticStatus.CloseToExpenses);

            return this.CreateResponse(response);
        }

        [HttpGet("options/currentUser")]
        public IActionResult GetByCurrentUser()
        {
            var response = analyticService.GetByCurrentUser();

            return this.CreateResponse(response);
        }

        [HttpPost("search")]
        public IActionResult GetByParameters([FromBody] AnalyticSearchParameters query)
        {
            var response = analyticService.Get(query);

            return this.CreateResponse(response);
        }

        [HttpGet("title/costcenter/{costCenterId}")]
        public IActionResult GetNewTitle(int costCenterId)
        {
            var response = analyticService.GetNewTitle(costCenterId);

            return this.CreateResponse(response);
        }
    }
}
