﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sofco.Core.Models.Common;
using Sofco.Core.Services.Common;
using Sofco.WebApi.Extensions;

namespace Sofco.WebApi.Controllers
{
    [Route("api/userDelegate")]
    [Authorize]
    public class UserDelegateController : Controller
    {
        private readonly IUserDelegateService userDelegateService;

        public UserDelegateController(IUserDelegateService userDelegateService)
        {
            this.userDelegateService = userDelegateService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserDelegateAddModel model)
        {
            var response = userDelegateService.Add(model);

            return this.CreateResponse(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var response = userDelegateService.Delete(id);

            return this.CreateResponse(response);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = userDelegateService.GetByUserId();

            return this.CreateResponse(response);
        }
    }
}
