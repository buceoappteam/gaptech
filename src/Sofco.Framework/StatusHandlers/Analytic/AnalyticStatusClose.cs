﻿using System;
using System.Collections.Generic;
using Sofco.Core.Config;
using Sofco.Core.DAL;
using Sofco.Core.Mail;
using Sofco.Domain.Enums;
using Sofco.Framework.MailData;

using Sofco.Domain.Utils;
using Sofco.Resources.Mails;

namespace Sofco.Framework.StatusHandlers.Analytic
{
    public static class AnalyticStatusClose
    {
        public static void Save(Domain.Models.Contracts.Analytic analytic, IUnitOfWork unitOfWork, Response response, AnalyticStatus status)
        {
            unitOfWork.AllocationRepository.RemoveAllocationByAnalytic(analytic.Id, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1));

            analytic.Status = status;
            unitOfWork.AnalyticRepository.Close(analytic);

            unitOfWork.Save();

            response.AddSuccess(status == AnalyticStatus.Close ? Resources.AllocationManagement.Analytic.CloseSuccess : Resources.AllocationManagement.Analytic.CloseForExpensesSuccess);
        }

        public static void SendMail(Response response, Domain.Models.Contracts.Analytic analytic, EmailConfig emailConfig, IMailSender mailSender, IUnitOfWork unitOfWork, IMailBuilder mailBuilder)
        {
            var recipients = new List<string>();

            var manager = unitOfWork.UserRepository.GetSingle(x => x.Id == analytic.ManagerId);

            if (manager != null) recipients.Add(manager.Email);

            var title = analytic.Status == AnalyticStatus.Close ? MailSubjectResource.CloseAnalytic : MailSubjectResource.CloseForExpensesAnalytic;
            var message = analytic.Status == AnalyticStatus.Close ? MailMessageResource.CloseAnalytic : MailMessageResource.CloseForExpensesAnalytic;

            var data = new CloseAnalyticData
            {
                Title = string.Empty,
                //Message = string.Format(message, $"{analytic.Title} - {analytic.Name}", analytic.ServiceName, $"{emailConfig.SiteUrl}contracts/analytics/{analytic.Id}/view"),
                Message = string.Empty,
                Recipients = recipients
            };

            var email = mailBuilder.GetEmail(data);

            mailSender.Send(email);
        }
    }
}
