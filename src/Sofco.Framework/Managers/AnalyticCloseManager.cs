﻿using System;
using Microsoft.Extensions.Options;
using Sofco.Core.Config;
using Sofco.Core.DAL;
using Sofco.Core.Logger;
using Sofco.Core.Mail;
using Sofco.Core.Managers;
using Sofco.Domain.Enums;
using Sofco.Domain.Utils;
using Sofco.Framework.StatusHandlers.Analytic;
using Sofco.Framework.ValidationHelpers.AllocationManagement;

namespace Sofco.Framework.Managers
{
    public class AnalyticCloseManager : IAnalyticCloseManager
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly ILogMailer<AnalyticCloseManager> logger;

        private readonly EmailConfig emailConfig;

        private readonly IMailSender mailSender;

        private readonly IMailBuilder mailBuilder;

        public AnalyticCloseManager(IUnitOfWork unitOfWork, 
            ILogMailer<AnalyticCloseManager> logger, 
            IOptions<EmailConfig> emailOptions, 
            IMailSender mailSender, 
            IMailBuilder mailBuilder)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.mailSender = mailSender;
            this.mailBuilder = mailBuilder;
            emailConfig = emailOptions.Value;
        }

        public Response Close(int analyticId, AnalyticStatus status)
        {
            var response = new Response();
            var analytic = AnalyticValidationHelper.Find(response, unitOfWork, analyticId);

            if (response.HasErrors()) return response;

            try
            {
                AnalyticStatusClose.Save(analytic, unitOfWork, response, status);
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
                response.AddError(Resources.Common.ErrorSave);

                return response;
            }

            try
            {
                AnalyticStatusClose.SendMail(response, analytic, emailConfig, mailSender, unitOfWork, mailBuilder);
            }
            catch (Exception ex)
            {
                response.AddWarning(Resources.Common.ErrorSendMail);
                logger.LogError(ex);
            }

            return response;
        }
    }
}
