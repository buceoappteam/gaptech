﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using Sofco.Common.Security.Interfaces;
using Sofco.Common.Settings;
using Sofco.Core.Data.Admin;
using Sofco.Core.DAL;
using Sofco.Core.Managers;
using Sofco.Core.Models.Admin;
using Sofco.Domain.Models.Admin;

namespace Sofco.Framework.Managers
{
    public class RoleManager : IRoleManager
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly ISessionManager sessionManager;

        private readonly IUserData userData;

        private readonly AppSetting appSetting;

        public UserLiteModel CurrentUser { get; set; }

        public RoleManager(
            IUnitOfWork unitOfWork, 
            ISessionManager sessionManager,
            IUserData userData,
            IOptions<AppSetting> appSettingOptions)
        {
            this.unitOfWork = unitOfWork;
            this.sessionManager = sessionManager;
            this.userData = userData;
            this.appSetting = appSettingOptions.Value;

            CurrentUser = userData.GetCurrentUser();
        }

        public List<Role> GetRoles()
        {
            var roles = unitOfWork.RoleRepository.GetRolesByUser(sessionManager.GetUserName()).ToList();

            return roles;
        }

        public bool IsManager()
        {
            return unitOfWork.RoleRepository.HasRole(CurrentUser.Id, appSetting.ManagerRole);
        }

        public bool IsManager(int currentUserId)
        {
            return unitOfWork.RoleRepository.HasRole(currentUserId, appSetting.ManagerRole);
        }

        public bool IsDirector()
        {
            return unitOfWork.RoleRepository.HasRole(CurrentUser.Id, appSetting.DirectorRole);
        }

        public bool IsDirector(int currentUserId)
        {
            return unitOfWork.RoleRepository.HasRole(currentUserId, appSetting.DirectorRole);
        }

        public bool IsHourDelegate(int currentUserId)
        {
            return unitOfWork.RoleRepository.HasRole(currentUserId, appSetting.HourDelegateRole);
        }
    }
}
