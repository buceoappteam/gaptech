﻿using System;
using System.Linq;
using Sofco.Core.Data.Admin;
using Sofco.Core.Data.Resources;
using Sofco.Core.DAL;
using Sofco.Core.Logger;
using Sofco.Core.Managers;
using Sofco.Core.Models.Admin;
using Sofco.Core.Validations;
using Sofco.Domain.Utils;

namespace Sofco.Framework.Managers
{
    public class WorkTimeSendManager : IWorkTimeSendManager
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IUserData userData;

        private readonly ILogMailer<WorkTimeSendManager> logger;

        private readonly IWorkTimeSendMailManager workTimeSendMailManager;

        private readonly IWorkTimeValidation workTimeValidation;

        private readonly IRoleManager roleManager;

        public WorkTimeSendManager(IWorkTimeSendMailManager workTimeSendMailManager, 
            IUserData userData, 
            IUnitOfWork unitOfWork, 
            ILogMailer<WorkTimeSendManager> logger,
            IWorkTimeValidation workTimeValidation,
            IRoleManager roleManager)
        {
            this.workTimeSendMailManager = workTimeSendMailManager;
            this.userData = userData;
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.workTimeValidation = workTimeValidation;
            this.roleManager = roleManager;
        }
         
        public Response Send()
        {
            var currentUser = userData.GetCurrentUser();

            var response = Validate(currentUser);
            if (response.HasErrors())
            {
                return response;
            }

            var isManager = roleManager.IsManager(currentUser.Id);

            var draftWorkTimes = unitOfWork.WorkTimeRepository.GetWorkTimeDraftByEmployeeId(currentUser.Employee.Id);

            try
            {
                if (isManager)
                    unitOfWork.WorkTimeRepository.SendManagerHours(currentUser.Employee.Id);
                else
                    unitOfWork.WorkTimeRepository.SendHours(currentUser.Employee.Id);

                response.AddSuccess(Resources.WorkTimeManagement.WorkTime.SentSuccess);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Resources.Common.ErrorSave);
            }

            if (!response.HasErrors() && !isManager)
            {
                workTimeSendMailManager.SendEmail(draftWorkTimes);
            }

            return response;
        }

        private Response Validate(UserLiteModel currentUser)
        {
            var response = new Response();

            var pendingWorkTimes = unitOfWork.WorkTimeRepository.GetWorkTimeDraftByEmployeeId(currentUser.Employee.Id);

            if (!pendingWorkTimes.Any())
            {
                response.AddError(Resources.WorkTimeManagement.WorkTime.NoPendingHours);
                return response;
            }

            workTimeValidation.ValidateAllocations(response, pendingWorkTimes);

            return response;
        }
    }
}
