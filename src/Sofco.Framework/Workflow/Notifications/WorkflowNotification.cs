﻿using System.Collections.Generic;
using System.Linq;
using Sofco.Common.Settings;
using Sofco.Core.DAL;
using Sofco.Core.Mail;
using Sofco.Core.Models.Workflow;
using Sofco.Domain.Enums;
using Sofco.Domain.Interfaces;
using Sofco.Domain.Models.Workflow;
using Sofco.Framework.MailData;

namespace Sofco.Framework.Workflow.Notifications
{
    public abstract class WorkflowNotification
    {
        private readonly IMailSender mailSender;
        private readonly IUnitOfWork unitOfWork;
        private readonly AppSetting appSetting;

        protected WorkflowNotification(IMailSender mailSender,
            AppSetting appSetting,
            IUnitOfWork unitOfWork)
        {
            this.mailSender = mailSender;
            this.unitOfWork = unitOfWork;
            this.appSetting = appSetting;
        }

        public abstract void Send(WorkflowEntity entity, WorkflowStateTransition transition,
            WorkflowChangeStatusParameters parameters);

        protected void SendMail(string subject, string body, WorkflowStateTransition transition, WorkflowEntity entity)
        {
            var recipientsList = new List<string>();

            foreach (var stateNotifier in transition.WorkflowStateNotifiers)
            {
                AddUser(recipientsList, stateNotifier);
                AddApplicant(entity, recipientsList, stateNotifier);
            }

            var data = new MailDefaultData()
            {
                Title = subject,
                Message = body,
                Recipients = recipientsList.Distinct().ToList()
            };

            mailSender.Send(data);
        }

        private void AddApplicant(WorkflowEntity entity, List<string> recipientsList, WorkflowStateNotifier stateNotifier)
        {
            if (stateNotifier.UserSource.Code == appSetting.ApplicantUserSource)
            {
                if (entity.UserApplicant != null && !string.IsNullOrWhiteSpace(entity.UserApplicant.Email))
                    recipientsList.Add(entity.UserApplicant.Email);
            }
        }

        private void AddUser(List<string> recipientsList, WorkflowStateNotifier stateNotifier)
        {
            if (stateNotifier.UserSource.Code == appSetting.UserUserSource)
            {
                var user = unitOfWork.UserRepository.Get(stateNotifier.UserSource.SourceId);

                if (user != null && !string.IsNullOrWhiteSpace(user.Email))
                    recipientsList.Add(user.Email);
            }
        }
    }
}
