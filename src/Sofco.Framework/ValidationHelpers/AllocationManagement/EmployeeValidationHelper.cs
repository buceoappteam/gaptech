﻿using Sofco.Core.DAL;
using Sofco.Core.DAL.Resources;
using Sofco.Core.Models.Admin;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Models.Rrhh;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Utils;

namespace Sofco.Framework.ValidationHelpers.AllocationManagement
{
    public static class EmployeeValidationHelper
    {
        public static void Exist(Response response, IEmployeeRepository employeeRepository, int employeeId)
        {
            var exist = employeeRepository.Exist(employeeId);

            if (!exist)
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.NotFound, MessageType.Error));
            }
        }

        public static Employee Find(Response response, IEmployeeRepository employeeRepository, int id)
        {
            var employee = employeeRepository.GetById(id);

            if (employee == null)
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.NotFound, MessageType.Error));
                response.Status = ResponseStatus.NotFound;
            }

            return employee;
        }

        public static void ValidateBusinessHours(Response response, EmployeeBusinessHoursParams model)
        {
            if (model.BusinessHours.GetValueOrDefault() < 1 || model.BusinessHours.GetValueOrDefault() > 8)
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.BusinessHoursWrong, MessageType.Error));
            }

            if (string.IsNullOrWhiteSpace(model.BusinessHoursDescription))
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.BusinessHoursEmpty, MessageType.Error));
            }
        }

        public static void ValidateBillingPercentage(Response response, EmployeeBusinessHoursParams model)
        {
            if (!model.BillingPercentage.HasValue)
            {
                response.AddError(Resources.AllocationManagement.Employee.BillingPercentageRequired);
            }
        }

        public static void ValidateManager(Response response, EmployeeBusinessHoursParams model, IUnitOfWork unitOfWork)
        {
            if (!model.ManagerId.HasValue || model.ManagerId.Value <= 0)
            {
                response.AddError(Resources.AllocationManagement.Employee.ManagerRequired);
            }
            else
            {
                var user = unitOfWork.UserRepository.Get(model.ManagerId.Value);

                if (user == null)
                {
                    response.AddError(Resources.Admin.User.ManagerNotFound);
                }
            }
        }

        public static void ValidateOffice(Response response, EmployeeBusinessHoursParams model)
        {
            if (string.IsNullOrWhiteSpace(model.Office))
            {
                response.AddError(Resources.AllocationManagement.Employee.OfficeRequired);
            }
        }

        public static void ValidateAdd(Response response, EmployeeAddModel model)
        {
            if (string.IsNullOrEmpty(model.Percentage.ToString()))
            {
                response.AddError(Resources.AllocationManagement.Employee.BillingPercentageRequired);
            }
            
            if (string.IsNullOrEmpty(model.BusinessHours.ToString()))
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.BusinessHoursWrong, MessageType.Error));
            }
            else
            {
                if (model.BusinessHours < 1 || model.BusinessHours > 8)
                {
                    response.Messages.Add(new Message(Resources.AllocationManagement.Employee.BusinessHoursWrong, MessageType.Error));
                }
            }

            if (string.IsNullOrWhiteSpace(model.EmployeeNumber))
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.NumberRequired, MessageType.Error));
            }

            if (string.IsNullOrWhiteSpace(model.Name))
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.NameRequired, MessageType.Error));
            }

            if (string.IsNullOrWhiteSpace(model.PhoneCountryCode.ToString()) || string.IsNullOrWhiteSpace(model.PhoneAreaCode.ToString()) || string.IsNullOrWhiteSpace(model.PhoneNumber.ToString()))
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.PhoneRequired, MessageType.Error));
            }

            if (model.ProvinceId == 0)
            {
                response.Messages.Add(new Message(Resources.AllocationManagement.Employee.ProvinceRequired, MessageType.Error));
            }
        }
    }
}
