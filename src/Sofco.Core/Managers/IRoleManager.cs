﻿using System.Collections.Generic;
using Sofco.Domain.Models.Admin;

namespace Sofco.Core.Managers
{
    public interface IRoleManager
    {
        List<Role> GetRoles();
        bool IsManager();
        bool IsManager(int currentUserId);
        bool IsDirector(int currentUserId);
        bool IsDirector();
        bool IsHourDelegate(int currentUserId);
    }
}