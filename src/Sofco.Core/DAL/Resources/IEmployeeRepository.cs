﻿using System;
using System.Collections.Generic;
using Sofco.Core.DAL.Common;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.DTO;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Relationships;
using Sofco.Domain.Utils;

namespace Sofco.Core.DAL.Resources
{
    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
        new ICollection<Employee> GetAll();

        bool Exist(int employeeId);

        Employee GetById(int id);

        Employee GetByEmail(string email);

        ICollection<Employee> Search(EmployeeSearchParams parameters);

        IList<EmployeeCategory> GetEmployeeCategories(int employeeId);

        void UpdateBusinessHours(Employee employee);

        IList<Employee> SearchUnemployees(UnemployeeSearchParameters parameters);

        IList<Employee> GetByAnalyticWithWorkTimes(int analyticId);

        IList<Employee> GetUnassignedBetweenDays(DateTime startDate, DateTime endDate);

        IList<Employee> GetByAnalyticIds(List<int> analyticIds);
        IList<Employee> GetByAnalyticIdInCurrentDate(int analyticId);
    }
}
