﻿using System;
using System.Collections.Generic;
using Sofco.Core.DAL.Common;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.Domain.DTO;
using Sofco.Domain.Models.Resources;

namespace Sofco.Core.DAL.Resources
{
    public interface IAllocationRepository : IBaseRepository<Allocation>
    {
        ICollection<Allocation> GetAllocationsBetweenDays(int employeeId, DateTime startDate, DateTime endDate);
        void UpdatePercentage(Allocation allocation);
        void UpdateReleaseDate(Allocation allocation);
        ICollection<Allocation> GetByEmployee(int id);
        ICollection<Employee> GetByEmployeesForReport(AllocationReportParams parameters);
        void RemoveAllocationByAnalytic(int analyticId, DateTime today);
        ICollection<Allocation> GetAllocationsLiteBetweenDaysForWorkTimeControl(int employeeId, DateTime startDate, DateTime endDate);
        ICollection<Employee> GetByAnalyticId(int analyticId);
        void DeleteAllocationWithReleaseDateNull();
        DateTime GetStartDate(int analitycId, int employeeId);
        bool ExistCurrentAllocationByEmployeeAndManagerId(int employeeId, int managerId, DateTime startDate);
    }
}
