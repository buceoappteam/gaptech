﻿using System;
using System.Collections.Generic;
using Sofco.Core.DAL.Common;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.Core.DAL.WorkTimeManagement
{
    public interface IWorkTimeRepository : IBaseRepository<WorkTime>
    {
        IList<WorkTime> Get(DateTime startDate, DateTime endDate, int currentUserId);

        IList<WorkTime> SearchApproved(WorktimeHoursApprovedParams model);

        decimal GetTotalHoursByDateExceptCurrentId(DateTime date, int currentUserId, int id);

        IList<WorkTime> SearchPending(int analyticId, int userId);

        void UpdateStatus(WorkTime worktime);

        void UpdateApprovalComment(WorkTime worktime);

        void SendHours(int employeeId);

        void Save(WorkTime workTime);

        decimal GetPendingHoursByEmployeeId(int employeeId);

        IList<WorkTime> Search(SearchParams parameters);

        void InsertBulk(IList<WorkTime> workTimesToAdd);

        void SendManagerHours(int id);

        List<WorkTime> GetWorkTimeDraftByEmployeeId(int employeeId);
        IList<WorkTime> GetByAnalyticIds(DateTime startDate, DateTime endDate, List<int> getAnalyticIds);
        IList<WorkTime> SearchPendingByEmployee(int employeeId, int userId, int analyticId);
    }
}
