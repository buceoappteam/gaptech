﻿using System.Collections.Generic;
using Sofco.Core.DAL.Common;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.Core.DAL.WorkTimeManagement
{
    public interface IRecentTaskRepository : IBaseRepository<RecentTask>
    {
        bool Exist(int taskId, int analyticId);
        IList<RecentTask> GetByUser(int currentUserId);
    }
}
