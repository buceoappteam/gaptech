﻿using System;
using System.Collections.Generic;
using Sofco.Core.DAL.Common;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;

namespace Sofco.Core.DAL.Contracts
{
    public interface IAnalyticRepository : IBaseRepository<Analytic>
    {
        bool Exist(int id);

        IList<Allocation> GetTimelineResources(int id, DateTime startDate, DateTime endDate);

        IList<Employee> GetResources(int id);

        IList<Employee> GetResources(int id, DateTime startDate, DateTime endDate);

        Analytic GetLastAnalytic(int costCenterId);

        bool ExistTitle(string analyticTitle);

        void Close(Analytic analytic);

        ICollection<Analytic> GetAllOpenReadOnly();

        ICollection<Analytic> GetByClient(int clientId, bool onlyActives);

        Analytic GetById(int allocationAnalyticId);

        ICollection<Analytic> GetByManagerIdAndDirectorId(int managerId);

        List<AnalyticLiteModel> GetLiteByManagerId(int managerId);

        AnalyticLiteModel GetAnalyticLiteById(int id);

        IList<Analytic> GetAnalyticsLiteByEmployee(int employeeId, int userId, DateTime dateFrom, DateTime dateTo);

        Analytic GetByTitle(string title);

        List<Analytic> GetBySearchCriteria(AnalyticSearchParameters searchCriteria);

        List<Analytic> GetForReport(List<int> analytics);

        List<Analytic> GetByAllocations(int employeeId, DateTime dateFrom, DateTime dateTo);

        bool IsManager(int analyticId, int currentUserId);
        bool IsDirector(int analyticId, int currentUserId);
        IList<int> GetAnalyticsIdByEmployee(int employeeId);
    }
}
