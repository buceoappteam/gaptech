﻿using System.Collections.Generic;
using Sofco.Core.DAL.Common;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Core.DAL.Contracts
{
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
        void UpdateStatus(Customer customer);
        IList<Customer> GetAllActives();
        Customer GetById(int id);
    }
}
