﻿using Sofco.Core.DAL.Common;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Core.DAL.Contracts
{
    public interface ICostCenterRepository : IBaseRepository<CostCenter>
    {
        bool ExistCode(int domainCode);
    }
}
