﻿using System.Collections.Generic;
using Sofco.Domain.Utils;

namespace Sofco.Core.DAL.Common
{
    public interface IUtilsRepository
    {
        IList<Solution> GetSolutions();
        IList<Technology> GetTechnologies();
        IList<Product> GetProducts();
        IList<SoftwareLaw> GetSoftwareLaws();
        IList<ServiceType> GetServiceTypes();
        IList<Country> GetCountries();
        IList<Province> GetProvinces(int countryId);
        IList<DocumentType> GetDocumentTypes();
    }
}
