﻿using System.Collections.Generic;
using Sofco.Core.Models.Common;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Common;

namespace Sofco.Core.DAL.Common
{
    public interface IUserDelegateRepository : IBaseRepository<UserDelegate>
    {
        bool Exist(UserDelegateAddModel model, int userId);
        IList<UserDelegate> GetByUserId(int userId);
        IList<UserDelegate> GetByGrantedUserIdAndType(int currentUserId, UserDelegateType type);
    }
}
