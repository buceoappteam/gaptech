﻿using Sofco.Core.DAL.Admin;
using Sofco.Core.DAL.Common;
using Sofco.Core.DAL.Contracts;
using Sofco.Core.DAL.Resources;
using Sofco.Core.DAL.Workflow;
using Sofco.Core.DAL.WorkTimeManagement;

namespace Sofco.Core.DAL
{
    public interface IUnitOfWork
    {
        #region Admin
        IUserRepository UserRepository { get; }
        IRoleRepository RoleRepository { get; }
        IModuleRepository ModuleRepository { get; }
        IFunctionalityRepository FunctionalityRepository { get; }
        IMenuRepository MenuRepository { get; }
        ISettingRepository SettingRepository { get; }
        IRoleFunctionalityRepository RoleFunctionalityRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        ITaskRepository TaskRepository { get; }
        IWorkflowStateRepository WorkflowStateRepository { get; }
        IUserRoleRepository UserRoleRepository { get; }

        #endregion

        #region Billing

        ICustomerRepository CustomerRepository { get; }

        #endregion

        #region AllocationManagement

        IAllocationRepository AllocationRepository { get; }
        IAnalyticRepository AnalyticRepository { get; }
        ICostCenterRepository CostCenterRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }

        #endregion

        #region HumanResources

        ICloseDateRepository CloseDateRepository { get; }

        #endregion

        #region Common

        IUtilsRepository UtilsRepository { get; }

        IFileRepository FileRepository { get; }
        IUserDelegateRepository UserDelegateRepository { get; }

        #endregion

        #region WorkTimeManagement

        IWorkTimeRepository WorkTimeRepository { get; }

        IHolidayRepository HolidayRepository { get; }
        IRecentTaskRepository RecentTaskRepository { get; }

        #endregion

        IWorkflowRepository WorkflowRepository { get; }

        IUserSourceRepository UserSourceRepository { get; }

        void Save();
    }
}
