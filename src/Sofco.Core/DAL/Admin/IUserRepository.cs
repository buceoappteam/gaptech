﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Sofco.Core.DAL.Common;
using Sofco.Core.Models.Admin;
using Sofco.Domain.Models.Admin;

namespace Sofco.Core.DAL.Admin
{
    public interface IUserRepository : IBaseRepository<User>
    {
        bool ExistById(int id);
        IList<User> GetAllActivesReadOnly();
        IList<User> GetAllWithEmployee();
        IList<User> GetAllFullReadOnly();
        User GetSingleWithUserGroup(Expression<Func<User, bool>> predicate);
        bool ExistByMail(string mail);
        UserLiteModel GetUserLiteByUserName(string userName);
        User GetByEmail(string email);
        IList<User> GetByRole(string roleCode);
        bool IsActive(string userMail);
        UserLiteModel GetUserLiteById(int userId);
        IList<User> GetByIdsWithRoles(IEnumerable<int> ids);
        User GetByIdWithEmployee(int id);
    }
}
