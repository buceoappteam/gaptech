﻿using Sofco.Core.DAL.Common;
using Sofco.Domain.Relationships;

namespace Sofco.Core.DAL.Admin
{
    public interface IUserRoleRepository : IBaseRepository<UserRole>
    {
        bool ExistById(int userId, int roleId);
    }
}
