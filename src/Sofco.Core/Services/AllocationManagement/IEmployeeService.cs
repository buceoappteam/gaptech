﻿using Sofco.Domain.Utils;
using System.Collections.Generic;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.DTO;
using Sofco.Domain.Models.Resources;
using Sofco.Core.Models.Admin;

namespace Sofco.Core.Services.AllocationManagement
{
    public interface IEmployeeService
    {
        ICollection<Employee> GetAll();

        Response<EmployeeModel> GetById(int id);

        Response<ICollection<Employee>> Search(EmployeeSearchParams parameters);

        Response<EmployeeProfileModel> GetProfile(int id);

        Response AddCategories(EmployeeAddCategoriesParams parameters);

        Response<IList<EmployeeCategoryOption>> GetCategories(int id);

        Response Update(int id, EmployeeBusinessHoursParams model);

        Response<IList<EmployeeCategoryOption>> GetCurrentCategories();

        IList<UnemployeeListItemModel> GetUnemployees(UnemployeeSearchParameters parameters);

        Response AddEmployee(EmployeeAddModel employee);

        Response UpdateEmployee(EmployeeAddModel employee, int id);
    }
}
