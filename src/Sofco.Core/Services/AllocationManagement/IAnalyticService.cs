﻿using System;
using Sofco.Domain.Utils;
using System.Collections.Generic;
using Sofco.Core.Models;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Models.Billing;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;

namespace Sofco.Core.Services.AllocationManagement
{
    public interface IAnalyticService
    {
        ICollection<Analytic> GetAll();

        Response<Analytic> GetById(int id);

        Response<IList<Allocation>> GetTimelineResources(int id, DateTime dateSince, int months);

        AnalyticOptions GetOptions();

        Response<Analytic> Add(Analytic analytic);

        Response<Analytic> Update(AnalyticModel domain);

        Response Close(int analyticId, AnalyticStatus status);

        Response<List<Option>> GetByCurrentUser();

        Response<List<AnalyticSearchViewModel>> Get(AnalyticSearchParameters searchParameters);

        Response<NewTitleModel> GetNewTitle(int costCenterId);
    }
}
