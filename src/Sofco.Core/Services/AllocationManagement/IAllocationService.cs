﻿using Sofco.Domain.DTO;
using Sofco.Domain.Utils;
using System;
using System.Collections.Generic;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.Models.Resources;

namespace Sofco.Core.Services.AllocationManagement
{
    public interface IAllocationService
    {
        Response<Allocation> Add(AllocationDto allocation);
        AllocationResponse GetAllocationsBetweenDays(int employeeId, DateTime startDate, DateTime endDate, IList<int> analyticIds);
        Response<AllocationReportModel> CreateReport(AllocationReportParams parameters);
        ICollection<Employee> GetByEmployeesByAnalytic(int analyticId);
        Response<byte[]> AddMassive(AllocationMassiveAddModel model);
    }
}
