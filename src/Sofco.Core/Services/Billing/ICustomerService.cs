﻿using System.Collections.Generic;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Utils;

namespace Sofco.Core.Services.Billing
{
    public interface ICustomerService
    {
        Response<IList<Customer>> GetCustomers();
        Response<IList<Customer>> GetAllCustomers();
        Response<IList<Option>> GetCustomersOptions();
        Response<Customer> GetCustomerById(int customerId);
        Response AddCustomer(Customer domain);
        Response<Customer> Active(int id, bool active);
        Response Update(Customer model, int id);
    }
}
