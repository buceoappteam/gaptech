﻿using System.Collections.Generic;
using Sofco.Core.Models.Common;
using Sofco.Domain.Utils;

namespace Sofco.Core.Services.Common
{
    public interface IUserDelegateService
    {
        Response Add(UserDelegateAddModel model);
        Response Delete(int id);
        Response<IList<UserDelegateModel>> GetByUserId();
    }
}
