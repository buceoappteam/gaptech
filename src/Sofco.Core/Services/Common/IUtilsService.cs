﻿using Sofco.Domain.Utils;
using System.Collections.Generic;

namespace Sofco.Core.Services.Common
{
    public interface IUtilsService
    {
        IList<Province> GetProvinces(int countryId);
        IList<Country> GetCountries();
        IEnumerable<Option> GetMonths();
        IEnumerable<Option> GetYears();
        IEnumerable<Option> GetCloseMonths();
        IList<DocumentType> GetDocumentTypes();
    }
}
