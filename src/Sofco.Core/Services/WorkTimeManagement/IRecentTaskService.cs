﻿using System.Collections.Generic;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.Domain.Utils;

namespace Sofco.Core.Services.WorkTimeManagement
{
    public interface IRecentTaskService
    {
        Response<IList<RecentTaskModel>> Get();
        Response Delete(int id);
        Response Update(int id, decimal value);
    }
}
