﻿using System.Collections.Generic;
using Sofco.Core.Models.Admin;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Utils;

namespace Sofco.Core.Services.Admin
{
    public interface IUserService
    {
        IList<User> GetAllReadOnly(bool active);

        Response<User> GetById(int id);

        Response<User> Active(int id, bool active);

        Response<User> AddUserRole(int userId, int roleId);

        Response<User> RemoveUserRole(int userId, int roleId);

        Response<User> ChangeUserRoles(int userId, List<int> rolesToAdd, List<int> rolesToRemove);

        Response<UserModel> GetUserInfo();

        Response CheckIfExist(string mail);

        IList<Option> GetManagers();

        IList<Option> GetDirectors();
    }
}
