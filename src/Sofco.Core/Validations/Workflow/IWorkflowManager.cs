﻿using Sofco.Domain.Interfaces;

namespace Sofco.Core.Validations.Workflow
{
    public interface IWorkflowManager
    {
        void CloseEntity(WorkflowEntity entity);
    }
}
