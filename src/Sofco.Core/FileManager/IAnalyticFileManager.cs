﻿using System.Collections.Generic;
using OfficeOpenXml;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Core.FileManager
{
    public interface IAnalyticFileManager
    {
        ExcelPackage CreateAnalyticReportExcel(IList<Analytic> analytics);
    }
}
