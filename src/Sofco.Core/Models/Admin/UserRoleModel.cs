﻿using System.Collections.Generic;

namespace Sofco.Core.Models.Admin
{
    public class UserRoleModel
    {
        public UserRoleModel()
        {
            RolesToAdd = new List<int>();
            RolesToRemove = new List<int>();
        }

        public List<int> RolesToAdd { get; set; }

        public List<int> RolesToRemove { get; set; }
    }
}
