﻿using System.Collections.Generic;
using Sofco.Domain.Utils;

namespace Sofco.Core.Models.Admin
{
    public class UserDetailModel : UserModel
    {
        public UserDetailModel()
        {
            Roles = new List<Option>();
            Modules = new List<ModuleModelDetail>();
        }

        public IList<Option> Roles { get; set; }

        public IList<ModuleModelDetail> Modules { get; set; }
    }
}
