﻿using Sofco.Domain.Models.Resources;

namespace Sofco.Core.Models.Admin
{
    public class UserLiteModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public Employee Employee { get; set; }
    }
}
