﻿using System;
using System.Collections.Generic;
using System.Text;
using Sofco.Domain.Models.Admin;

namespace Sofco.Core.Models.Admin
{
    public class FunctionalityModel
    {
        public FunctionalityModel(Functionality functionality)
        {
            Id = functionality.Id;
            Description = functionality.Description;
            Active = functionality.Active;
        }

        public string Description { get; set; }

        public bool Active { get; set; }

        public int Id { get; set; }
    }
}
