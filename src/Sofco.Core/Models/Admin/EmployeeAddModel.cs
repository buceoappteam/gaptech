﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sofco.Core.Models.Admin
{
    public class EmployeeAddModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EmployeeNumber { get; set; }
        public string Seniority { get; set; }
        public string OfficeAddress { get; set; }
        public string Profile { get; set; }
        public string Technology { get; set; }
        public string Address { get; set; }
        public string BusinessHoursDescription { get; set; }
        public string Location { get; set; }
        public string Country { get; set; }
        public string DocumentNumber { get; set; }
        public string Cuil { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? EndDate { get; set; }

        public int BusinessHours { get; set; }
        public int PhoneCountryCode { get; set; }
        public int PhoneAreaCode { get; set; }
        public int PhoneNumber { get; set; }
        public int ProvinceId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int UserId { get; set; }

        public decimal Percentage { get; set; }

        public Domain.Models.Resources.Employee CreateDomain()
        {
            var domain = new Domain.Models.Resources.Employee
            {
                EmployeeNumber = this.EmployeeNumber,
                Name = this.Name,
                Birthday = this.Birthday,
                StartDate = this.StartDate,
                EndDate = this.EndDate,
                Profile = this.Profile,
                Technology = this.Technology,
                Seniority = this.Seniority,
                BillingPercentage = this.Percentage,
                Address = this.Address,
                Location = this.Location,
                ProvinceId = this.ProvinceId,
                OfficeAddress = this.OfficeAddress,
                Email = this.Email,
                BusinessHours = this.BusinessHours,
                BusinessHoursDescription = this.BusinessHoursDescription,
                DocumentTypeId = this.DocumentTypeId,
                DocumentNumber = this.DocumentNumber,
                Cuil = this.Cuil,
                PhoneCountryCode = this.PhoneCountryCode,
                PhoneAreaCode = this.PhoneAreaCode,
                PhoneNumber = this.PhoneNumber.ToString(),
                UserId = this.UserId
            };

            return domain;
        }
    }
}
