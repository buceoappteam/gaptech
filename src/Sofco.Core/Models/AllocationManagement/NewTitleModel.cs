﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sofco.Core.Models.AllocationManagement
{
    public class NewTitleModel
    {
        public int TitleId { get; set; }
        public string Title { get; set; }
    }
}
