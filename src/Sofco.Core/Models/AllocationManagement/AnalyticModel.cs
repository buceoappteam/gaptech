﻿using System;
using System.Linq;
using Sofco.Domain;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Core.Models.AllocationManagement
{
    public class AnalyticModel : BaseEntity
    {
        public AnalyticModel()
        {
        }

        public AnalyticModel(Analytic domain)
        {
            Id = domain.Id;
            Title = domain.Title;
            Name = domain.Name;
            //SoftwareLawId = domain.SoftwareLawId;
            StartDateContract = domain.StartDateContract;
            EndDateContract = domain.EndDateContract;
            ManagerId = domain.ManagerId;
            DirectorId = domain.DirectorId;
            CustomerId = domain.CustomerId;
            Proposal = domain.Proposal;
            //SolutionId = domain.SolutionId;
            //TechnologyId = domain.TechnologyId;
            //ServiceTypeId = domain.ServiceTypeId;

            CostCenterId = domain.CostCenterId;
            Status = domain.Status;
            CreationDate = domain.CreationDate;
        }

        public string Title { get; set; }

        public string Name { get; set; }

        public int CostCenterId { get; set; }

        //public string ClientExternalId { get; set; }

       //public string ClientExternalName { get; set; }

        public int CustomerId { get; set; }

        public string ContractNumber { get; set; }

        public string Service { get; set; }

        public string ServiceId { get; set; }

        //public int? SoftwareLawId { get; set; }

        public int? ActivityId { get; set; }

        public DateTime StartDateContract { get; set; }

        public DateTime EndDateContract { get; set; }

        //public int? CommercialManagerId { get; set; }

        //public int? SectorId { get; set; }

        public int? ManagerId { get; set; }
        public int? DirectorId { get; set; }

        public string Proposal { get; set; }

        //public int? SolutionId { get; set; }

        //public int? TechnologyId { get; set; }

        public DateTime CreationDate { get; set; }

        public int? ClientGroupId { get; set; }

        //public int? ServiceTypeId { get; set; }

        //public string[] UsersQv { get; set; }

        public AnalyticStatus Status { get; set; }

        public int TitleId { get; set; }

        public virtual Analytic CreateDomain()
        {
            var domain = new Analytic();

            FillData(domain);

            domain.TitleId = TitleId;
            domain.CreationDate = DateTime.UtcNow;
            domain.Status = AnalyticStatus.Open;

            return domain;
        }

        protected void FillData(Analytic domain)
        {
            domain.Id = Id;
            domain.Title = Title;
            domain.Name = Name;
            //domain.SoftwareLawId = SoftwareLawId;
            domain.StartDateContract = StartDateContract.Date;
            domain.EndDateContract = EndDateContract.Date;
            domain.ManagerId = ManagerId.GetValueOrDefault();
            domain.DirectorId = DirectorId.GetValueOrDefault();
            domain.Proposal = Proposal;
            domain.CustomerId = CustomerId;
            //domain.SolutionId = SolutionId;
            //domain.TechnologyId = TechnologyId;
            //domain.ServiceTypeId = ServiceTypeId;

            domain.CostCenterId = CostCenterId;
        }

        public Analytic CreateDomainDaf()
        {
            var domain = new Analytic
            {
                Id = Id,
                //SoftwareLawId = SoftwareLawId,
            };

            return domain;
        }

        public void UpdateDomain(Analytic analytic)
        {
            FillData(analytic);
        }
    }
}
