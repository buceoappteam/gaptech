﻿using System;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Core.Models.AllocationManagement
{
    public class AnalyticSearchViewModel
    {
        public AnalyticSearchViewModel()
        {
        }

        public AnalyticSearchViewModel(Analytic domain)
        {
            Id = domain.Id;
            Title = domain.Title;
            Name = domain.Name;
            StartDate = domain.StartDateContract;
            EndDate = domain.EndDateContract;
            Status = domain.Status;
            ManagerName = domain.Manager?.Name;
            DirectorName = domain.Director?.Name;
            CustomerName = domain.Customer?.Name;
        }

        public string CustomerName { get; set; }

        public string DirectorName { get; set; }

        public string ManagerName { get; set; }

        public int Id { get; }

        public string Name { get; }

        public string Title { get; }

        public AnalyticStatus Status { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }
}
