﻿using Sofco.Domain.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sofco.Core.Models.AllocationManagement
{
    public class AddCustomerModel
    {
        public string Name { get; set; }
        public string Cuit { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public int ProvinceId { get; set; }
        public string Contact { get; set; }
        public bool Active { get; set; }

        public Customer CreateDomain()
        {
            var domain = new Customer
            {
                Name = this.Name,
                Cuit = this.Cuit,
                Telephone = this.Telephone,
                Address = this.Address,
                PostalCode = this.PostalCode,
                City = this.City,
                ProvinceId = this.ProvinceId,
                Contact = this.Contact,
                Active = this.Active
            };

            return domain;
        }
    }
}
