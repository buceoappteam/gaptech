﻿using System;
using Sofco.Domain;
using Sofco.Domain.Models.Resources;

namespace Sofco.Core.Models.AllocationManagement
{
    public class UnemployeeListItemModel : BaseEntity
    {
        public UnemployeeListItemModel(Employee domain)
        {
            Id = domain.Id;
            EmployeeNumber = domain.EmployeeNumber;
            Name = domain.Name;
            StartDate = domain.StartDate;
            EndDate = domain.EndDate.GetValueOrDefault();
        }

        public string Name { get; set; }

        public string EmployeeNumber { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
