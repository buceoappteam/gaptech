﻿namespace Sofco.Core.Models.AllocationManagement
{
    public class AnalyticLiteModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Name { get; set; }
    }
}
