﻿namespace Sofco.Core.Models.AllocationManagement
{
    public class AnalyticSearchParameters
    {
        public int? AnalyticId { get; set; }

        public int? CustomerId { get; set; }

        public int? AnalyticStatusId { get; set; }

        public int? ManagerId { get; set; }

        public int? DirectorId { get; set; }
    }
}
