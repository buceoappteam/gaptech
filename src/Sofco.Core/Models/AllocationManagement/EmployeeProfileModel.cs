﻿using System;
using System.Collections.Generic;

namespace Sofco.Core.Models.AllocationManagement
{
    public class EmployeeProfileModel
    {
        public EmployeeProfileModel()
        {
            Allocations = new List<EmployeeAllocationModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string EmployeeNumber { get; set; }

        public string Seniority { get; set; }

        public string Profile { get; set; }

        public string Technology { get; set; }

        public decimal Percentage { get; set; }

        public string OfficeAddress { get; set; }

        public IList<EmployeeAllocationModel> Allocations { get; set; }

        public string Address { get; set; }

        public string Location { get; set; }

        public string Province { get; set; }

        public int ProvinceId { get; set; }

        public string Country { get; set; }

        public int CountryId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? Birthday { get; set; }

        public int BusinessHours { get; set; }

        public string BusinessHoursDescription { get; set; }

        //public string DocumentNumberType { get; set; }

        public int? DocumentTypeId { get; set; }

        public int DocumentNumber { get; set; }

        public string Cuil { get; set; }
        public string Email { get; set; }

        public int PhoneCountryCode { get; set; }

        public int PhoneAreaCode { get; set; }

        public string PhoneNumber { get; set; }
    }
}
