﻿using System.Collections.Generic;
using Sofco.Domain.Models.WorkTimeManagement;

namespace Sofco.Core.Models.WorkTimeManagement
{
    public class RecentTaskModel
    {
        public int AnalyticId { get; set; }

        public string Analytic { get; set; }

        public IList<RecentTask> Tasks { get; set; }
    }
}
