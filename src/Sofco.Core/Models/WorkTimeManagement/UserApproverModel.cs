﻿namespace Sofco.Core.Models.WorkTimeManagement
{
    public class UserApproverModel
    {
        public int Id { get; set; }

        public int AnalyticId { get; set; }

        public int ApproverUserId { get; set; }

        public int UserId { get; set; }

        public int EmployeeId { get; set; }
    }
}
