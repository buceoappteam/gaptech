﻿namespace Sofco.Core.Models.WorkTimeManagement
{
    public class UserApproverQuery
    {
        public int AnalyticId { get; set; }

        public int ApprovalId { get; set; }
    }
}
