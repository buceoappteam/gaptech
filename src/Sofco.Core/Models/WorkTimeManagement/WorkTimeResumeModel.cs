﻿namespace Sofco.Core.Models.WorkTimeManagement
{
    public class WorkTimeResumeModel
    {
        public decimal HoursApproved { get; set; }

        public decimal HoursRejected { get; set; }

        public decimal HoursPending => BusinessHours - Total <= 0 ? 0 : BusinessHours - Total;

        public decimal HoursDraft{ get; set; }

        public decimal HoursPendingApproved { get; set; }

        public decimal Total => HoursApproved + HoursRejected + HoursPendingApproved + HoursDraft;

        public decimal BusinessHours { get; set; }

        public decimal HoursUntilToday { get; set; }
    }
}
