﻿using System.Collections.Generic;
using Sofco.Core.Models.Common;

namespace Sofco.Core.Models.WorkTimeManagement
{
    public class AnalyticsWithEmployees
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public IList<ResourceOption> Resources { get; set; }
    }
}
