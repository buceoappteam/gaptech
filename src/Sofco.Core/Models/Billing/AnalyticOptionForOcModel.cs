﻿namespace Sofco.Core.Models.Billing
{
    public class AnalyticOptionForOcModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int ManagerId { get; set; }

        public int DirectorId { get; set; }
    }
}
