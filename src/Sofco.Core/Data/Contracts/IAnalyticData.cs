﻿using System.Collections.Generic;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.Models.Contracts;

namespace Sofco.Core.Data.Contracts
{
    public interface IAnalyticData
    {
        AnalyticLiteModel GetLiteById(int analyticId);

        List<Analytic> GetByManagerId(int managerId);
    }
}