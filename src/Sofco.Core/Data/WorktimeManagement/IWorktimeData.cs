﻿using System.Collections.Generic;
using Sofco.Core.Models.WorkTimeManagement;

namespace Sofco.Core.Data.WorktimeManagement
{
    public interface IWorktimeData
    {
        IList<WorkTimeControlResourceModel> GetAllControlHoursReport(string username);

        void SaveControlHoursReport(IList<WorkTimeControlResourceModel> list, string username);

        void ClearControlHoursReportKey(string username);
    }
}
