﻿using Sofco.Domain.Models.Resources;

namespace Sofco.Core.Data.Resources
{
    public interface IEmployeeData
    {
        Employee GetCurrentEmployee();

        Employee GetByEmployeeId(int employeeId);
    }
}