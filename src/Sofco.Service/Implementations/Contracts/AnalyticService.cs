﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using Sofco.Core.Config;
using Sofco.Core.Data.Admin;
using Sofco.Core.Data.Resources;
using Sofco.Core.DAL;
using Sofco.Core.FileManager;
using Sofco.Core.Logger;
using Sofco.Core.Mail;
using Sofco.Core.Managers;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Models.Billing;
using Sofco.Core.Services.AllocationManagement;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Utils;
using Sofco.Framework.MailData;
using Sofco.Framework.ValidationHelpers.AllocationManagement;
using Sofco.Resources.Mails;
using Sofco.Core.Services.Admin;

namespace Sofco.Service.Implementations.Contracts
{
    public class AnalyticService : IAnalyticService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMailSender mailSender;
        private readonly ILogMailer<AnalyticService> logger;
        private readonly EmailConfig emailConfig;
        private readonly IMailBuilder mailBuilder;
        private readonly IEmployeeData employeeData;
        private readonly IAnalyticFileManager analyticFileManager;
        private readonly IUserData userData;
        private readonly IRoleManager roleManager;
        private readonly IAnalyticCloseManager analyticCloseManager;
        private readonly IUserService userService;

        public AnalyticService(IUnitOfWork unitOfWork, IMailSender mailSender, ILogMailer<AnalyticService> logger,
            IOptions<EmailConfig> emailOptions, IMailBuilder mailBuilder,
            IEmployeeData employeeData, IAnalyticFileManager analyticFileManager,
            IUserData userData, IRoleManager roleManager,
            IAnalyticCloseManager analyticCloseManager,
            IUserService userService)
        {
            this.unitOfWork = unitOfWork;
            this.mailSender = mailSender;
            this.logger = logger;
            emailConfig = emailOptions.Value;
            this.mailBuilder = mailBuilder;
            this.employeeData = employeeData;
            this.analyticFileManager = analyticFileManager;
            this.userData = userData;
            this.roleManager = roleManager;
            this.analyticCloseManager = analyticCloseManager;
            this.userService = userService;
        }

        public Response<List<Option>> GetByCurrentUser()
        {
            var employeeId = employeeData.GetCurrentEmployee().Id;
            var userId = userData.GetCurrentUser().Id;

            var closeDates = unitOfWork.CloseDateRepository.GetBeforeCurrentAndNext();

            // Item 1: DateFrom
            // Item 2: DateTo
            var period = closeDates.GetPeriodExcludeDays();

            var result = unitOfWork.AnalyticRepository.GetAnalyticsLiteByEmployee(employeeId, userId, period.Item1.Date, period.Item2.Date).ToList();

            var response = new Response<List<Option>> { Data = new List<Option>() };

            foreach (var analytic in result)
            {
                if (response.Data.All(x => x.Id != analytic.Id))
                {
                    response.Data.Add(new Option { Id = analytic.Id, Text = $"{analytic.Title} - {analytic.Name}" });
                }
            }

            return response;
        }

        public Response<List<AnalyticSearchViewModel>> Get(AnalyticSearchParameters searchParameters)
        {
            var response = new Response<List<AnalyticSearchViewModel>>
            {
                Data = Translate(unitOfWork.AnalyticRepository.GetBySearchCriteria(searchParameters))
            };

            return response;
        }

        public ICollection<Analytic> GetAll()
        {
            return unitOfWork.AnalyticRepository.GetAllReadOnly();
        }

        public AnalyticOptions GetOptions()
        {
            var options = new AnalyticOptions();

            options.Managers = this.userService.GetManagers();
            options.Directors = this.userService.GetDirectors();

            //options.Solutions = unitOfWork.UtilsRepository.GetSolutions().Select(x => new Option { Id = x.Id, Text = x.Text }).ToList();
            //options.Technologies = unitOfWork.UtilsRepository.GetTechnologies().Select(x => new Option { Id = x.Id, Text = x.Text }).ToList();
            options.Products = unitOfWork.UtilsRepository.GetProducts().Select(x => new Option { Id = x.Id, Text = x.Text }).ToList();
            //options.SoftwareLaws = unitOfWork.UtilsRepository.GetSoftwareLaws().Select(x => new Option { Id = x.Id, Text = x.Text }).ToList();
            //options.ServiceTypes = unitOfWork.UtilsRepository.GetServiceTypes().Select(x => new Option { Id = x.Id, Text = x.Text }).ToList();

            return options;
        }

        public Response<Analytic> GetById(int id)
        {
            var response = new Response<Analytic>();

            response.Data = AnalyticValidationHelper.Find(response, unitOfWork, id);

            return response;
        }

        public Response<IList<Allocation>> GetTimelineResources(int id, DateTime dateSince, int months)
        {
            var response = new Response<IList<Allocation>>();

            var startDate = new DateTime(dateSince.Year, dateSince.Month, 1);
            var endDateAux = dateSince.AddMonths(months - 1);
            var endDate = new DateTime(endDateAux.Year, endDateAux.Month, DateTime.DaysInMonth(endDateAux.Year, endDateAux.Month));

            var resources = unitOfWork.AnalyticRepository.GetTimelineResources(id, startDate, endDate);

            if (!resources.Any())
            {
                response.Messages.Add(new Message(Sofco.Resources.AllocationManagement.Analytic.ResourcesNotFound, MessageType.Warning));
            }

            response.Data = resources;

            return response;
        }

        public Response<Analytic> Add(Analytic analytic)
        {
            var response = new Response<Analytic>();

            AnalyticValidationHelper.CheckTitle(response, analytic, unitOfWork.CostCenterRepository);
            AnalyticValidationHelper.CheckIfTitleExist(response, analytic, unitOfWork.AnalyticRepository);
            AnalyticValidationHelper.CheckName(response, analytic);
            AnalyticValidationHelper.CheckDirector(response, analytic);
            AnalyticValidationHelper.CheckManager(response, analytic);
            AnalyticValidationHelper.CheckCustomer(response, analytic);
            AnalyticValidationHelper.CheckCostCenter(response, analytic);
            AnalyticValidationHelper.CheckDates(response, analytic);

            if (response.HasErrors()) return response;

            try
            {
                //if (analytic.SolutionId == 0) analytic.SolutionId = null;
                //if (analytic.TechnologyId == 0) analytic.TechnologyId = null;
                //if (analytic.ServiceTypeId == 0) analytic.ServiceTypeId = null;

                unitOfWork.AnalyticRepository.Insert(analytic);

                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.AllocationManagement.Analytic.SaveSuccess);
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            SendMail(analytic, response);

            return response;
        }

        public Response<Analytic> Update(AnalyticModel analyticModel)
        {
            var response = new Response<Analytic>();

            var analytic = AnalyticValidationHelper.Find(response, unitOfWork, analyticModel.Id);

            analyticModel.UpdateDomain(analytic);

            AnalyticValidationHelper.CheckName(response, analytic);
            AnalyticValidationHelper.CheckDirector(response, analytic);
            AnalyticValidationHelper.CheckDates(response, analytic);

            if (response.HasErrors()) return response;

            try
            {
                unitOfWork.AnalyticRepository.Update(analytic);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.AllocationManagement.Analytic.UpdateSuccess);
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        public Response Close(int analyticId, AnalyticStatus status)
        {
            return analyticCloseManager.Close(analyticId, status);
        }

        private void SendMail(Analytic analytic, Response response)
        {
            try
            {
                var subject = string.Format(MailSubjectResource.AddAnalytic);
                var body = string.Format(MailMessageResource.AddAnalytic, $"{analytic.Title} - {analytic.Name}", $"{emailConfig.SiteUrl}contracts/analytics/{analytic.Id}/view");

                var recipientsList = new List<string>();

                var manager = unitOfWork.UserRepository.GetSingle(x => x.Id == analytic.ManagerId);
                var director = unitOfWork.UserRepository.GetSingle(x => x.Id == analytic.DirectorId);

                if (manager != null) recipientsList.Add(manager.Email);
                if (director != null) recipientsList.Add(director.Email);

                var data = new AddAnalyticData
                {
                    Title = subject,
                    Message = body,
                    Recipients = recipientsList
                };

                var email = mailBuilder.GetEmail(data);

                mailSender.Send(email);
            }
            catch (Exception ex)
            {
                response.AddWarning(Sofco.Resources.Common.ErrorSendMail);
                logger.LogError(ex);
            }
        }

        public Response<NewTitleModel> GetNewTitle(int costCenterId)
        {
            var response = new Response<NewTitleModel> { Data = new NewTitleModel() };

            if (costCenterId == 0)
            {
                response.AddError(Sofco.Resources.AllocationManagement.Analytic.CostCenterIsRequired);
                return response;
            }

            //Busca la ultima analitica
            var analytic = unitOfWork.AnalyticRepository.GetLastAnalytic(costCenterId);

            if (analytic == null)
            {
                var costCenter = unitOfWork.CostCenterRepository.GetSingle(x => x.Id == costCenterId);
                response.Data.Title = $"{costCenter.Code}-{costCenter.Letter}0001";
                response.Data.TitleId = 1;
            }
            else
            {
                analytic.TitleId++;
                response.Data.Title = $"{analytic.CostCenter.Code}-{analytic.CostCenter.Letter}{analytic.TitleId.ToString().PadLeft(4, '0')}";
                response.Data.TitleId = analytic.TitleId;
            }

            return response;
        }

        private List<AnalyticSearchViewModel> Translate(List<Analytic> data)
        {
            return data.Select(x => new AnalyticSearchViewModel(x)).ToList();
        }
    }
}
