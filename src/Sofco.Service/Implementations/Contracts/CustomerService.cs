﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sofco.Core.DAL;
using Sofco.Core.Logger;
using Sofco.Core.Services.Billing;
using Sofco.Domain.Models.Contracts;
using Sofco.Domain.Utils;

namespace Sofco.Service.Implementations.Contracts
{
    public class CustomerService : ICustomerService
    {
        private readonly ILogMailer<CustomerService> logger;
        private readonly IUnitOfWork unitOfWork;

        public CustomerService(
            ILogMailer<CustomerService> logger,
            IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
        }

        public Response<IList<Customer>> GetAllCustomers()
        {
            var response = new Response<IList<Customer>> { Data = new List<Customer>() };

            response.Data = unitOfWork.CustomerRepository.GetAll();

            return response;
        }
        public Response<IList<Customer>> GetCustomers()
        {
            var response = new Response<IList<Customer>> { Data = new List<Customer>() };

            response.Data = unitOfWork.CustomerRepository.GetAllActives();

            return response;
        }

        public Response<IList<Option>> GetCustomersOptions()
        {
            var result = GetCustomers();

            var response = new Response<IList<Option>>
            {
                Data = result.Data
                    .Select(x => new Option() {Id = x.Id, Text = x.Name})
                    .OrderBy(x => x.Text)
                    .ToList()
            };

            return response;
        }

        public Response<Customer> GetCustomerById(int customerId)
        {
            var response = new Response<Customer>();

            if (customerId == 0)
            {
                response.AddError(Sofco.Resources.Billing.Customer.NotFound);
                return response;
            }

            var customer = unitOfWork.CustomerRepository.GetById(customerId);

            if (customer == null)
            {
                response.AddError(Sofco.Resources.Billing.Customer.NotFound);
                return response;
            }

            response.Data = customer;
            return response;
        }

        public Response AddCustomer(Customer domain)
        {
            var response = new Response();

            try
            {
                unitOfWork.CustomerRepository.Insert(domain);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.Billing.Customer.ConsultantAdded);
            }
            catch (Exception e)
            {
                response.AddError(Sofco.Resources.Common.ErrorSave);
                logger.LogError(e);
            }

            return response;
        }

        public Response<Customer> Active(int id, bool active)
        {
            var response = new Response<Customer>();
            var entity = unitOfWork.CustomerRepository.GetSingle(x => x.Id == id);

            if (entity != null)
            {
                entity.Active = active;

                unitOfWork.CustomerRepository.Update(entity);
                unitOfWork.Save();

                response.Data = entity;
                response.AddSuccess(active ? Sofco.Resources.Billing.Customer.ConsultantEnabled : Sofco.Resources.Billing.Customer.ConsultantDisabled);
                return response;
            }

            response.AddError(Sofco.Resources.Billing.Customer.NotFound);
            return response;
        }

        public Response Update(Customer model, int id)
        {
            var response = new Response();
            var entity = unitOfWork.CustomerRepository.GetSingle(x => x.Id == id);

            if (entity == null)
            {
                response.AddError(Sofco.Resources.Billing.Customer.ConsultantNotFound);
                return response;
            }

           if (response.HasErrors()) return response;

            try
            {
                entity.Name = model.Name;
                entity.Address = model.Address;
                entity.City = model.City;
                entity.Contact = model.Contact;
                entity.Cuit = model.City;
                entity.PostalCode = model.PostalCode;
                entity.ProvinceId = model.ProvinceId;
                entity.Telephone = model.Telephone;
            

                unitOfWork.CustomerRepository.Update(entity);
                unitOfWork.Save();
                response.AddSuccess(Sofco.Resources.Billing.Customer.ConsultantUpdated);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }
    }
}
