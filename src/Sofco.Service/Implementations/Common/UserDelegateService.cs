﻿using Microsoft.Extensions.Options;
using Sofco.Common.Settings;
using Sofco.Core.DAL;
using Sofco.Core.Data.Admin;
using Sofco.Core.Logger;
using Sofco.Core.Models.Admin;
using Sofco.Core.Models.Common;
using Sofco.Core.Services.Common;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Common;
using Sofco.Domain.Relationships;
using Sofco.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sofco.Service.Implementations.Common
{
    public class UserDelegateService : IUserDelegateService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILogMailer<UserDelegateService> logger;
        private readonly IUserData userData;
        private readonly AppSetting appSetting;

        public UserDelegateService(IUnitOfWork unitOfWork, ILogMailer<UserDelegateService> logger, IUserData userData, IOptions<AppSetting> appSettingOptions)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.userData = userData;
            this.appSetting = appSettingOptions.Value;
        }

        public Response Add(UserDelegateAddModel model)
        {
            var currentUser = userData.GetCurrentUser();

            var response = new Response();

            Validate(model, response, currentUser.Id);

            if (response.HasErrors()) return response;

            try
            {
                var userDelegate = new UserDelegate
                {
                    GrantedUserId = model.GrantedUserId,
                    UserId = currentUser.Id,
                    Type = model.Type.GetValueOrDefault(),
                    AnalyticSourceId = model.AnalyticSourceId,
                    UserSourceId = model.UserSourceId,
                    SourceType = model.SourceType,
                    Created = DateTime.UtcNow
                };

                unitOfWork.UserDelegateRepository.Insert(userDelegate);

                HandleAddDelegate(userDelegate, currentUser);

                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.Admin.UserDelegate.AddSuccess);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        private void HandleAddDelegate(UserDelegate userDelegate, UserLiteModel currentUser)
        {
            if (userDelegate.Type == UserDelegateType.WorkTimeApproval)
            {
                var role = unitOfWork.RoleRepository.GetByCode(appSetting.HourDelegateRole);

                if (!unitOfWork.UserRoleRepository.ExistById(userDelegate.GrantedUserId, role.Id))
                {
                    var userRole = new UserRole
                    {
                        RoleId = role.Id,
                        UserId = userDelegate.GrantedUserId
                    };

                    unitOfWork.UserRoleRepository.Insert(userRole);
                }
            }
        }

        public Response Delete(int id)
        {
            var response = new Response();

            var currentUser = userData.GetCurrentUser();

            var userDelegate = unitOfWork.UserDelegateRepository.Get(id);

            if (userDelegate == null)
            {
                response.AddError(Sofco.Resources.Admin.UserDelegate.NotFound);
                return response;
            }

            try
            {
                unitOfWork.UserDelegateRepository.Delete(userDelegate);

                HandleDeleteDelegate(userDelegate, currentUser);

                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.Admin.UserDelegate.DeleteSuccess);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        private void HandleDeleteDelegate(UserDelegate userDelegate, UserLiteModel currentUser)
        {
            if (userDelegate.Type == UserDelegateType.WorkTimeApproval)
            {
                var role = unitOfWork.RoleRepository.GetByCode(appSetting.HourDelegateRole);

                if (unitOfWork.UserRoleRepository.ExistById(userDelegate.GrantedUserId, role.Id))
                {
                    var userRole = new UserRole
                    {
                        RoleId = role.Id,
                        UserId = userDelegate.GrantedUserId
                    };

                    unitOfWork.UserRoleRepository.Delete(userRole);
                }
            }
        }

        public Response<IList<UserDelegateModel>> GetByUserId()
        {
            var currentUser = userData.GetCurrentUser();

            var response = new Response<IList<UserDelegateModel>> { Data = new List<UserDelegateModel>() };

            var delegates = unitOfWork.UserDelegateRepository.GetByUserId(currentUser.Id);

            foreach (var userDelegate in delegates)
            {
                var model = new UserDelegateModel(userDelegate);
                HandleGetDelegate(userDelegate, model);
                response.Data.Add(model);
            }

            return response;
        }

        private void HandleGetDelegate(UserDelegate userDelegate, UserDelegateModel model)
        {
            if (userDelegate.Type == UserDelegateType.WorkTimeApproval)
            {
                var analytic = unitOfWork.AnalyticRepository.Get(userDelegate.AnalyticSourceId.GetValueOrDefault());

                if (analytic != null)
                {
                    model.AnalyticSourceName = $"{analytic.Title} - {analytic.Name}";
                }

                if (userDelegate.SourceType == UserDelegateSourceType.User)
                {
                    var user = unitOfWork.UserRepository.Get(userDelegate.UserSourceId.GetValueOrDefault());

                    if (user != null)
                    {
                        model.UserSourceName = user.Name;
                    }
                }
                else
                {
                    model.UserSourceName = "Todos";
                }
            }
        }

        private void Validate(UserDelegateAddModel model, Response response, int currentUserId)
        {
            if (model == null)
            {
                response.AddError(Sofco.Resources.Admin.UserDelegate.ModelNull);
                return;
            }

            if (!unitOfWork.UserRepository.ExistById(model.GrantedUserId))
                response.AddError(Sofco.Resources.Admin.UserDelegate.GrantedUserNotFound);

            if (!model.Type.HasValue)
                response.AddError(Sofco.Resources.Admin.UserDelegate.TypeRequired);

            if (model.Type.HasValue && model.Type.Value == UserDelegateType.WorkTimeApproval)
            {
                if (!model.AnalyticSourceId.HasValue || model.AnalyticSourceId == 0)
                    response.AddError(Sofco.Resources.Admin.UserDelegate.AnalyticRequired);

                if (model.SourceType == UserDelegateSourceType.User && (!model.UserSourceId.HasValue || model.UserSourceId == 0))
                    response.AddError(Sofco.Resources.Admin.UserDelegate.ResourceRequired);
            }

            if (response.HasErrors()) return;

            if (unitOfWork.UserDelegateRepository.Exist(model, currentUserId))
                response.AddError(Sofco.Resources.Admin.UserDelegate.AlreadyExist);

            if(currentUserId == model.GrantedUserId)
                response.AddError(Sofco.Resources.Admin.UserDelegate.UserEqualsGrantedUser);

            if (model.SourceType == UserDelegateSourceType.User && model.GrantedUserId == model.UserSourceId)
                response.AddError(Sofco.Resources.Admin.UserDelegate.UserSourceEqualsGrantedUser);
        }
    }
}
