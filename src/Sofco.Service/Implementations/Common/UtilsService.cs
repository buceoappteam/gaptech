﻿using System;
using Sofco.Core.Services.Common;
using System.Collections.Generic;
using Sofco.Core.DAL;
using Sofco.Domain.Enums;
using Sofco.Domain.Utils;
using Sofco.Framework.Helpers;

namespace Sofco.Service.Implementations.Common
{
    public class UtilsService : IUtilsService
    {
        private readonly IUnitOfWork unitOfWork;

        public UtilsService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<Option> GetMonths()
        {
            yield return new Option { Id = 1, Text = Sofco.Resources.Months.January };
            yield return new Option { Id = 2, Text = Sofco.Resources.Months.February };
            yield return new Option { Id = 3, Text = Sofco.Resources.Months.March };
            yield return new Option { Id = 4, Text = Sofco.Resources.Months.April };
            yield return new Option { Id = 5, Text = Sofco.Resources.Months.May };
            yield return new Option { Id = 6, Text = Sofco.Resources.Months.June };
            yield return new Option { Id = 7, Text = Sofco.Resources.Months.July };
            yield return new Option { Id = 8, Text = Sofco.Resources.Months.August };
            yield return new Option { Id = 9, Text = Sofco.Resources.Months.September };
            yield return new Option { Id = 10, Text = Sofco.Resources.Months.October };
            yield return new Option { Id = 11, Text = Sofco.Resources.Months.November };
            yield return new Option { Id = 12, Text = Sofco.Resources.Months.December };
        }
        public IEnumerable<Option> GetCloseMonths()
        {
            var closeDates = unitOfWork.CloseDateRepository.GetAllBeforeNextMonth();

            for (int i = 1; i < closeDates.Count; i++)
            {
                var closeDateBefore = closeDates[i - 1];
                var closeDate = closeDates[i];

                yield return new Option { Id = closeDate.Id, Text = $"{ closeDateBefore.Day+1 } { DatesHelper.GetMonthDesc(closeDateBefore.Month) } {closeDateBefore.Year} al " +
                                                         $"{ closeDate.Day } { DatesHelper.GetMonthDesc(closeDate.Month) } {closeDateBefore.Year}" };
            }
        }

        public IEnumerable<Option> GetYears()
        {
            var today = DateTime.UtcNow;

            for (int i = today.AddYears(-5).Year; i < today.AddYears(1).Year; i++)
            {
                yield return new Option { Id = i, Text = i.ToString() };
            }
        }

        public IList<Province> GetProvinces(int countryId)
        {
            return unitOfWork.UtilsRepository.GetProvinces(countryId);
        }

        public IList<Country> GetCountries()
        {
            return unitOfWork.UtilsRepository.GetCountries();
        }

        public IList<DocumentType> GetDocumentTypes()
        {
            return unitOfWork.UtilsRepository.GetDocumentTypes();
        }
    }
}
