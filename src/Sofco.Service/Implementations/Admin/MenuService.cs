﻿using Sofco.Core.DAL;
using Sofco.Core.Managers;
using Sofco.Core.Models.Admin;
using Sofco.Core.Services.Admin;
using Sofco.Domain.Utils;
using System.Linq;

namespace Sofco.Service.Implementations.Admin
{
    public class MenuService : IMenuService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IRoleManager roleManager;

        public MenuService(IUnitOfWork unitOfWork, IRoleManager roleManager)
        {
            this.unitOfWork = unitOfWork;
            this.roleManager = roleManager;
        }

        public Response<MenuResponseModel> GetFunctionalitiesByUserName()
        {
            var roles = roleManager.GetRoles();

            var modules = unitOfWork.MenuRepository.GetFunctionalitiesByRoles(roles.Select(x => x.Id));

            var model = new MenuResponseModel();

            foreach (var item in modules)
            {
                if (!item.Functionality.Active || !item.Functionality.Module.Active) continue;

                if (model.Menus.Any(x => x.Module == item.Functionality.Module.Code && x.Functionality == item.Functionality.Code)) continue;

                var menu = new MenuModel
                {
                    Description = item.Functionality.Description,
                    Functionality = item.Functionality.Code,
                    Module = item.Functionality.Module.Code
                };

                model.Menus.Add(menu);
            }

            model.IsManager = roleManager.IsManager();
            model.IsDirector = roleManager.IsDirector();

            return new Response<MenuResponseModel> { Data = model };
        }
    }
}
