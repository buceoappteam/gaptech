﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Options;
using Sofco.Common.Security.Interfaces;
using Sofco.Common.Settings;
using Sofco.Core.DAL;
using Sofco.Core.Logger;
using Sofco.Core.Models.Admin;
using Sofco.Domain.Utils;
using Sofco.Core.Services.Admin;
using Sofco.Domain.Enums;
using Sofco.Domain.Models.Admin;
using Sofco.Domain.Relationships;
using Sofco.Framework.ValidationHelpers.AllocationManagement;

namespace Sofco.Service.Implementations.Admin
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly ILogMailer<UserService> logger;

        private readonly ISessionManager sessionManager;

        private readonly IMapper mapper;

        private readonly AppSetting appSetting;

        public UserService(IUnitOfWork unitOfWork, ILogMailer<UserService> logger, ISessionManager sessionManager, IMapper mapper, IOptions<AppSetting> appSettingOptions)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.sessionManager = sessionManager;
            this.mapper = mapper;
            this.appSetting = appSettingOptions.Value;
        }

        public Response<User> Active(int id, bool active)
        {
            var response = new Response<User>();
            var userEntity = unitOfWork.UserRepository.GetByIdWithEmployee(id);
                    
            if (userEntity != null)
            {
                userEntity.Active = active;

                if (active)
                {
                    userEntity.StartDate = DateTime.Now;
                    userEntity.EndDate = null;

                    userEntity.Employee.EndDate = null;
                }
                else
                {
                    userEntity.EndDate = DateTime.Now;
                    userEntity.Employee.EndDate = DateTime.Now;
                }

                unitOfWork.UserRepository.Update(userEntity);

                unitOfWork.Save();

                response.Data = userEntity;
                response.AddSuccess(active ? Sofco.Resources.Admin.User.Enabled : Sofco.Resources.Admin.User.Disabled);
                return response;
            }

            response.Messages.Add(new Message(Sofco.Resources.Admin.User.NotFound, MessageType.Error));
            return response;
        }

        public Response<User> AddUserRole(int userId, int roleId)
        {
            var response = new Response<User>();

            var user = unitOfWork.UserRepository.GetSingleWithUserGroup(x => x.Id == userId);

            if (user == null)
            {
                response.AddError(Sofco.Resources.Admin.User.NotFound);
                return response;
            }

            var role = unitOfWork.RoleRepository.GetSingle(x => x.Id == roleId);

            if (role == null)
            {
                response.AddError(Sofco.Resources.Admin.Role.NotFound);
                return response;
            }

            var entity = new UserRole() { UserId = userId, RoleId = roleId };

            unitOfWork.UserRoleRepository.Insert(entity);
            unitOfWork.Save();

            response.AddSuccess(Sofco.Resources.Admin.User.GroupAssigned);

            return response;
        }

        public IList<User> GetAllReadOnly(bool active)
        {
            if (active)
                return unitOfWork.UserRepository.GetAllActivesReadOnly();
            else
                return unitOfWork.UserRepository.GetAllWithEmployee();
        }

        public Response<User> GetById(int id)
        {
            var response = new Response<User>();
            var entity = unitOfWork.UserRepository.GetSingleWithUserGroup(x => x.Id == id);

            if (entity != null)
            {
                response.Data = entity;
                return response;
            }

            response.AddError(Sofco.Resources.Admin.User.NotFound);
            response.Status = ResponseStatus.NotFound; 
            return response;
        }

        public Response<User> RemoveUserRole(int userId, int roleId)
        {
            var response = new Response<User>();

            var userExist = unitOfWork.UserRepository.ExistById(userId);

            if (!userExist)
            {
                response.AddError(Sofco.Resources.Admin.User.NotFound);
                return response;
            }

            var roleExist = unitOfWork.RoleRepository.ExistById(roleId);

            if (!roleExist)
            {
                response.AddError(Sofco.Resources.Admin.Role.NotFound);
                return response;
            }

            try
            {
                var entity = new UserRole() { UserId = userId, RoleId = roleId };

                unitOfWork.UserRoleRepository.Delete(entity);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.Admin.User.GroupRemoved);
            }
            catch (Exception e)
            {
                response.AddError(Sofco.Resources.Common.ErrorSave);
                logger.LogError(e);
            }

            return response;
        }

        public Response<User> ChangeUserRoles(int userId, List<int> rolesToAdd, List<int> rolesToRemove)
        {
            var response = new Response<User>();

            var userExist = unitOfWork.UserRepository.ExistById(userId);

            if (!userExist)
            {
                response.AddError(Sofco.Resources.Admin.User.NotFound);
                return response;
            }

            try
            {
                foreach (var roleId in rolesToAdd)
                {
                    var entity = new UserRole() { UserId = userId, RoleId = roleId };
                    var roleExist = unitOfWork.RoleRepository.ExistById(roleId);
                    var userGroupExist = unitOfWork.UserRoleRepository.ExistById(userId, roleId);

                    if (roleExist && !userGroupExist)
                    {
                        unitOfWork.UserRoleRepository.Insert(entity);
                    }
                }

                foreach (var roleId in rolesToRemove)
                {
                    var entity = new UserRole() { UserId = userId, RoleId = roleId };
                    var roleExist = unitOfWork.RoleRepository.ExistById(roleId);
                    var userGroupExist = unitOfWork.UserRoleRepository.ExistById(userId, roleId);

                    if (roleExist && userGroupExist)
                    {
                        unitOfWork.UserRoleRepository.Delete(entity);
                    }
                }

                unitOfWork.Save();
                response.AddSuccess(Sofco.Resources.Admin.User.UserGroupsUpdated);
            }
            catch (Exception e)
            {
                response.AddError(Sofco.Resources.Common.ErrorSave);
                logger.LogError(e);
            }

            return response;
        }

        public IList<Option> GetManagers()
        {
            return unitOfWork.UserRepository.GetByRole(appSetting.ManagerRole).Select(x => new Option { Id = x.Id, Text = x.Name }).ToList();
        }

        public IList<Option> GetDirectors()
        {
            return unitOfWork.UserRepository.GetByRole(appSetting.DirectorRole).Select(x => new Option { Id = x.Id, Text = x.Name }).ToList();
        }

        public Response<UserModel> GetUserInfo()
        {
            var email = sessionManager.GetUserEmail();

            var response = new Response<UserModel>();

            return SetUserInfo(email, response);
        }

        private Response<UserModel> SetUserInfo(string email, Response<UserModel> response)
        {
            var user = unitOfWork.UserRepository.GetByEmail(email);

            if (user == null)
            {
                response.AddError(Sofco.Resources.Admin.User.NotFound);
                return response;
            }

            response.Data = Translate(user);

            return response;
        }

        public Response CheckIfExist(string mail)
        {
            var response = new Response();

            if (unitOfWork.UserRepository.ExistByMail(mail))
            {
                response.AddError(Sofco.Resources.Admin.User.AlreadyExist);
            }

            return response;
        }

        private UserModel Translate(User user)
        {
            return mapper.Map<User, UserModel>(user);
        }
    }
}
