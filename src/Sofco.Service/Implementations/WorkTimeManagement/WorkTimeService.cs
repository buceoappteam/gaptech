﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Sofco.Common.Settings;
using Sofco.Core.Data.Admin;
using Sofco.Core.Data.Resources;
using Sofco.Core.DAL;
using Sofco.Core.Logger;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.Core.Services.WorkTimeManagement;
using Sofco.Framework.ValidationHelpers.WorkTimeManagement;
using Sofco.Domain.Enums;
using Sofco.Domain.Utils;
using Sofco.Core.FileManager;
using Sofco.Core.Managers;
using Sofco.Core.Models.Admin;
using Sofco.Core.Models.Common;
using Sofco.Core.Services.Rrhh;
using Sofco.Core.Validations;
using Sofco.Domain.Models.WorkTimeManagement;
using Sofco.Framework.ValidationHelpers.AllocationManagement;

namespace Sofco.Service.Implementations.WorkTimeManagement
{
    public class WorkTimeService : IWorkTimeService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IUserData userData;

        private readonly ILogMailer<WorkTimeService> logger;

        private readonly IWorkTimeValidation workTimeValidation;

        private readonly IWorkTimeImportFileManager workTimeImportFileManager;

        private readonly IWorkTimeExportFileManager workTimeExportFileManager;

        private readonly IWorkTimeResumeManager workTimeResumeManger;

        private readonly IWorkTimeRejectManager workTimeRejectManager;

        private readonly IWorkTimeSendManager workTimeSendHoursManager;

        private readonly IRoleManager roleManager;

        public WorkTimeService(ILogMailer<WorkTimeService> logger,
            IUnitOfWork unitOfWork,
            IUserData userData,
            IWorkTimeValidation workTimeValidation,
            IWorkTimeImportFileManager workTimeImportFileManager,
            IWorkTimeExportFileManager workTimeExportFileManager,
            IWorkTimeResumeManager workTimeResumeManger,
            IWorkTimeRejectManager workTimeRejectManager, 
            IWorkTimeSendManager workTimeSendHoursManager,
            IRoleManager roleManager)
        {
            this.unitOfWork = unitOfWork;
            this.userData = userData;
            this.workTimeValidation = workTimeValidation;
            this.logger = logger;
            this.workTimeImportFileManager = workTimeImportFileManager;
            this.workTimeResumeManger = workTimeResumeManger;
            this.workTimeExportFileManager = workTimeExportFileManager;
            this.workTimeRejectManager = workTimeRejectManager;
            this.workTimeSendHoursManager = workTimeSendHoursManager;
            this.roleManager = roleManager;
        }

        public Response<WorkTimeModel> Get(DateTime date)
        {
            if (date == DateTime.MinValue) return new Response<WorkTimeModel>();

            var result = new Response<WorkTimeModel> { Data = new WorkTimeModel() };

            try
            {
                var currentUser = userData.GetCurrentUser();

                var startDate = new DateTime(date.Year, date.Month, 1);

                var endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);

                var workTimes = unitOfWork.WorkTimeRepository.Get(startDate, endDate, currentUser.Id);

                var calendars = workTimes.Select(x => new WorkTimeCalendarModel(x)).ToList();

                result.Data.Calendar = calendars;

                var dateUtc = date.ToUniversalTime();

                result.Data.Holidays = unitOfWork.HolidayRepository.Get(dateUtc.Year, dateUtc.Month);

                result.Data.Resume = workTimeResumeManger.GetCurrentPeriodResume();

                var closeDates = unitOfWork.CloseDateRepository.GetBeforeCurrentAndNext();

                var period = closeDates.GetPeriodIncludeDays();

                result.Data.PeriodStartDate = period.Item1;
                result.Data.PeriodEndDate = period.Item2;

                return result;
            }
            catch (Exception e)
            {
                logger.LogError(e);

                result.AddError(Sofco.Resources.Common.GeneralError);

                return result;
            }
        }

        public Response<WorkTime> Save(WorkTimeAddModel model)
        {
            var response = new Response<WorkTime>();

            model.Date = model.Date.ToUniversalTime();

            var currentUser = userData.GetCurrentUser();

            model.EmployeeId = currentUser.Employee.Id;

            WorkTimeValidationHandler.ValidateStatus(response, model);
            WorkTimeValidationHandler.ValidateEmployee(response, unitOfWork, model);
            WorkTimeValidationHandler.ValidateAnalytic(response, unitOfWork, model);
            workTimeValidation.ValidateDate(response, model);
            WorkTimeValidationHandler.ValidateTask(response, unitOfWork, model);
            WorkTimeValidationHandler.ValidateUserComment(response, model);
            workTimeValidation.ValidateHours(response, model, currentUser.Employee.BusinessHours);
            workTimeValidation.ValidateAllocations(response, model);

            if (response.HasErrors()) return response;

            try
            {
                var workTime = model.CreateDomain();
                
                unitOfWork.WorkTimeRepository.Save(workTime);

                unitOfWork.Save();

                response.Data = workTime;

                response.AddSuccess(Sofco.Resources.WorkTimeManagement.WorkTime.AddSuccess);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            if (!response.HasErrors())
            {
                try
                {
                    if (!unitOfWork.RecentTaskRepository.Exist(model.TaskId, model.AnalyticId))
                    {
                        var task = unitOfWork.TaskRepository.Get(model.TaskId);
                        var analytic = unitOfWork.AnalyticRepository.Get(model.AnalyticId);

                        var recentTask = new RecentTask
                        {
                            AnalyticId = analytic.Id,
                            Analytic = $"{analytic.Title} - {analytic.Name}",
                            CategoryId = task.CategoryId,
                            Hours = model.Hours,
                            Reference = model.Reference,
                            TaskId = task.Id,
                            Task = task.Description,
                            UserId = currentUser.Id
                        };

                        unitOfWork.RecentTaskRepository.Insert(recentTask);
                        unitOfWork.Save();
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e);
                }
            }

            return response;
        }

        public Response<IList<HoursApprovedModel>> GetHoursApproved(WorktimeHoursApprovedParams model)
        {
            var response = new Response<IList<HoursApprovedModel>>();

            if (response.HasErrors()) return response;

            model.AnalyticIds = GetAnalyticIds(model.AnalyticId);

            var list = unitOfWork.WorkTimeRepository.SearchApproved(model).ToList();

            if (!list.Any())
            {
                response.AddWarning(Sofco.Resources.WorkTimeManagement.WorkTime.SearchNotFound);
            }

            response.Data = list.Select(x => new HoursApprovedModel(x)).ToList();

            return response;
        }

        public Response<IList<HoursApprovedModel>> GetHoursPending(WorktimeHoursPendingParams model)
        {
            var response = new Response<IList<HoursApprovedModel>>();

            var currentUser = userData.GetCurrentUser();

            var delegates = unitOfWork.UserDelegateRepository.GetByGrantedUserIdAndType(currentUser.Id, UserDelegateType.WorkTimeApproval);

            var analyticsCurrentUser = unitOfWork.AnalyticRepository.GetByManagerIdAndDirectorId(currentUser.Id);

            var analyticsTuple = new List<Tuple<int, int>>();
            var analyticIdsCurrentUser = analyticsCurrentUser.Select(x => x.Id).ToList();

            foreach (var analyticId in analyticIdsCurrentUser)
                analyticsTuple.Add(new Tuple<int, int>(analyticId, currentUser.Id));

            var employeesTuple = new List<Tuple<int, int, int>>();

            foreach (var userDelegate in delegates)
            {
                if (userDelegate.SourceType == UserDelegateSourceType.Analytic)
                {
                    if (analyticsTuple.All(x => x.Item1 != userDelegate.AnalyticSourceId.GetValueOrDefault()))
                    {
                        analyticsTuple.Add(new Tuple<int, int>(userDelegate.AnalyticSourceId.GetValueOrDefault(), userDelegate.UserId));
                    }
                }

                if (userDelegate.SourceType == UserDelegateSourceType.User)
                {
                    var user = unitOfWork.UserRepository.GetUserLiteById(userDelegate.UserSourceId.GetValueOrDefault());

                    employeesTuple.Add(new Tuple<int, int, int>(user.Employee.Id, userDelegate.UserId, userDelegate.AnalyticSourceId.GetValueOrDefault()));
                }
            }

            var list = new List<WorkTime>();

            if (model.AnalyticId.HasValue && model.AnalyticId > 0)
            {
                var tuple = analyticsTuple.SingleOrDefault(x => x.Item1 == model.AnalyticId);

                if (tuple != null)
                {
                    list.AddRange(unitOfWork.WorkTimeRepository.SearchPending(tuple.Item1, tuple.Item2));
                }
            }
            else
            {
                if (analyticIdsCurrentUser.Any())
                {
                    foreach (var tuple in analyticsTuple)
                    {
                        list.AddRange(unitOfWork.WorkTimeRepository.SearchPending(tuple.Item1, tuple.Item2));
                    }
                }
            }

            foreach (var tuple in employeesTuple)
            {
                if (list.All(x => x.EmployeeId != tuple.Item1))
                {
                    if (model.EmployeeId.HasValue && model.EmployeeId.Value > 0)
                    {
                        if (model.EmployeeId.Value == tuple.Item1)
                        {
                            var worktimes = unitOfWork.WorkTimeRepository.SearchPendingByEmployee(tuple.Item1, tuple.Item2, tuple.Item3);

                            foreach (var worktime in worktimes) list.Add(worktime);
                        }
                    }
                    else
                    {
                        var worktimes = unitOfWork.WorkTimeRepository.SearchPendingByEmployee(tuple.Item1, tuple.Item2, tuple.Item3);

                        foreach (var worktime in worktimes) list.Add(worktime);
                    }
                }
            }

            if (!list.Any())
            {
                response.AddWarning(Sofco.Resources.WorkTimeManagement.WorkTime.SearchNotFound);
            }

            response.Data = new List<HoursApprovedModel>();

            foreach (var workTime in list)
            {
                var itemToAdd = new HoursApprovedModel(workTime);
                response.Data.Add(itemToAdd);
            }

            return response;
        }

        public Response Approve(int id)
        {
            var response = new Response();

            var currentUser = userData.GetCurrentUser();

            var isManager = roleManager.IsManager(currentUser.Id);
            var isDirector = roleManager.IsDirector(currentUser.Id);
            var delegates = unitOfWork.UserDelegateRepository.GetByGrantedUserIdAndType(currentUser.Id, UserDelegateType.WorkTimeApproval);

            if (!isManager && !isDirector && !delegates.Any())
            {
                response.AddError(Sofco.Resources.WorkTimeManagement.WorkTime.CannotChangeStatus);
                return response;
            }

            return Approve(id, currentUser, isManager, isDirector);
        }

        private Response Approve(int id, UserLiteModel currentUser, bool isManager, bool isDirector)
        {
            var response = new Response();

            var worktime = unitOfWork.WorkTimeRepository.GetSingle(x => x.Id == id);

            try
            {
                WorkTimeValidationHandler.ValidateApproveOrReject(worktime, response);

                if (response.HasErrors()) return response;

                var statusChanged = false;

                if (worktime.Status == WorkTimeStatus.Sent && isManager && unitOfWork.AnalyticRepository.IsManager(worktime.AnalyticId, currentUser.Id))
                {
                    statusChanged = true;
                    worktime.Status = WorkTimeStatus.SentToDirector;
                }

                if (worktime.Status == WorkTimeStatus.SentToDirector && isDirector && unitOfWork.AnalyticRepository.IsDirector(worktime.AnalyticId, currentUser.Id))
                {
                    statusChanged = true;
                    worktime.Status = WorkTimeStatus.Approved;
                }

                if (!statusChanged)
                {
                    response.AddError(Sofco.Resources.WorkTimeManagement.WorkTime.CannotChangeStatus);
                    return response;
                }
                
                worktime.ApprovalUserId = currentUser.Id;

                unitOfWork.WorkTimeRepository.UpdateStatus(worktime);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.WorkTimeManagement.WorkTime.ApprovedSuccess);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        public IList<AnalyticsWithEmployees> GetAnalytics()
        {
            var currentUser = userData.GetCurrentUser();

            var response = new List<AnalyticsWithEmployees>();

            var delegates = unitOfWork.UserDelegateRepository.GetByGrantedUserIdAndType(currentUser.Id, UserDelegateType.WorkTimeApproval);

            var list = unitOfWork.AnalyticRepository.GetByManagerIdAndDirectorId(currentUser.Id).ToList();

            foreach (var analytic in list)
            {
                var itemToAdd = new AnalyticsWithEmployees
                {
                    Id = analytic.Id,
                    Text = $"{analytic.Title} - {analytic.Name}"
                };

                itemToAdd.Resources = unitOfWork.EmployeeRepository.GetByAnalyticIdInCurrentDate(analytic.Id).Select(x => new ResourceOption { Id = x.Id, Text = x.Name, UserId = x.UserId}).ToList();

                response.Add(itemToAdd);
            }

            foreach (var userDelegate in delegates)
            {
                if (userDelegate.SourceType == UserDelegateSourceType.Analytic)
                {
                    if (response.All(x => x.Id != userDelegate.AnalyticSourceId.GetValueOrDefault()))
                    {
                        var analytic = unitOfWork.AnalyticRepository.GetAnalyticLiteById(userDelegate.AnalyticSourceId.GetValueOrDefault());

                        var itemToAdd = new AnalyticsWithEmployees
                        {
                            Id = analytic.Id,
                            Text = $"{analytic.Title} - {analytic.Name}"
                        };

                        itemToAdd.Resources = unitOfWork.EmployeeRepository.GetByAnalyticIdInCurrentDate(analytic.Id).Select(x => new ResourceOption { Id = x.Id, Text = x.Name, UserId = x.UserId}).ToList();

                        response.Add(itemToAdd);
                    }
                        
                }

                if (userDelegate.SourceType == UserDelegateSourceType.User)
                {
                    var user = unitOfWork.UserRepository.GetUserLiteById(userDelegate.UserSourceId.GetValueOrDefault());
                    var analyticId = userDelegate.AnalyticSourceId.GetValueOrDefault();

                    if (response.All(x => x.Id != analyticId))
                    {
                        var analytic = unitOfWork.AnalyticRepository.GetAnalyticLiteById(analyticId);

                        var itemToAdd = new AnalyticsWithEmployees
                        {
                            Id = analytic.Id,
                            Text = $"{analytic.Title} - {analytic.Name}",
                            Resources = new List<ResourceOption>()
                        };

                        itemToAdd.Resources.Add(new ResourceOption { Id = user.Employee.Id, Text = user.Employee.Name, UserId = user.Id });

                        response.Add(itemToAdd);
                    }
                    else
                    {
                        var analytic = response.FirstOrDefault(x => x.Id == analyticId);

                        analytic?.Resources.Add(new ResourceOption { Id = user.Employee.Id, Text = user.Employee.Name, UserId = user.Id });
                    }
                }
            }

            return response;
        }

        public Response Reject(int id, string comments, bool massive)
        {
            return workTimeRejectManager.Reject(id, comments, massive);
        }

        public Response ApproveAll(List<HoursToApproveModel> hours)
        {
            var response = new Response();

            var currentUser = userData.GetCurrentUser();

            var isManager = roleManager.IsManager(currentUser.Id);
            var isDirector = roleManager.IsDirector(currentUser.Id);
            var delegates = unitOfWork.UserDelegateRepository.GetByGrantedUserIdAndType(currentUser.Id, UserDelegateType.WorkTimeApproval);

            if (!isManager && !isDirector && !delegates.Any())
            {
                response.AddError(Sofco.Resources.WorkTimeManagement.WorkTime.CannotChangeStatus);
                return response;
            }

            var anyError = false;
            var anySuccess = false;

            foreach (var hour in hours)
            {
                var hourResponse = Approve(hour.Id, currentUser, isManager, isDirector);

                if (hourResponse.HasErrors())
                {
                    anyError = true;

                    foreach (var hourResponseMessage in hourResponse.Messages)
                    {
                        if (response.Messages.All(x => x.Text != hourResponseMessage.Text))
                        {
                            response.Messages.Add(hourResponseMessage);
                        }
                    }
                }
                else
                    anySuccess = true;
            }

            if (anySuccess && anyError)
            {
                response.AddWarning(Sofco.Resources.WorkTimeManagement.WorkTime.ApprovedWithSomeErrors);
            }

            if (anySuccess)
            {
                response.AddSuccess(Sofco.Resources.WorkTimeManagement.WorkTime.ApprovedSuccess);
            }

            return response;
        }

        public Response Send()
        {
            return workTimeSendHoursManager.Send();
        }

        public Response<IList<WorkTimeSearchItemResult>> Search(SearchParams parameters)
        {
            var response = new Response<IList<WorkTimeSearchItemResult>>();

            if (!parameters.StartDate.HasValue || parameters.StartDate == DateTime.MinValue ||
                !parameters.EndDate.HasValue || parameters.EndDate == DateTime.MinValue)
            {
                response.AddError(Sofco.Resources.WorkTimeManagement.WorkTime.DatesRequired);
                return response;
            }

            var worktimes = unitOfWork.WorkTimeRepository.Search(parameters);

            response.Data = new List<WorkTimeSearchItemResult>();

            foreach (var worktime in worktimes)
            {
                var model = new WorkTimeSearchItemResult();

                if (worktime.Analytic != null)
                {
                    model.Client = worktime.Analytic.Customer.Name;
                    model.AnalyticTitle = worktime.Analytic.Title;
                    model.Analytic = worktime.Analytic.Name;
                    model.Manager = worktime.Analytic?.Manager?.Name;
                }

                if (worktime.Employee != null)
                {
                    model.Employee = $"{worktime.Employee?.EmployeeNumber} - {worktime.Employee.Name}";
                    model.Profile = worktime.Employee.Profile;
                }

                if (worktime.Task != null)
                {
                    model.Task = worktime.Task.Description;

                    if (worktime.Task.Category != null)
                    {
                        model.Category = worktime.Task.Category.Description;
                    }
                }

                model.Date = worktime.Date;
                model.Hours = worktime.Hours;
                model.Reference = worktime.Reference;
                model.Comments = worktime.UserComment;
                model.Status = worktime.Status.ToString();

                response.Data.Add(model);
            }

            if (!response.Data.Any())
            {
                response.AddWarning(Sofco.Resources.WorkTimeManagement.WorkTime.SearchNotFound);
            }

            return response;
        }

        public Response RejectAll(WorkTimeRejectParams parameters)
        {
            var response = new Response();
            var anyError = false;
            var anySuccess = false;

            foreach (var hourId in parameters.HourIds)
            {
                var hourResponse = Reject(hourId, parameters.Comments, true);

                if (hourResponse.HasErrors())
                    anyError = true;
                else
                    anySuccess = true;
            }

            if (anySuccess && anyError)
            {
                response.AddWarning(Sofco.Resources.WorkTimeManagement.WorkTime.RejectedWithSomeErrors);
            }

            if (anySuccess)
            {
                var workTime = unitOfWork.WorkTimeRepository.GetSingle(x => x.Id == parameters.HourIds.FirstOrDefault());

                workTimeRejectManager.SendGeneralRejectMail(workTime);
                response.AddSuccess(Sofco.Resources.WorkTimeManagement.WorkTime.RejectedSuccess);
            }
            else
            {
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        public Response Delete(int id)
        {
            var response = new Response();

            var workTime = unitOfWork.WorkTimeRepository.GetSingle(x => x.Id == id);

            workTimeValidation.ValidateDelete(response, workTime);

            if (response.HasErrors()) return response;

            try
            {
                unitOfWork.WorkTimeRepository.Delete(workTime);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.WorkTimeManagement.WorkTime.DeleteSuccess);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.WorkTimeManagement.WorkTime.DeleteError);
            }

            return response;
        }

        public void Import(int analyticId, IFormFile file, Response<IList<WorkTimeImportResult>> response)
        {
            AnalyticValidationHelper.Exist(response, unitOfWork.AnalyticRepository, analyticId);

            if (response.HasErrors()) return;

            var memoryStream = new MemoryStream();
            file.CopyTo(memoryStream);

            workTimeImportFileManager.Import(analyticId, memoryStream, response);
        }

        public byte[] ExportTemplate(int analyticId)
        {
            var excel = workTimeExportFileManager.CreateTemplateExcel(analyticId);

            return excel.GetAsByteArray();
        }

        public IEnumerable<Option> GetStatus()
        {
            yield return new Option { Id = (int)WorkTimeStatus.Draft, Text = WorkTimeStatus.Draft.ToString() };
            yield return new Option { Id = (int)WorkTimeStatus.Sent, Text = WorkTimeStatus.Sent.ToString() };
            yield return new Option { Id = (int)WorkTimeStatus.SentToDirector, Text = WorkTimeStatus.SentToDirector.ToString() };
            yield return new Option { Id = (int)WorkTimeStatus.Rejected, Text = WorkTimeStatus.Rejected.ToString() };
            yield return new Option { Id = (int)WorkTimeStatus.Approved, Text = WorkTimeStatus.Approved.ToString() };
        }

        private List<int> GetAnalyticIds(int? analyticId)
        {
            var currentUser = userData.GetCurrentUser();

            var availableAnalyticIds = unitOfWork.AnalyticRepository.GetLiteByManagerId(currentUser.Id).Select(s => s.Id).ToList();

            if (!analyticId.HasValue || analyticId == 0) return availableAnalyticIds;

            var selectedAnalyticId = analyticId.Value;

            return availableAnalyticIds.Contains(selectedAnalyticId) 
                ? new List<int> { selectedAnalyticId } 
                : new List<int> ();
        }
    }
}

