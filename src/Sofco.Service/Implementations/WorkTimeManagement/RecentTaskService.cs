﻿using System;
using Sofco.Core.DAL;
using Sofco.Core.Data.Admin;
using Sofco.Core.Logger;
using Sofco.Core.Models.WorkTimeManagement;
using Sofco.Core.Services.WorkTimeManagement;
using Sofco.Domain.Models.WorkTimeManagement;
using Sofco.Domain.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Sofco.Service.Implementations.WorkTimeManagement
{
    public class RecentTaskService : IRecentTaskService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILogMailer<RecentTaskService> logger;
        private readonly IUserData userData;

        public RecentTaskService(IUnitOfWork unitOfWork, ILogMailer<RecentTaskService> logger, IUserData userData)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.userData = userData;
        }

        public Response<IList<RecentTaskModel>> Get()
        {
            var response = new Response<IList<RecentTaskModel>> { Data = new List<RecentTaskModel>() };

            var currentUser = userData.GetCurrentUser();

            try
            {
                var recentTasks = unitOfWork.RecentTaskRepository.GetByUser(currentUser.Id);

                foreach (var recentTask in recentTasks)
                {
                    var model = response.Data.SingleOrDefault(x => x.AnalyticId == recentTask.AnalyticId);

                    if (model == null)
                    {
                        model = new RecentTaskModel
                        {
                            Analytic = recentTask.Analytic,
                            AnalyticId = recentTask.AnalyticId,
                            Tasks = new List<RecentTask> { recentTask }
                        };

                        response.Data.Add(model);
                    }
                    else
                    {
                        model.Tasks.Add(recentTask);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError(e);
            }

            return response;
        }

        public Response Delete(int id)
        {
            var response = new Response();

            try
            {
                var recentTask = unitOfWork.RecentTaskRepository.Get(id);

                if (recentTask != null)
                {
                    unitOfWork.RecentTaskRepository.Delete(recentTask);
                    unitOfWork.Save();
                }
            }
            catch (Exception e)
            {
                logger.LogError(e);
            }

            return response;
        }

        public Response Update(int id, decimal value)
        {
            var response = new Response();

            try
            {
                var recentTask = unitOfWork.RecentTaskRepository.Get(id);

                if (recentTask != null)
                {
                    recentTask.Hours = value;
                    unitOfWork.RecentTaskRepository.Update(recentTask);
                    unitOfWork.Save();
                }
            }
            catch (Exception e)
            {
                logger.LogError(e);
            }

            return response;
        }
    }
}
