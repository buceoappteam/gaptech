﻿using System;
using Sofco.Core.Services.AllocationManagement;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Options;
using Sofco.Common.Security.Interfaces;
using Sofco.Common.Settings;
using Sofco.Core.Config;
using Sofco.Core.Data.Admin;
using Sofco.Core.Data.Resources;
using Sofco.Core.DAL;
using Sofco.Core.Logger;
using Sofco.Core.Mail;
using Sofco.Core.Models;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Core.Models.Rrhh;
using Sofco.Domain.Utils;
using Sofco.Framework.ValidationHelpers.AllocationManagement;
using Sofco.Domain.DTO;
using Sofco.Domain.Relationships;
using Sofco.Framework.Helpers;
using Sofco.Resources.Mails;
using Sofco.Domain.Models.Resources;
using Sofco.Core.Models.Admin;
using Sofco.Core.Services.Admin;
using Sofco.Domain.Models.Admin;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Sofco.Service.Implementations.AllocationManagement
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILogMailer<EmployeeService> logger;
        private readonly IMapper mapper;
        private readonly IEmployeeData employeeData;
        private readonly IUserData userData;
        private readonly AppSetting appSetting;

        public EmployeeService(IUnitOfWork unitOfWork,
            ILogMailer<EmployeeService> logger,
            IMapper mapper,
            IEmployeeData employeeData,
            IUserData userData,
            IOptions<AppSetting> appSettingOptions)
        {
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.mapper = mapper;
            this.employeeData = employeeData;
            this.userData = userData;
            this.appSetting = appSettingOptions.Value;
        }

        public ICollection<Employee> GetAll()
        {
            return unitOfWork.EmployeeRepository.GetAll();
        }

        public Response<EmployeeModel> GetById(int id)
        {
            var response = new Response<EmployeeModel>();

            var result = EmployeeValidationHelper.Find(response, unitOfWork.EmployeeRepository, id);

            response.Data = Translate(result);

            return response;
        }

        private EmployeeModel Translate(Employee employee)
        {
            return mapper.Map<Employee, EmployeeModel>(employee);
        }

        public Response<ICollection<Employee>> Search(EmployeeSearchParams parameters)
        {
            var response = new Response<ICollection<Employee>>() { Data = new List<Employee>() };

            var employees = unitOfWork.EmployeeRepository.Search(parameters);

            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            foreach (var employee in employees)
            {
                if (parameters.ManagerId.HasValue && parameters.ManagerId.Value > 0)
                {
                    if (unitOfWork.AllocationRepository.ExistCurrentAllocationByEmployeeAndManagerId(employee.Id, parameters.ManagerId.Value, startDate))
                    {
                        response.Data.Add(employee);
                    }
                }
                else
                {
                    response.Data.Add(employee);
                }
            }

            if (!response.Data.Any())
            {
                response.AddWarning(Sofco.Resources.AllocationManagement.Employee.EmployeesNotFound);
            }

            return response;
        }

        public Response<EmployeeProfileModel> GetProfile(int id)
        {
            var response = new Response<EmployeeProfileModel>();

            var employee = EmployeeValidationHelper.Find(response, unitOfWork.EmployeeRepository, id);

            if (response.HasErrors()) return response;

            response.Data = GetEmployeeModel(employee);

            return response;
        }

        public Response AddCategories(EmployeeAddCategoriesParams parameters)
        {
            var response = new Response();

            foreach (var employeeId in parameters.Employees)
            {
                if (parameters.Clean)
                {
                    unitOfWork.CategoryRepository.CleanByEmployeeId(employeeId);
                    unitOfWork.Save();
                }

                foreach (var categoryId in parameters.CategoriesToAdd)
                {
                    if (!unitOfWork.CategoryRepository.ExistEmployeeCategory(employeeId, categoryId))
                    {
                        var employeeCategory = new EmployeeCategory(employeeId, categoryId);
                        unitOfWork.CategoryRepository.AddEmployeeCategory(employeeCategory);
                    }
                }
            }

            try
            {
                unitOfWork.Save();
                response.AddSuccess(Sofco.Resources.AllocationManagement.Employee.CategoriesAdded);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        public ICollection<Option> GetAnalytics(int id)
        {
            return unitOfWork.AnalyticRepository.GetByManagerIdAndDirectorId(id).Select(x => new Option { Id = x.Id, Text = $"{x.Title} - {x.Name}" }).ToList();
        }

        public Response<IList<EmployeeCategoryOption>> GetCategories(int employeeId)
        {
            var employeeCategories = unitOfWork.EmployeeRepository.GetEmployeeCategories(employeeId);

            var response = new Response<IList<EmployeeCategoryOption>> { Data = new List<EmployeeCategoryOption>() };

            foreach (var employeeCategory in employeeCategories)
            {
                if (employeeCategory.Category == null) continue;

                foreach (var task in employeeCategory.Category.Tasks)
                {
                    if (!task.Active) continue;

                    var option = new EmployeeCategoryOption
                    {
                        CategoryId = employeeCategory.CategoryId,
                        Category = employeeCategory.Category?.Description,
                        TaskId = task.Id,
                        Task = task.Description
                    };

                    response.Data.Add(option);
                }
            }

            return response;
        }

        public Response Update(int id, EmployeeBusinessHoursParams model)
        {
            var response = new Response();

            var stored = EmployeeValidationHelper.Find(response, unitOfWork.EmployeeRepository, id);
            EmployeeValidationHelper.ValidateBusinessHours(response, model);
            EmployeeValidationHelper.ValidateBillingPercentage(response, model);
            EmployeeValidationHelper.ValidateOffice(response, model);
            EmployeeValidationHelper.ValidateManager(response, model, unitOfWork);

            if (response.HasErrors()) return response;

            try
            {
                var employee = new Employee();
                employee.Id = id;
                employee.BusinessHours = model.BusinessHours.GetValueOrDefault();
                employee.BusinessHoursDescription = model.BusinessHoursDescription;
                employee.OfficeAddress = model.Office;
                employee.BillingPercentage = model.BillingPercentage.GetValueOrDefault();

                unitOfWork.EmployeeRepository.UpdateBusinessHours(employee);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.AllocationManagement.Employee.BusinessHoursUpdated);
            }
            catch (Exception e)
            {
                logger.LogError(e);
                response.AddError(Sofco.Resources.Common.ErrorSave);
            }

            return response;
        }

        public Response<IList<EmployeeCategoryOption>> GetCurrentCategories()
        {
            var currentEmployee = employeeData.GetCurrentEmployee();

            return GetCategories(currentEmployee.Id);
        }

        public IList<UnemployeeListItemModel> GetUnemployees(UnemployeeSearchParameters parameters)
        {
            var employees = unitOfWork.EmployeeRepository.SearchUnemployees(parameters);

            return employees.Select(x => new UnemployeeListItemModel(x)).ToList();
        }

        public Response<EmployeeWorkingPendingHoursModel> GetPendingWorkingHours(int employeeId)
        {
            var pendingHours = unitOfWork.WorkTimeRepository.GetPendingHoursByEmployeeId(employeeId);

            var result = new Response<EmployeeWorkingPendingHoursModel>
            {
                Data = new EmployeeWorkingPendingHoursModel
                {
                    EmployeeId = employeeId,
                    PendingHours = pendingHours
                }
            };

            return result;
        }

        public Response<List<Option>> GetEmployeeOptionByCurrentManager()
        {
            var analytics = unitOfWork.AnalyticRepository.GetLiteByManagerId(userData.GetCurrentUser().Id);

            var employees = unitOfWork.EmployeeRepository.GetByAnalyticIds(analytics.Select(x => x.Id).ToList());

            return new Response<List<Option>> { Data = Translate(employees.ToList()) };
        }

        public Response<EmployeeModel> GetByMail(string email)
        {
            var response = new Response<EmployeeModel>();

            var result = unitOfWork.EmployeeRepository.GetByEmail(email);

            response.Data = Translate(result);

            return response;
        }

        private EmployeeProfileModel GetEmployeeModel(Employee employee)
        {
            var model = TranslateToProfile(employee);

            model.EndDate = employee?.EndDate;

            var employeeAllocations = unitOfWork.AllocationRepository.GetByEmployee(employee.Id);

            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            var analitycs = employeeAllocations.Where(x => x.Percentage > 0 && x.StartDate.Date == date).Select(x => x.Analytic).Distinct();

            foreach (var analityc in analitycs)
            {
                var allocationThisMonth = analityc.Allocations.FirstOrDefault(x => x.StartDate == new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
                if (allocationThisMonth != null)
                {
                    var item = new EmployeeAllocationModel
                    {
                        Title = analityc.Title,
                        Name = analityc.Name,
                        AllocationPercentage = allocationThisMonth.Percentage,
                        StartDate = unitOfWork.AllocationRepository.GetStartDate(analityc.Id, employee.Id),
                        Client = analityc.Customer.Name,
                        ReleaseDate = allocationThisMonth?.ReleaseDate,
                    };

                    model.Allocations.Add(item);
                }
            }

            return model;
        }

        public Response AddEmployee(EmployeeAddModel model)
        {
            var response = new Response();

            EmployeeValidationHelper.ValidateAdd(response, model);
            if (response.HasErrors()) return response;

            var currentUser = userData.GetCurrentUser();

            Employee employeeDomail = model.CreateDomain();
            employeeDomail.CreatedByUser = currentUser.Id.ToString();
            employeeDomail.Created = DateTime.Now;

            var userDomain = new User();

            try
            {
                userDomain.Name = model.Name;
                userDomain.UserName = model.UserName;
                userDomain.Email = model.Email;
                userDomain.Active = true;
                userDomain.StartDate = DateTime.UtcNow;
                userDomain.Employee = employeeDomail;

                unitOfWork.UserRepository.Insert(userDomain);
                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.Admin.User.Created);
            }
            catch (Exception e)
            {
                response.AddError(Sofco.Resources.Common.ErrorSave);
                logger.LogError(e);
            }

            try
            {
                var role = unitOfWork.RoleRepository.GetByCode(appSetting.UserRole);

                var userRole = new UserRole
                {
                    RoleId = role.Id,
                    UserId = userDomain.Id
                };

                unitOfWork.UserRoleRepository.Insert(userRole);
                unitOfWork.Save();
            }
            catch (Exception e)
            {
                logger.LogError(e);
            }

            return response;
        }

        public Response UpdateEmployee(EmployeeAddModel employee, int id)
        {
            var response = new Response();

            EmployeeValidationHelper.ValidateAdd(response, employee);
            if (response.HasErrors()) return response;

            if (employee.UserId == 0)
            {
                response.AddError(Sofco.Resources.Admin.User.NotFound);
                return response;
            }

            try
            {
                var entity = unitOfWork.UserRepository.GetByIdWithEmployee(employee.UserId);
                if (entity == null)
                {
                    response.AddError(Sofco.Resources.Admin.User.NotFound);
                    return response;
                }

                entity.Name = employee.Name;

                entity.Employee.Modified = DateTime.Now;

                entity.Employee.Address = employee.Address;
                entity.Employee.BillingPercentage = employee.Percentage;
                entity.Employee.Birthday = employee.Birthday;
                entity.Employee.BusinessHours = employee.BusinessHours;
                entity.Employee.BusinessHoursDescription = employee.BusinessHoursDescription;
                entity.Employee.Cuil = employee.Cuil;
                entity.Employee.DocumentNumber = employee.DocumentNumber;
                entity.Employee.DocumentTypeId = employee.DocumentTypeId;
                entity.Employee.Email = employee.Email;
                entity.Employee.EmployeeNumber = employee.EmployeeNumber;
                entity.Employee.Location = employee.Location;
                entity.Employee.Name = employee.Name;
                entity.Employee.OfficeAddress = employee.OfficeAddress;
                entity.Employee.PhoneAreaCode = employee.PhoneAreaCode;
                entity.Employee.PhoneCountryCode = employee.PhoneCountryCode;
                entity.Employee.PhoneNumber = employee.PhoneNumber.ToString();
                entity.Employee.Profile = employee.Profile;
                entity.Employee.ProvinceId = employee.ProvinceId;
                entity.Employee.Seniority = employee.Seniority;
                entity.Employee.StartDate = employee.StartDate;
                entity.Employee.Technology = employee.Technology;

                unitOfWork.UserRepository.Update(entity);

                unitOfWork.Save();

                response.AddSuccess(Sofco.Resources.Common.SaveSuccess);
            }
            catch (Exception e)
            {
                if (e is DbUpdateException dbUpdateEx)
                {
                    if (dbUpdateEx?.InnerException is SqlException sqlEx && (sqlEx.Number == 2601 || sqlEx.Number == 2627))
                    {
                        response.AddError(Sofco.Resources.AllocationManagement.Employee.EmployeeNumberAlreadyAssigned);
                    }
                    else
                    {
                        response.AddError(Sofco.Resources.Common.ErrorSave);
                    }
                }
                else
                {
                    response.AddError(Sofco.Resources.Common.ErrorSave);
                }

                logger.LogError(e);
            }

            return response;
        }

        private EmployeeProfileModel TranslateToProfile(Employee employee)
        {
            return mapper.Map<Employee, EmployeeProfileModel>(employee);
        }

        private List<Option> Translate(List<Employee> employees)
        {
            return mapper.Map<List<Employee>, List<Option>>(employees);
        }
    }
}
