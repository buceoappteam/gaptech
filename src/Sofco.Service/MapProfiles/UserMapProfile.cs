﻿using AutoMapper;
using Sofco.Core.Models.Admin;
using Sofco.Domain.AzureAd;
using Sofco.Domain.Models.Admin;

namespace Sofco.Service.MapProfiles
{
    public class UserMapProfile : Profile
    {
        public UserMapProfile()
        {
            CreateMap<User, UserModel>()
                .ForMember(d => d.EmployeeId, s => s.ResolveUsing(x => x.Employee?.Id));

            CreateMap<User, UserModel>()
              .ForMember(d => d.EmployeeName, s => s.ResolveUsing(x => x.Employee?.Name));

            CreateMap<User, UserModel>()
            .ForMember(d => d.employeeNumber, s => s.ResolveUsing(x =>
            {
                int legajo;
                if (int.TryParse(x.Employee.EmployeeNumber, out legajo))
                {
                    return legajo;
                }
                else
                {
                    return 0;
                }
            }));

            CreateMap<User, UserDetailModel>();

            CreateMap<AzureAdLoginResponse, UserTokenModel>()
                .ForMember(d => d.AccessToken, s => s.MapFrom(x => x.access_token))
                .ForMember(d => d.RefreshToken, s => s.MapFrom(x => x.refresh_token))
                .ForMember(d => d.ExpiresIn, s => s.MapFrom(x => x.expires_in));

            CreateMap<User, UserSelectListItem>()
                .ForMember(d => d.Text, s => s.MapFrom(x => x.Name));

            CreateMap<User, UserLiteModel>();

            CreateMap<UserLiteModel, User>();
        }
    }
}
