﻿using AutoMapper;
using Sofco.Core.Models.AllocationManagement;
using Sofco.Domain.Models.Resources;
using Sofco.Domain.Utils;

namespace Sofco.Service.MapProfiles
{
    public class EmployeeMapProfile : Profile
    {
        private const int InitialBillingPercentage = 100;

        private const string ArCountry = "Argentina";

        private const char ListSeparator = ';';

        public EmployeeMapProfile()
        {
            CreateMap<Employee, EmployeeModel>();

            CreateMap<Allocation, EmployeeAllocationModel>();

            CreateMap<Employee, Option>()
                .ForMember(d => d.Text, s => s.ResolveUsing(x => $"{x.EmployeeNumber} - {x.Name}"));

            CreateMap<Employee, EmployeeProfileModel>()
                .ForMember(d => d.Percentage, s => s.MapFrom(x => x.BillingPercentage))
                .ForMember(d => d.Province, s => s.MapFrom(x => x.Province.Text))
                .ForMember(d => d.ProvinceId, s => s.MapFrom(x => x.Province.Id))
                .ForMember(d => d.CountryId, s => s.MapFrom(x => x.Province.CountryId))
                .ForMember(d => d.DocumentTypeId, s => s.MapFrom(x => x.DocumentType.Id))
                .ForMember(d => d.Cuil, s => s.MapFrom(x => x.Cuil.ToString()))
                .ForMember(d => d.Email, s => s.MapFrom(x => x.Email.ToString()))
                .ForMember(d => d.Country, s => s.MapFrom(x => x.Province.Country.Text));
        }
    }
}
